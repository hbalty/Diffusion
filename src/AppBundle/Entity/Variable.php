<?php

namespace AppBundle\Entity;
use AppBundle\Entity\Metier;
use Doctrine\ORM\Mapping as ORM;



/**
 * Template
 *
 * @ORM\Table(name="variable")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VariableRepository")
 */
 class Variable
 {
    
    
     /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    
    public function getId(){
        return $this->id;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
     private $name;

     public function getName()
     {
         return $this->name;
     }

     public function setName($name)
     {
         $this->name = $name;
     }
 }
