<?php

namespace AppBundle\Entity;
use AppBundle\Entity\Metier;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Application
 * @UniqueEntity(
 *     fields={"nomApplication"},
 *     message="Nom application déja utilisé"
 * )
 * @ORM\Table(name="application")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;




    /**
     * @var string
     * @Assert\NotBlank(message = "Le champ nom application ne doit pas être vide !")
     * @ORM\Column(name="nomApplication", type="string", length=255, unique=true)
     */
    private $nomApplication;

    /**
     * @var boolean
     *
     * @ORM\Column(name="activationStatus", type="boolean")
     */
    private $activationStatus;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }





    /**
     * Set nomApplication
     *
     * @param string $nomApplication
     *
     * @return Application
     */
    public function setNomApplication($nomApplication)
    {
        $this->nomApplication = $nomApplication;

        return $this;
    }

    /**
     * Get nomApplication
     *
     * @return string
     */
    public function getNomApplication()
    {
        return $this->nomApplication;
    }

    /**
     * Set activationStatus
     *
     * @param boolean $activationStatus
     *
     * @return Application
     */

    public function setActivationStatus($activationStatus)
    {
        $this->activationStatus = $activationStatus;

        return $this;
    }

    /**
     * Get activationStatus
     *
     * @return boolean
     */
    public function getActivationStatus()
    {
        return $this->activationStatus;
    }


    /**
     *@Assert\NotBlank(message="Veuillez choisir au moins un metier !")
     * Plusieurs Metiers peuvent appartenir à plusieurs applications
     * @ORM\ManyToMany(targetEntity="Metier", mappedBy="applications")
     */

    private $metiers ;

    public function __construct(){
        $this->metiers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function addMetier(Metier $metier)
   {
       $this->metiers[] = $metier;
   }


   public function getMetiers(){
      return $this->metiers;
   }


       public function setMetiers(Metier $metier){
          $this->metiers = $metier;
          return $this;
       }


}
