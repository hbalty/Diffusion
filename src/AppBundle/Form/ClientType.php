<?php

namespace AppBundle\Form;

use AppBundle\Entity\Metier;
use AppBundle\Entity\Client;
use AppBundle\Entity\MailingList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\ImpactMetierType;
use AppBundle\Form\ImpactApplicationType;
use Doctrine\ORM\EntityRepository;

class ClientType extends AbstractType
{

    /**
    * @param FormBuilderInterface $builder
    * @param array $options
    */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('clientName',TextType::class)
        ->add('lang',TextType::class)
        ->add('timezone',TextType::class)
        ->add('templateSubject',TextType::class)
         ->add('template',CKEditorType::class, array(
        'config' => array(
        'uiColor' => '#ffffff',
        'height' => '500',
        'allowedContent' => true,
        ), 
        ));
      

    }

    /**
    * @param OptionsResolverInterface $resolver
    */

    public function setDefaultOptions(OptionsResolverInterface $resolver){
      var_dump($options['constraints']);

      $resolver->setDefaults(array(
        'data_class' => 'AppBundle\Entity\Client',
        'idClient' => null
      ));
    }


}
