<?php

namespace AppBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use AppBundle\Entity\Application;
use AppBundle\Entity\Metier;
use AppBundle\Form\ApplicationType;

class ApplicationController extends Controller
{
    /**
     * @Route("/application/list", name="application_list")
     */

 public function listAction(Request $request)
    {
      $app = new Application();
      $applications = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Application')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();





          return $this->render('application/application.html.twig', array('applications' => $applications));

    }


    /**
     * @Route("/application/add", name="application_add")
     */

 public function addAction(Request $request)
    {
      $applications = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Application')
            ->createQueryBuilder('c')
            ->getQuery()->iterate();

      $application = new Application();

       // Génération du formulaire de login !
       $form = $this->createForm(ApplicationType::class,$application);


      $form->handleRequest($request) ;
        if ($form->isSubmitted() && $form->isValid()){
             $data = $form->getData();
             $em = $this->getDoctrine()->getEntityManager() ;
             
             $em->persist($data);
             $em->flush();
              return $this->render('/application/applicationAdd.html.twig',array(
                     'form' => $form->createView(),
                     'applications' => $applications,
                     'success' => "Application ajoutée avec succès ! ",
               ));
           } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('/application/applicationAdd.html.twig',array(
                     'form' => $form->createView(),
                     'applications' => $applications,
                     'errors' => $errors,
               ));

           }

      return $this->render('/application/applicationAdd.html.twig',array(
       'form' => $form->createView(),
       'applications' => $applications,
       ));


    }

    /**
     * @Route("/application/deactivate/{id}", name="application_deactivate")
     */

  public function deactivateAction($id)
    {
         $application = $this->getDoctrine()
         ->getRepository("AppBundle:Application")
         ->findOneById($id) ;
         if ($application->getActivationStatus() == 0 )
              $application->setActivationStatus(1) ;
            else {
              $application->setActivationStatus(0) ;
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->flush() ;
            
            return $this->redirect('/application/list') ;
    }


    /**
     * @Route("/application/details/{id}", name="application_details")
     */

  public function detailsAction($id)
    {
        $application = $this
        ->getDoctrine()
        ->getRepository('AppBundle:Application')
        ->findOneById($id) ;

        $metier = new Metier();
        $lesMetiers = $application->getMetiers();

         return $this->render("application/applicationDetails.html.twig",array(
       'application' => $application,
      ));


    }


    /**
     * @Route("/application/update/{idApplication}", name="application_update")
     */

 public function updateAction(Request $request, $idApplication)
    {
      // Récupération du l'application
      $application = $this->getDoctrine()
      ->getRepository('AppBundle:Application')
      ->findOneById($idApplication);

      // Création du formulaire
      $form = $this->createForm(ApplicationType::class, $application) ;

      // handling request
      $form->handleRequest($request) ;

      if($form->isValid() && $form->isSubmitted()){
        $updatedApplication = $form->getData();
       
        // entity manager
        $em = $this->getDoctrine()->getEntityManager();
        $em->flush();
        
         return $this->render('application/applicationUpdate.html.twig', array(
        'form' => $form->createView(),
        'success' => "L'application est bien mise à jour !"
      )) ;
        
      } else if ($form->isSubmitted() && !$form->isValid()){
             $data = $form->getData() ;
             $validator = $this->get('validator');
             $errors = $validator->validate($data);
             
                   return $this->render('application/applicationUpdate.html.twig', array(
          'form' => $form->createView(),
          'errors' =>  $errors,
      )) ;
                  
      }
      return $this->render('application/applicationUpdate.html.twig', array(
        'form' => $form->createView(),
      )) ;


    }



}
