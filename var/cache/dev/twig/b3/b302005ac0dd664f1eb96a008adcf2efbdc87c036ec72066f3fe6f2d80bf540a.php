<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_97f4d654b9cd957174e49c68b980807053f98472df251143559d9f6be9f0b64c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2450310c69a856b81baae0f9d0372253855d4de6ac36c726237a7cf36bbef7f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2450310c69a856b81baae0f9d0372253855d4de6ac36c726237a7cf36bbef7f0->enter($__internal_2450310c69a856b81baae0f9d0372253855d4de6ac36c726237a7cf36bbef7f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2450310c69a856b81baae0f9d0372253855d4de6ac36c726237a7cf36bbef7f0->leave($__internal_2450310c69a856b81baae0f9d0372253855d4de6ac36c726237a7cf36bbef7f0_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5d82e41b0765d3d636c8cb48e1743860c6f48ce54ceecb83a4ed8cac40619fc2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d82e41b0765d3d636c8cb48e1743860c6f48ce54ceecb83a4ed8cac40619fc2->enter($__internal_5d82e41b0765d3d636c8cb48e1743860c6f48ce54ceecb83a4ed8cac40619fc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 4)->display($context);
        
        $__internal_5d82e41b0765d3d636c8cb48e1743860c6f48ce54ceecb83a4ed8cac40619fc2->leave($__internal_5d82e41b0765d3d636c8cb48e1743860c6f48ce54ceecb83a4ed8cac40619fc2_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/request.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\request.html.twig");
    }
}
