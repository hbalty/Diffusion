<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_7999280e353cafa0cc8edc74987c7b833be87e2bc3f6f45e361507310fd64151 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df634ea7b798acb7c10058ba850bc67d3f3b484e053fa5dc6b3fb2de2169b3f7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df634ea7b798acb7c10058ba850bc67d3f3b484e053fa5dc6b3fb2de2169b3f7->enter($__internal_df634ea7b798acb7c10058ba850bc67d3f3b484e053fa5dc6b3fb2de2169b3f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_df634ea7b798acb7c10058ba850bc67d3f3b484e053fa5dc6b3fb2de2169b3f7->leave($__internal_df634ea7b798acb7c10058ba850bc67d3f3b484e053fa5dc6b3fb2de2169b3f7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/FormTable/hidden_row.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\hidden_row.html.php");
    }
}
