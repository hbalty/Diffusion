<?php

/* /incident/incidentAdd.html.twig */
class __TwigTemplate_7b28ab012531d9645cfb0db779e97e437a8a7c2deb788aa5ea22a44b689cd1ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "/incident/incidentAdd.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f681e7021a83057aef0d5eeb30e70ea430eaca56562919fcd7aee0c80882668c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f681e7021a83057aef0d5eeb30e70ea430eaca56562919fcd7aee0c80882668c->enter($__internal_f681e7021a83057aef0d5eeb30e70ea430eaca56562919fcd7aee0c80882668c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/incident/incidentAdd.html.twig"));

        $__internal_b128ceb34b07c92600d8a3b6471ae4e09834a45d442e3e8caceab60bce2fc6c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b128ceb34b07c92600d8a3b6471ae4e09834a45d442e3e8caceab60bce2fc6c1->enter($__internal_b128ceb34b07c92600d8a3b6471ae4e09834a45d442e3e8caceab60bce2fc6c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/incident/incidentAdd.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f681e7021a83057aef0d5eeb30e70ea430eaca56562919fcd7aee0c80882668c->leave($__internal_f681e7021a83057aef0d5eeb30e70ea430eaca56562919fcd7aee0c80882668c_prof);

        
        $__internal_b128ceb34b07c92600d8a3b6471ae4e09834a45d442e3e8caceab60bce2fc6c1->leave($__internal_b128ceb34b07c92600d8a3b6471ae4e09834a45d442e3e8caceab60bce2fc6c1_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_9d8ee17f65baa5c7fd643e8036a2d768a02ce23e9edfea86c76967f8ef460c3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d8ee17f65baa5c7fd643e8036a2d768a02ce23e9edfea86c76967f8ef460c3c->enter($__internal_9d8ee17f65baa5c7fd643e8036a2d768a02ce23e9edfea86c76967f8ef460c3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_07a96746862ab08f06d9804a25846efb3f32dcf23bc0aa9a559efe0187476338 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07a96746862ab08f06d9804a25846efb3f32dcf23bc0aa9a559efe0187476338->enter($__internal_07a96746862ab08f06d9804a25846efb3f32dcf23bc0aa9a559efe0187476338_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "
";
        
        $__internal_07a96746862ab08f06d9804a25846efb3f32dcf23bc0aa9a559efe0187476338->leave($__internal_07a96746862ab08f06d9804a25846efb3f32dcf23bc0aa9a559efe0187476338_prof);

        
        $__internal_9d8ee17f65baa5c7fd643e8036a2d768a02ce23e9edfea86c76967f8ef460c3c->leave($__internal_9d8ee17f65baa5c7fd643e8036a2d768a02ce23e9edfea86c76967f8ef460c3c_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_24974366940a1a6cc79fdb981d1253d11c15ba6b433aaccd0295081654e92591 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24974366940a1a6cc79fdb981d1253d11c15ba6b433aaccd0295081654e92591->enter($__internal_24974366940a1a6cc79fdb981d1253d11c15ba6b433aaccd0295081654e92591_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_909498aa13fd1a0154fc32fa8fe101dd236fddfdaab40b69c4466a830ecaf22b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_909498aa13fd1a0154fc32fa8fe101dd236fddfdaab40b69c4466a830ecaf22b->enter($__internal_909498aa13fd1a0154fc32fa8fe101dd236fddfdaab40b69c4466a830ecaf22b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "      
    ";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "      
     ";
        
        $__internal_909498aa13fd1a0154fc32fa8fe101dd236fddfdaab40b69c4466a830ecaf22b->leave($__internal_909498aa13fd1a0154fc32fa8fe101dd236fddfdaab40b69c4466a830ecaf22b_prof);

        
        $__internal_24974366940a1a6cc79fdb981d1253d11c15ba6b433aaccd0295081654e92591->leave($__internal_24974366940a1a6cc79fdb981d1253d11c15ba6b433aaccd0295081654e92591_prof);

    }

    // line 22
    public function block_body($context, array $blocks = array())
    {
        $__internal_abe1d02ea2edea8a9c93b76876b66be9caf8e90d86cd1fa3e63750438f36bd5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abe1d02ea2edea8a9c93b76876b66be9caf8e90d86cd1fa3e63750438f36bd5a->enter($__internal_abe1d02ea2edea8a9c93b76876b66be9caf8e90d86cd1fa3e63750438f36bd5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b782c08b208d79bbd0d22cb5230819a21e97f1a277740f5937bfaa5680388766 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b782c08b208d79bbd0d22cb5230819a21e97f1a277740f5937bfaa5680388766->enter($__internal_b782c08b208d79bbd0d22cb5230819a21e97f1a277740f5937bfaa5680388766_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Incident <small>Décrire l'incident en Français</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                    
                      ";
        // line 42
        if (array_key_exists("success", $context)) {
            // line 43
            echo "                        ";
            if (((isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")) != null)) {
                // line 44
                echo "                    <div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                        ";
                // line 45
                echo twig_escape_filter($this->env, (isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")), "html", null, true);
                echo "                
                    </div>
\t\t\t";
            }
            // line 48
            echo "\t\t";
        }
        // line 49
        echo "\t\t
                ";
        // line 50
        if (array_key_exists("errors", $context)) {
            // line 51
            echo "                        ";
            if (((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")) != null)) {
                // line 52
                echo "                    <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">

                                        <ul> 
                                        ";
                // line 55
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 56
                    echo "                                                <li> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                    echo " </li>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                                        </ul>

                                    </div>
                ";
            }
            // line 62
            echo "\t\t";
        }
        // line 63
        echo "                    <h3> Général </h3>
                   
                        ";
        // line 65
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), 'form_start', array("attr" => array("id" => "demo-form2", "data-parsley-validate" => "", "class" => "form-horizontal form-label-left", "novalidate" => "")));
        echo "
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"incident[client]\">Client <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 71
        if (array_key_exists("client", $context)) {
            // line 72
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "client", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12", "disabled" => "true")));
            echo "
                                ";
        } else {
            // line 74
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "client", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
            echo " 
                           ";
        }
        // line 76
        echo "                        </div>
                      </div>
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 83
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "Titre", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
                        </div>
                      </div>
                      
                       <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Type de l'incident <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          <div id=\"gender\" class=\"btn-group\" data-toggle=\"buttons\">
                                ";
        // line 91
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "Type", array()), 'widget', array("attr" => array("class" => "iCheck-helper")));
        echo " 
                          </div>
                        </div>
                      </div>
                      

                      
                     
                      
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date Debut de l'incident
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                         <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non ";
        // line 106
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "ddiConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 107
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateDebut", array()), 'widget', array("attr" => array("class" => "")));
        echo " </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                      
                      
                       
                       
                       
                      
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Durée approximative de résolution 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           ";
        // line 123
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "dureeResolution", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12", "placeholder" => "Ex : 30 min, 1 heure, ...")));
        echo "   
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 133
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "dfiConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 134
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateFin", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "   </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                         

                         
                         

                         
                            <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Description du l'incident <span class=\"required\">*</span> 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 150
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "descTechnique", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                            
                             <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Notes internes
                        <div class=\"alert alert-info alert-dismissible fade in\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                        </button>
                       Ces notes ne seront pas prises en compte dans les diffusions.
                         </div>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 163
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "notes", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                             <hr class=\"half-rule\"/>
                <h3> Impacts </h3>     
                        <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Impact <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          ";
        // line 171
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "Impact", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo " 
                        </div>
                      </div>
                             
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Debut Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 181
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "ddimConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 182
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateDebutImpact", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "     </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                 <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 196
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "dfimConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 197
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateFinImpact", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "   </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                         
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Ressenti Utilisateur <span class=\"required\">*</span> 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 209
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "ressentiUtilisateur", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                             

                            
                             <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Les Applications/Metiers impactés <span class=\"required\">*</span> 
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          Application   ";
        // line 219
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "isApplicationImpact", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none", "id" => "tg-button")));
        echo "    Metier 
                                        
                        </div>
                        </div>
                            
                             <div class=\"form-group\" id=\"impactApplication\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Application 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 228
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "impactApplication", array()), 'widget', array("attr" => array("class" => "select2_multiple form-control")));
        echo "   
                        </div>
                      </div>
                            
                       
                        <div class=\"form-group\"  id=\"impactMetier\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Metier 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 237
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "impactMetier", array()), 'widget', array("attr" => array("class" => "select2_multiple form-control")));
        echo "   
                        </div>
                      </div>
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                      </div>
                     
                        ";
        // line 248
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), 'form_end');
        echo "
                    
                  </div>
                </div>
              </div>
            </div>

</div>




";
        
        $__internal_b782c08b208d79bbd0d22cb5230819a21e97f1a277740f5937bfaa5680388766->leave($__internal_b782c08b208d79bbd0d22cb5230819a21e97f1a277740f5937bfaa5680388766_prof);

        
        $__internal_abe1d02ea2edea8a9c93b76876b66be9caf8e90d86cd1fa3e63750438f36bd5a->leave($__internal_abe1d02ea2edea8a9c93b76876b66be9caf8e90d86cd1fa3e63750438f36bd5a_prof);

    }

    // line 262
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c6a6274026ffaea2a4fe059cc6196e69c0a87084f421601cde141f3734c66fc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c6a6274026ffaea2a4fe059cc6196e69c0a87084f421601cde141f3734c66fc3->enter($__internal_c6a6274026ffaea2a4fe059cc6196e69c0a87084f421601cde141f3734c66fc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_8cbd9f029c229153aa731ca5e208bd5c70211e9ec38c3c5f6c4312a1a90dd404 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8cbd9f029c229153aa731ca5e208bd5c70211e9ec38c3c5f6c4312a1a90dd404->enter($__internal_8cbd9f029c229153aa731ca5e208bd5c70211e9ec38c3c5f6c4312a1a90dd404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 263
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 264
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 266
        echo "      ";
        
        $__internal_8cbd9f029c229153aa731ca5e208bd5c70211e9ec38c3c5f6c4312a1a90dd404->leave($__internal_8cbd9f029c229153aa731ca5e208bd5c70211e9ec38c3c5f6c4312a1a90dd404_prof);

        
        $__internal_c6a6274026ffaea2a4fe059cc6196e69c0a87084f421601cde141f3734c66fc3->leave($__internal_c6a6274026ffaea2a4fe059cc6196e69c0a87084f421601cde141f3734c66fc3_prof);

    }

    public function getTemplateName()
    {
        return "/incident/incidentAdd.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  582 => 266,  526 => 264,  521 => 263,  512 => 262,  489 => 248,  475 => 237,  463 => 228,  451 => 219,  438 => 209,  423 => 197,  419 => 196,  402 => 182,  398 => 181,  385 => 171,  374 => 163,  358 => 150,  339 => 134,  335 => 133,  322 => 123,  303 => 107,  299 => 106,  281 => 91,  270 => 83,  261 => 76,  255 => 74,  249 => 72,  247 => 71,  238 => 65,  234 => 63,  231 => 62,  225 => 58,  216 => 56,  212 => 55,  207 => 52,  204 => 51,  202 => 50,  199 => 49,  196 => 48,  190 => 45,  187 => 44,  184 => 43,  182 => 42,  161 => 23,  152 => 22,  141 => 17,  91 => 15,  87 => 14,  84 => 13,  75 => 12,  64 => 19,  62 => 12,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}

{% block header %}
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  {% block stylesheets %}
      
    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}
      
     {% endblock %}

{% endblock %}

{% block body %}

<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Incident <small>Décrire l'incident en Français</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                    
                      {% if success is defined %}
                        {% if success != null %}
                    <div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                        {{  success }}                
                    </div>
\t\t\t{%endif %}
\t\t{%endif %}
\t\t
                {% if errors is defined %}
                        {% if errors != null %}
                    <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">

                                        <ul> 
                                        {% for error in errors %}
                                                <li> {{ error.message }} </li>
                                        {% endfor%}
                                        </ul>

                                    </div>
                {%endif %}
\t\t{%endif %}
                    <h3> Général </h3>
                   
                        {{ form_start(formDeclarerIncident, { 'attr' : { 'id' : 'demo-form2', 'data-parsley-validate': '', 'class' : 'form-horizontal form-label-left', 'novalidate': ''  } }) }}
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"incident[client]\">Client <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          {% if client is defined %}
                                {{ form_widget(formDeclarerIncident.client, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12',  'disabled' : 'true'} } )}}
                                {% else %}
                                {{ form_widget(formDeclarerIncident.client, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}} 
                           {% endif %}
                        </div>
                      </div>
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          {{ form_widget(formDeclarerIncident.Titre, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}}
                        </div>
                      </div>
                      
                       <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Type de l'incident <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          <div id=\"gender\" class=\"btn-group\" data-toggle=\"buttons\">
                                {{ form_widget(formDeclarerIncident.Type, { 'attr' : { 'class' : 'iCheck-helper'} } )}} 
                          </div>
                        </div>
                      </div>
                      

                      
                     
                      
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date Debut de l'incident
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                         <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non {{ form_widget(formDeclarerIncident.ddiConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateDebut, { 'attr' : { 'class' : ''} } )}} </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                      
                      
                       
                       
                       
                      
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Durée approximative de résolution 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           {{ form_widget(formDeclarerIncident.dureeResolution,  { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12','placeholder' : 'Ex : 30 min, 1 heure, ...'} } )}}   
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  {{ form_widget(formDeclarerIncident.dfiConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateFin, { 'attr' : { 'class' : '', 'required' : 'false'} } )}}   </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                         

                         
                         

                         
                            <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Description du l'incident <span class=\"required\">*</span> 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.descTechnique, { 'attr' : { 'class' : 'form-control', 'rows' : '4'} } )}}
                        </div>
                      </div>
                            
                             <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Notes internes
                        <div class=\"alert alert-info alert-dismissible fade in\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">×</span>
                        </button>
                       Ces notes ne seront pas prises en compte dans les diffusions.
                         </div>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.notes, { 'attr' : { 'class' : 'form-control', 'rows' : '4'} } )}}
                        </div>
                      </div>
                             <hr class=\"half-rule\"/>
                <h3> Impacts </h3>     
                        <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Impact <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          {{ form_widget(formDeclarerIncident.Impact, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}} 
                        </div>
                      </div>
                             
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Debut Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  {{ form_widget(formDeclarerIncident.ddimConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateDebutImpact, { 'attr' : { 'class' : '', 'required' : 'false'} } )}}     </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                 <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  {{ form_widget(formDeclarerIncident.dfimConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateFinImpact, { 'attr' : { 'class' : '', 'required' : 'false'} } )}}   </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                         
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Ressenti Utilisateur <span class=\"required\">*</span> 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.ressentiUtilisateur, { 'attr' : { 'class' : 'form-control', 'rows' : '4'} } )}}
                        </div>
                      </div>
                             

                            
                             <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Les Applications/Metiers impactés <span class=\"required\">*</span> 
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          Application   {{ form_widget(formDeclarerIncident.isApplicationImpact, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none', 'id':'tg-button'} } )}}    Metier 
                                        
                        </div>
                        </div>
                            
                             <div class=\"form-group\" id=\"impactApplication\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Application 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.impactApplication, { 'attr' : { 'class' : 'select2_multiple form-control'} } )}}   
                        </div>
                      </div>
                            
                       
                        <div class=\"form-group\"  id=\"impactMetier\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Metier 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.impactMetier, { 'attr' : { 'class' : 'select2_multiple form-control'} } )}}   
                        </div>
                      </div>
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                      </div>
                     
                        {{ form_end(formDeclarerIncident) }}
                    
                  </div>
                </div>
              </div>
            </div>

</div>




{% endblock %}

    {% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}", "/incident/incidentAdd.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\incident\\incidentAdd.html.twig");
    }
}
