<?php

/* loginTemplate.html.twig */
class __TwigTemplate_532f0fe434ab2774df6da58569ef0a44a060f65aed46b486fb5bac9d78d12d0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bd9cb3a8beff530d4b52220713d1f27e555cd3febcf9a84ec6dd6469c39e4685 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bd9cb3a8beff530d4b52220713d1f27e555cd3febcf9a84ec6dd6469c39e4685->enter($__internal_bd9cb3a8beff530d4b52220713d1f27e555cd3febcf9a84ec6dd6469c39e4685_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "loginTemplate.html.twig"));

        $__internal_0fba5992bd69b30e0bc0061d318e42b6e14a707a9e4e70597a9b0fcc84da755b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fba5992bd69b30e0bc0061d318e42b6e14a707a9e4e70597a9b0fcc84da755b->enter($__internal_0fba5992bd69b30e0bc0061d318e42b6e14a707a9e4e70597a9b0fcc84da755b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "loginTemplate.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
     ";
        // line 4
        $this->displayBlock('header', $context, $blocks);
        // line 23
        echo "  </head>

  <body class=\"login\">
      


      <header> 
        <div class=\"login_header\"> 
            <img class=\"logo_cds\" src=\"http://club-commerce-connecte.com/wp-content/uploads/2016/11/Cdiscount-Logo-2016-1.png\">
        </div>
      </header>
    <div>
      <a class=\"hiddenanchor\" id=\"signup\"></a>
      <a class=\"hiddenanchor\" id=\"signin\"></a>

      <div class=\"login_wrapper\">
         
        <div class=\"animate form login_form\">
                         ";
        // line 41
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "87487f8_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_87487f8_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/87487f8_login-logo_1.png");
            // line 42
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"logo\" class=\"login_logo\" />
                          ";
        } else {
            // asset "87487f8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_87487f8") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/87487f8.png");
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"logo\" class=\"login_logo\" />
                          ";
        }
        unset($context["asset_url"]);
        // line 44
        echo "          <section class=\"login_content\">
           ";
        // line 45
        $this->displayBlock('body', $context, $blocks);
        // line 48
        echo "          </section>
        </div>

      </div>
    </div>
        
  </body>
</html>
";
        
        $__internal_bd9cb3a8beff530d4b52220713d1f27e555cd3febcf9a84ec6dd6469c39e4685->leave($__internal_bd9cb3a8beff530d4b52220713d1f27e555cd3febcf9a84ec6dd6469c39e4685_prof);

        
        $__internal_0fba5992bd69b30e0bc0061d318e42b6e14a707a9e4e70597a9b0fcc84da755b->leave($__internal_0fba5992bd69b30e0bc0061d318e42b6e14a707a9e4e70597a9b0fcc84da755b_prof);

    }

    // line 4
    public function block_header($context, array $blocks = array())
    {
        $__internal_f162749b1b1780efff1b0f7040a96aaaf35cdc1a5f77b738ea5e51c89f96a6ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f162749b1b1780efff1b0f7040a96aaaf35cdc1a5f77b738ea5e51c89f96a6ba->enter($__internal_f162749b1b1780efff1b0f7040a96aaaf35cdc1a5f77b738ea5e51c89f96a6ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_9273ade91da2c16f5e9301b7ff49fdd01fc88f4935dd7d3a51d7f8a8846dc28e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9273ade91da2c16f5e9301b7ff49fdd01fc88f4935dd7d3a51d7f8a8846dc28e->enter($__internal_9273ade91da2c16f5e9301b7ff49fdd01fc88f4935dd7d3a51d7f8a8846dc28e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        echo "  
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    ";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "    
    ";
        
        $__internal_9273ade91da2c16f5e9301b7ff49fdd01fc88f4935dd7d3a51d7f8a8846dc28e->leave($__internal_9273ade91da2c16f5e9301b7ff49fdd01fc88f4935dd7d3a51d7f8a8846dc28e_prof);

        
        $__internal_f162749b1b1780efff1b0f7040a96aaaf35cdc1a5f77b738ea5e51c89f96a6ba->leave($__internal_f162749b1b1780efff1b0f7040a96aaaf35cdc1a5f77b738ea5e51c89f96a6ba_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b53ae2b9f16cac1c63b023a63f29a700fb1c95da574d9e50f23d614943647f0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b53ae2b9f16cac1c63b023a63f29a700fb1c95da574d9e50f23d614943647f0b->enter($__internal_b53ae2b9f16cac1c63b023a63f29a700fb1c95da574d9e50f23d614943647f0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_7715a76cd76c629cbf36e8e36d066e78f56b0d29534ccb4bd97428d3eb63c465 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7715a76cd76c629cbf36e8e36d066e78f56b0d29534ccb4bd97428d3eb63c465->enter($__internal_7715a76cd76c629cbf36e8e36d066e78f56b0d29534ccb4bd97428d3eb63c465_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        echo "      
    ";
        // line 16
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 17
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 19
        echo "      
    ";
        
        $__internal_7715a76cd76c629cbf36e8e36d066e78f56b0d29534ccb4bd97428d3eb63c465->leave($__internal_7715a76cd76c629cbf36e8e36d066e78f56b0d29534ccb4bd97428d3eb63c465_prof);

        
        $__internal_b53ae2b9f16cac1c63b023a63f29a700fb1c95da574d9e50f23d614943647f0b->leave($__internal_b53ae2b9f16cac1c63b023a63f29a700fb1c95da574d9e50f23d614943647f0b_prof);

    }

    // line 45
    public function block_body($context, array $blocks = array())
    {
        $__internal_e78781ca2a4bfa988ba2d6104015a174487794c2d7a31273d5a46a5dbae6cddc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e78781ca2a4bfa988ba2d6104015a174487794c2d7a31273d5a46a5dbae6cddc->enter($__internal_e78781ca2a4bfa988ba2d6104015a174487794c2d7a31273d5a46a5dbae6cddc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6376c14894cd51b9700e9b8a19ff0c8ca776e7a42cf31708002a3f5d591e81b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6376c14894cd51b9700e9b8a19ff0c8ca776e7a42cf31708002a3f5d591e81b7->enter($__internal_6376c14894cd51b9700e9b8a19ff0c8ca776e7a42cf31708002a3f5d591e81b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 46
        echo "
            ";
        
        $__internal_6376c14894cd51b9700e9b8a19ff0c8ca776e7a42cf31708002a3f5d591e81b7->leave($__internal_6376c14894cd51b9700e9b8a19ff0c8ca776e7a42cf31708002a3f5d591e81b7_prof);

        
        $__internal_e78781ca2a4bfa988ba2d6104015a174487794c2d7a31273d5a46a5dbae6cddc->leave($__internal_e78781ca2a4bfa988ba2d6104015a174487794c2d7a31273d5a46a5dbae6cddc_prof);

    }

    public function getTemplateName()
    {
        return "loginTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 46,  206 => 45,  195 => 19,  145 => 17,  141 => 16,  138 => 15,  129 => 14,  118 => 21,  116 => 14,  96 => 4,  78 => 48,  76 => 45,  73 => 44,  59 => 42,  55 => 41,  35 => 23,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
  <head>
     {% block header %}  
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    {% block stylesheets %}
      
    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}
      
    {% endblock %}
    
    {% endblock%}
  </head>

  <body class=\"login\">
      


      <header> 
        <div class=\"login_header\"> 
            <img class=\"logo_cds\" src=\"http://club-commerce-connecte.com/wp-content/uploads/2016/11/Cdiscount-Logo-2016-1.png\">
        </div>
      </header>
    <div>
      <a class=\"hiddenanchor\" id=\"signup\"></a>
      <a class=\"hiddenanchor\" id=\"signin\"></a>

      <div class=\"login_wrapper\">
         
        <div class=\"animate form login_form\">
                         {% image '@AppBundle/Resources/public/images/login-logo.png' %}
                          <img src=\"{{ asset_url }}\" alt=\"logo\" class=\"login_logo\" />
                          {% endimage %}
          <section class=\"login_content\">
           {% block body %}

            {% endblock %}
          </section>
        </div>

      </div>
    </div>
        
  </body>
</html>
", "loginTemplate.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\loginTemplate.html.twig");
    }
}
