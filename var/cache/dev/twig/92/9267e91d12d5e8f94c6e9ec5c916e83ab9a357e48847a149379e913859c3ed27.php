<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_e9fd9f6a14ba8106afcc1ccf894198f4e21557117857d712f27790394efb5aa7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c4feebec930af8818132d75038e51fdea7684af70241b91d95c3717d0d8320c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c4feebec930af8818132d75038e51fdea7684af70241b91d95c3717d0d8320c->enter($__internal_3c4feebec930af8818132d75038e51fdea7684af70241b91d95c3717d0d8320c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c4feebec930af8818132d75038e51fdea7684af70241b91d95c3717d0d8320c->leave($__internal_3c4feebec930af8818132d75038e51fdea7684af70241b91d95c3717d0d8320c_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f84e9aac188bc37a4fd251a49981ba1f5bd2b4f7485a3cbbde089bca7f6792c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f84e9aac188bc37a4fd251a49981ba1f5bd2b4f7485a3cbbde089bca7f6792c0->enter($__internal_f84e9aac188bc37a4fd251a49981ba1f5bd2b4f7485a3cbbde089bca7f6792c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_f84e9aac188bc37a4fd251a49981ba1f5bd2b4f7485a3cbbde089bca7f6792c0->leave($__internal_f84e9aac188bc37a4fd251a49981ba1f5bd2b4f7485a3cbbde089bca7f6792c0_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 6,  34 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/check_email.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\check_email.html.twig");
    }
}
