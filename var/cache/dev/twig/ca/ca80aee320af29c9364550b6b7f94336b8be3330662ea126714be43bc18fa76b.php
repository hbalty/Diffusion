<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_b8c12c19aa7ac2176661534c53eeb59563de274630e541f132bcad2bb4ca8ea1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2873844809b8da7be0f969b6258e0acd3798da7029a8bf570fc69ec3f1e662d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2873844809b8da7be0f969b6258e0acd3798da7029a8bf570fc69ec3f1e662d->enter($__internal_c2873844809b8da7be0f969b6258e0acd3798da7029a8bf570fc69ec3f1e662d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_c2873844809b8da7be0f969b6258e0acd3798da7029a8bf570fc69ec3f1e662d->leave($__internal_c2873844809b8da7be0f969b6258e0acd3798da7029a8bf570fc69ec3f1e662d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/hidden_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}
