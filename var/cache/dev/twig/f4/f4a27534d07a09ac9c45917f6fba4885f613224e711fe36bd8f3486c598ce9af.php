<?php

/* /incident/incidentClone.html.twig */
class __TwigTemplate_da3f4ebe9ab68048ceb73d5cd996606efabac12a48b279efb23bfd9106196d2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "/incident/incidentClone.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31477f9a570c2b4828f8acb8e1959749e1d26dc4d2094f3206958e7231c3b9e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31477f9a570c2b4828f8acb8e1959749e1d26dc4d2094f3206958e7231c3b9e6->enter($__internal_31477f9a570c2b4828f8acb8e1959749e1d26dc4d2094f3206958e7231c3b9e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/incident/incidentClone.html.twig"));

        $__internal_0e942ada6a1c5731969076480598f57ec244f39ef61430a2de7f0bc8b174f8e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e942ada6a1c5731969076480598f57ec244f39ef61430a2de7f0bc8b174f8e0->enter($__internal_0e942ada6a1c5731969076480598f57ec244f39ef61430a2de7f0bc8b174f8e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/incident/incidentClone.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_31477f9a570c2b4828f8acb8e1959749e1d26dc4d2094f3206958e7231c3b9e6->leave($__internal_31477f9a570c2b4828f8acb8e1959749e1d26dc4d2094f3206958e7231c3b9e6_prof);

        
        $__internal_0e942ada6a1c5731969076480598f57ec244f39ef61430a2de7f0bc8b174f8e0->leave($__internal_0e942ada6a1c5731969076480598f57ec244f39ef61430a2de7f0bc8b174f8e0_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_44ae56e940166e98296c9f75ac47ceb5fc12edf2a3b3bdd34e8a12eaad92f62b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44ae56e940166e98296c9f75ac47ceb5fc12edf2a3b3bdd34e8a12eaad92f62b->enter($__internal_44ae56e940166e98296c9f75ac47ceb5fc12edf2a3b3bdd34e8a12eaad92f62b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_0fe85d8dbb33a42cc6b6f24b8b88d1263abc2ff84a58994ebd73d69543b29cd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0fe85d8dbb33a42cc6b6f24b8b88d1263abc2ff84a58994ebd73d69543b29cd9->enter($__internal_0fe85d8dbb33a42cc6b6f24b8b88d1263abc2ff84a58994ebd73d69543b29cd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "
";
        
        $__internal_0fe85d8dbb33a42cc6b6f24b8b88d1263abc2ff84a58994ebd73d69543b29cd9->leave($__internal_0fe85d8dbb33a42cc6b6f24b8b88d1263abc2ff84a58994ebd73d69543b29cd9_prof);

        
        $__internal_44ae56e940166e98296c9f75ac47ceb5fc12edf2a3b3bdd34e8a12eaad92f62b->leave($__internal_44ae56e940166e98296c9f75ac47ceb5fc12edf2a3b3bdd34e8a12eaad92f62b_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1c02f5956d18dabcbbb8ce99278af73cad72c91653adeaa27f0f01278513f5d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c02f5956d18dabcbbb8ce99278af73cad72c91653adeaa27f0f01278513f5d3->enter($__internal_1c02f5956d18dabcbbb8ce99278af73cad72c91653adeaa27f0f01278513f5d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_cc3b2b6a6e77c7f94f6944e6e0c635d27c352904f4febde06b9e69e3e29a1b32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc3b2b6a6e77c7f94f6944e6e0c635d27c352904f4febde06b9e69e3e29a1b32->enter($__internal_cc3b2b6a6e77c7f94f6944e6e0c635d27c352904f4febde06b9e69e3e29a1b32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "      
    ";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "      
     ";
        
        $__internal_cc3b2b6a6e77c7f94f6944e6e0c635d27c352904f4febde06b9e69e3e29a1b32->leave($__internal_cc3b2b6a6e77c7f94f6944e6e0c635d27c352904f4febde06b9e69e3e29a1b32_prof);

        
        $__internal_1c02f5956d18dabcbbb8ce99278af73cad72c91653adeaa27f0f01278513f5d3->leave($__internal_1c02f5956d18dabcbbb8ce99278af73cad72c91653adeaa27f0f01278513f5d3_prof);

    }

    // line 22
    public function block_body($context, array $blocks = array())
    {
        $__internal_b48d81debd3d9f0436679cf3e8795c1bbd072e43b20244c157eba5051f4011a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b48d81debd3d9f0436679cf3e8795c1bbd072e43b20244c157eba5051f4011a1->enter($__internal_b48d81debd3d9f0436679cf3e8795c1bbd072e43b20244c157eba5051f4011a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_85cd5f414568a4a507913b55b3f9679e2538b316d23366bf5087b89b0970f1dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_85cd5f414568a4a507913b55b3f9679e2538b316d23366bf5087b89b0970f1dc->enter($__internal_85cd5f414568a4a507913b55b3f9679e2538b316d23366bf5087b89b0970f1dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 23
        echo "
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Incident <small>Décrire l'incident en Français</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                              
                      ";
        // line 42
        if (array_key_exists("success", $context)) {
            // line 43
            echo "                        ";
            if (((isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")) != null)) {
                // line 44
                echo "                    <div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                        ";
                // line 45
                echo twig_escape_filter($this->env, (isset($context["success"]) ? $context["success"] : $this->getContext($context, "success")), "html", null, true);
                echo "                
                    </div>
\t\t\t";
            }
            // line 48
            echo "\t\t";
        }
        // line 49
        echo "\t\t
                ";
        // line 50
        if (array_key_exists("errors", $context)) {
            // line 51
            echo "                        ";
            if (((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")) != null)) {
                // line 52
                echo "                    <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">

                                        <ul> 
                                        ";
                // line 55
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 56
                    echo "                                                <li> ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                    echo " </li>
                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                                        </ul>

                                    </div>
                ";
            }
            // line 62
            echo "        ";
        }
        // line 63
        echo "        
                    <h3> Général </h3>
                   
                        ";
        // line 66
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), 'form_start', array("attr" => array("id" => "demo-form2", "data-parsley-validate" => "", "class" => "form-horizontal form-label-left", "novalidate" => "")));
        echo "
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">Client <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 72
        if (array_key_exists("client", $context)) {
            // line 73
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "client", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12", "disabled" => "true")));
            echo "
                                ";
        } else {
            // line 75
            echo "                                ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "client", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
            echo " 
                           ";
        }
        // line 77
        echo "                        </div>
                      </div>
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          ";
        // line 84
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "Titre", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo "
                        </div>
                      </div>
                      
                       <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Type de l'incident</label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          <div id=\"gender\" class=\"btn-group\" data-toggle=\"buttons\">
                                ";
        // line 92
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "Type", array()), 'widget', array("attr" => array("class" => "iCheck-helper")));
        echo " 
                          </div>
                        </div>
                      </div>
                      

                      
                     
                      
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date Debut de l'incident
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                         <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non ";
        // line 107
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "ddiConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 108
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateDebut", array()), 'widget', array("attr" => array("class" => "")));
        echo " </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                      
                      
                       
                       
                       
                      
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Durée approximative de résolution 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           ";
        // line 124
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "dureeResolution", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12", "placeholder" => "Ex : 30 min, 1 heure, ...")));
        echo "   
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 134
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "dfiConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 135
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateFin", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "   </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                         

                         
                         

                         
                            <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Description du l'incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 151
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "descTechnique", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                            
                             <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Notes internes
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 159
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "notes", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                             <hr class=\"half-rule\"/>
                <h3> Impacts </h3>     
                        <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Impact <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          ";
        // line 167
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "Impact", array()), 'widget', array("attr" => array("class" => "form-control col-md-7 col-xs-12")));
        echo " 
                        </div>
                      </div>
                             
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Debut Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 177
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "ddimConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 178
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateDebutImpact", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "     </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                 <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  ";
        // line 192
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "dfimConnue", array()), 'widget', array("attr" => array("class" => "js-switch", "data-switchery" => "true", "style" => "display : none")));
        echo "  Oui   </td>
                                <td> ";
        // line 193
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "DateFinImpact", array()), 'widget', array("attr" => array("class" => "", "required" => "false")));
        echo "   </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                         
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Ressenti Utilisateur
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 205
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "ressentiUtilisateur", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => "4")));
        echo "
                        </div>
                      </div>
                             

                            
                             <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Type impact
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                              <input type=\"checkbox\" id=\"tg-button\" class=\"js-switch\" checked=\"\" data-switchery=\"true\" style=\"display: none;\"> Metier 

                        </div>
                        </div>
                            
                             <div class=\"form-group\" id=\"impactApplication\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Application 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 224
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "impactApplication", array()), 'widget', array("attr" => array("class" => "select2_multiple form-control")));
        echo "   
                        </div>
                      </div>
                            
                       
                                <div class=\"form-group\"  id=\"impactMetier\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Metier 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            ";
        // line 233
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), "impactMetier", array()), 'widget', array("attr" => array("class" => "select2_multiple form-control")));
        echo "   
                        </div>
                      </div>
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                      </div>
                     
                        ";
        // line 244
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["formDeclarerIncident"]) ? $context["formDeclarerIncident"] : $this->getContext($context, "formDeclarerIncident")), 'form_end');
        echo "
                    
                  </div>
                </div>
              </div>
            </div>

</div>




";
        
        $__internal_85cd5f414568a4a507913b55b3f9679e2538b316d23366bf5087b89b0970f1dc->leave($__internal_85cd5f414568a4a507913b55b3f9679e2538b316d23366bf5087b89b0970f1dc_prof);

        
        $__internal_b48d81debd3d9f0436679cf3e8795c1bbd072e43b20244c157eba5051f4011a1->leave($__internal_b48d81debd3d9f0436679cf3e8795c1bbd072e43b20244c157eba5051f4011a1_prof);

    }

    // line 258
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_587f2ebccfe24a86244820ccd7cc0b6378c052ae81272ff23e917276d94947cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_587f2ebccfe24a86244820ccd7cc0b6378c052ae81272ff23e917276d94947cf->enter($__internal_587f2ebccfe24a86244820ccd7cc0b6378c052ae81272ff23e917276d94947cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_f4e898acd5ea159ffbdfe871f0810c9ba814b3cb14982509d06cd61d4a4e628f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4e898acd5ea159ffbdfe871f0810c9ba814b3cb14982509d06cd61d4a4e628f->enter($__internal_f4e898acd5ea159ffbdfe871f0810c9ba814b3cb14982509d06cd61d4a4e628f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 259
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 260
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 262
        echo "      ";
        
        $__internal_f4e898acd5ea159ffbdfe871f0810c9ba814b3cb14982509d06cd61d4a4e628f->leave($__internal_f4e898acd5ea159ffbdfe871f0810c9ba814b3cb14982509d06cd61d4a4e628f_prof);

        
        $__internal_587f2ebccfe24a86244820ccd7cc0b6378c052ae81272ff23e917276d94947cf->leave($__internal_587f2ebccfe24a86244820ccd7cc0b6378c052ae81272ff23e917276d94947cf_prof);

    }

    public function getTemplateName()
    {
        return "/incident/incidentClone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  575 => 262,  519 => 260,  514 => 259,  505 => 258,  482 => 244,  468 => 233,  456 => 224,  434 => 205,  419 => 193,  415 => 192,  398 => 178,  394 => 177,  381 => 167,  370 => 159,  359 => 151,  340 => 135,  336 => 134,  323 => 124,  304 => 108,  300 => 107,  282 => 92,  271 => 84,  262 => 77,  256 => 75,  250 => 73,  248 => 72,  239 => 66,  234 => 63,  231 => 62,  225 => 58,  216 => 56,  212 => 55,  207 => 52,  204 => 51,  202 => 50,  199 => 49,  196 => 48,  190 => 45,  187 => 44,  184 => 43,  182 => 42,  161 => 23,  152 => 22,  141 => 17,  91 => 15,  87 => 14,  84 => 13,  75 => 12,  64 => 19,  62 => 12,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}

{% block header %}
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js\"> </script>
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.css\"/>
  {% block stylesheets %}
      
    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}
      
     {% endblock %}

{% endblock %}

{% block body %}

<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2>Ajouter Incident <small>Décrire l'incident en Français</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>
                              
                      {% if success is defined %}
                        {% if success != null %}
                    <div class=\"alert alert-success alert-dismissible fade in\" role=\"alert\">
                        {{  success }}                
                    </div>
\t\t\t{%endif %}
\t\t{%endif %}
\t\t
                {% if errors is defined %}
                        {% if errors != null %}
                    <div class=\"alert alert-danger alert-dismissible fade in\" role=\"alert\">

                                        <ul> 
                                        {% for error in errors %}
                                                <li> {{ error.message }} </li>
                                        {% endfor%}
                                        </ul>

                                    </div>
                {%endif %}
        {%endif %}
        
                    <h3> Général </h3>
                   
                        {{ form_start(formDeclarerIncident, { 'attr' : { 'id' : 'demo-form2', 'data-parsley-validate': '', 'class' : 'form-horizontal form-label-left', 'novalidate': ''  } }) }}
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"first-name\">Client <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          {% if client is defined %}
                                {{ form_widget(formDeclarerIncident.client, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12',  'disabled' : 'true'} } )}}
                                {% else %}
                                {{ form_widget(formDeclarerIncident.client, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}} 
                           {% endif %}
                        </div>
                      </div>
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"last-name\">Titre <span class=\"required\">*</span>
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          
                          {{ form_widget(formDeclarerIncident.Titre, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}}
                        </div>
                      </div>
                      
                       <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Type de l'incident</label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          <div id=\"gender\" class=\"btn-group\" data-toggle=\"buttons\">
                                {{ form_widget(formDeclarerIncident.Type, { 'attr' : { 'class' : 'iCheck-helper'} } )}} 
                          </div>
                        </div>
                      </div>
                      

                      
                     
                      
                      <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\">Date Debut de l'incident
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                         <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non {{ form_widget(formDeclarerIncident.ddiConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateDebut, { 'attr' : { 'class' : ''} } )}} </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                      
                      
                       
                       
                       
                      
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Durée approximative de résolution 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           {{ form_widget(formDeclarerIncident.dureeResolution,  { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12','placeholder' : 'Ex : 30 min, 1 heure, ...'} } )}}   
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                           <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  {{ form_widget(formDeclarerIncident.dfiConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateFin, { 'attr' : { 'class' : '', 'required' : 'false'} } )}}   </td>
                                
                                </tr>
                        </table>
                        </div>
                      </div>
                         

                         
                         

                         
                            <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Description du l'incident 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.descTechnique, { 'attr' : { 'class' : 'form-control', 'rows' : '4'} } )}}
                        </div>
                      </div>
                            
                             <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Notes internes
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.notes, { 'attr' : { 'class' : 'form-control', 'rows' : '4'} } )}}
                        </div>
                      </div>
                             <hr class=\"half-rule\"/>
                <h3> Impacts </h3>     
                        <div class=\"form-group\">
                        <label for=\"middle-name\" class=\"control-label col-md-3 col-sm-3 col-xs-12\">Impact <span class=\"required\">*</span> </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                          {{ form_widget(formDeclarerIncident.Impact, { 'attr' : { 'class' : 'form-control col-md-7 col-xs-12'} } )}} 
                        </div>
                      </div>
                             
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Debut Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  {{ form_widget(formDeclarerIncident.ddimConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateDebutImpact, { 'attr' : { 'class' : '', 'required' : 'false'} } )}}     </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                        
                         <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Date Fin Impact
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                                 <table>
                                <tr>
                                <td style=\"width: 150px;\"> Connue : Non  {{ form_widget(formDeclarerIncident.dfimConnue, { 'attr' : { 'class' : 'js-switch','data-switchery' : 'true', 'style' : 'display : none'} } )}}  Oui   </td>
                                <td> {{ form_widget(formDeclarerIncident.DateFinImpact, { 'attr' : { 'class' : '', 'required' : 'false'} } )}}   </td>
                                
                                </tr>
                        </table>
                          
                        </div>
                      </div>
                         
                        <div class=\"form-group\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Ressenti Utilisateur
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.ressentiUtilisateur, { 'attr' : { 'class' : 'form-control', 'rows' : '4'} } )}}
                        </div>
                      </div>
                             

                            
                             <div class=\"form-group\">
                                <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Type impact
                                </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                              <input type=\"checkbox\" id=\"tg-button\" class=\"js-switch\" checked=\"\" data-switchery=\"true\" style=\"display: none;\"> Metier 

                        </div>
                        </div>
                            
                             <div class=\"form-group\" id=\"impactApplication\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Application 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.impactApplication, { 'attr' : { 'class' : 'select2_multiple form-control'} } )}}   
                        </div>
                      </div>
                            
                       
                                <div class=\"form-group\"  id=\"impactMetier\">
                        <label class=\"control-label col-md-3 col-sm-3 col-xs-12\"> Impact Metier 
                        </label>
                        <div class=\"col-md-6 col-sm-6 col-xs-12\">
                            {{ form_widget(formDeclarerIncident.impactMetier, { 'attr' : { 'class' : 'select2_multiple form-control'} } )}}   
                        </div>
                      </div>
                      
                      <div class=\"ln_solid\"></div>
                      <div class=\"form-group\">
                        <div class=\"col-md-6 col-sm-6 col-xs-12 col-md-offset-3\">
                          <input type=\"submit\" class=\"btn btn-success\">
                        </div>
                      </div>
                     
                        {{ form_end(formDeclarerIncident) }}
                    
                  </div>
                </div>
              </div>
            </div>

</div>




{% endblock %}

    {% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}", "/incident/incidentClone.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\incident\\incidentClone.html.twig");
    }
}
