<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_50f68c4c874055ada00dd21e85884c5123ccf51a98516d783e8c6722545de1e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec1ade65d18732e78aa6bd2f67bce376eede52cd3b5fb037acfd4dc5350cfd7f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec1ade65d18732e78aa6bd2f67bce376eede52cd3b5fb037acfd4dc5350cfd7f->enter($__internal_ec1ade65d18732e78aa6bd2f67bce376eede52cd3b5fb037acfd4dc5350cfd7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : null), "message" => (isset($context["status_text"]) ? $context["status_text"] : null))));
        echo "
";
        
        $__internal_ec1ade65d18732e78aa6bd2f67bce376eede52cd3b5fb037acfd4dc5350cfd7f->leave($__internal_ec1ade65d18732e78aa6bd2f67bce376eede52cd3b5fb037acfd4dc5350cfd7f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/error.json.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.json.twig");
    }
}
