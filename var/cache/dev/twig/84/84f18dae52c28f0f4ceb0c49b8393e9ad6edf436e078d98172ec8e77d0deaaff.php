<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_158cb271feb4e1d56af631cbadc3cb6bc4d6bba4d020a417af6b57a31c8f5f87 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae043b087d404c6cb461b86537eabc0ffe5eff20e0391e64bdda709e1aa0e39d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae043b087d404c6cb461b86537eabc0ffe5eff20e0391e64bdda709e1aa0e39d->enter($__internal_ae043b087d404c6cb461b86537eabc0ffe5eff20e0391e64bdda709e1aa0e39d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        // line 1
        echo "

";
        // line 3
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_ae043b087d404c6cb461b86537eabc0ffe5eff20e0391e64bdda709e1aa0e39d->leave($__internal_ae043b087d404c6cb461b86537eabc0ffe5eff20e0391e64bdda709e1aa0e39d_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_d31d17cc3ec8ff0f3231004d7f592bd04bdcf03a0ccecc45c97946b38c29a0ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d31d17cc3ec8ff0f3231004d7f592bd04bdcf03a0ccecc45c97946b38c29a0ab->enter($__internal_d31d17cc3ec8ff0f3231004d7f592bd04bdcf03a0ccecc45c97946b38c29a0ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        echo "    ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_d31d17cc3ec8ff0f3231004d7f592bd04bdcf03a0ccecc45c97946b38c29a0ab->leave($__internal_d31d17cc3ec8ff0f3231004d7f592bd04bdcf03a0ccecc45c97946b38c29a0ab_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  39 => 4,  27 => 3,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Security/login.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login.html.twig");
    }
}
