<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_1f3abf8e969739df2bb411aaf393d13ad1d7bdd7200c9bd385647a4d221c25f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93c2d5f7af19a944a5c0640d148a6f81c710cc0705b0faa0bceae013924335ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93c2d5f7af19a944a5c0640d148a6f81c710cc0705b0faa0bceae013924335ff->enter($__internal_93c2d5f7af19a944a5c0640d148a6f81c710cc0705b0faa0bceae013924335ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_93c2d5f7af19a944a5c0640d148a6f81c710cc0705b0faa0bceae013924335ff->leave($__internal_93c2d5f7af19a944a5c0640d148a6f81c710cc0705b0faa0bceae013924335ff_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/attributes.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\attributes.html.php");
    }
}
