<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_bb4c5b1d932ac7eb02b2521123e27089b5c080aff7a786b6dd03db2a8c0ad89b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2e5f6bb8239464d36a90f599a041fcbd7672801c6f1e96b9e6ec161b39289060 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e5f6bb8239464d36a90f599a041fcbd7672801c6f1e96b9e6ec161b39289060->enter($__internal_2e5f6bb8239464d36a90f599a041fcbd7672801c6f1e96b9e6ec161b39289060_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2e5f6bb8239464d36a90f599a041fcbd7672801c6f1e96b9e6ec161b39289060->leave($__internal_2e5f6bb8239464d36a90f599a041fcbd7672801c6f1e96b9e6ec161b39289060_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_abcae03a3f2a2a0703efb395063926da0ebeda616a614d62c3b415d7914e0fd3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abcae03a3f2a2a0703efb395063926da0ebeda616a614d62c3b415d7914e0fd3->enter($__internal_abcae03a3f2a2a0703efb395063926da0ebeda616a614d62c3b415d7914e0fd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_abcae03a3f2a2a0703efb395063926da0ebeda616a614d62c3b415d7914e0fd3->leave($__internal_abcae03a3f2a2a0703efb395063926da0ebeda616a614d62c3b415d7914e0fd3_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_bb04dc74a6b176099b5b5ce9988cc22e32a05e62c97c277b2c3d8540e39fe4fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb04dc74a6b176099b5b5ce9988cc22e32a05e62c97c277b2c3d8540e39fe4fd->enter($__internal_bb04dc74a6b176099b5b5ce9988cc22e32a05e62c97c277b2c3d8540e39fe4fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_bb04dc74a6b176099b5b5ce9988cc22e32a05e62c97c277b2c3d8540e39fe4fd->leave($__internal_bb04dc74a6b176099b5b5ce9988cc22e32a05e62c97c277b2c3d8540e39fe4fd_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
