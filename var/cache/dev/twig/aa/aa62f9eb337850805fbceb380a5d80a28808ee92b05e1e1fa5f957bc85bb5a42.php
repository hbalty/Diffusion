<?php

/* @LdapTools/Collector/ldaptools.html.twig */
class __TwigTemplate_1f2b9ba68e63bda4094b35f0385476d16cc0e9653b53d0a124eab93fc71a0886 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "@LdapTools/Collector/ldaptools.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e21715234345c8549dba21310b96f8d646dbedc95a79eeb9194aed2b32f21252 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e21715234345c8549dba21310b96f8d646dbedc95a79eeb9194aed2b32f21252->enter($__internal_e21715234345c8549dba21310b96f8d646dbedc95a79eeb9194aed2b32f21252_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@LdapTools/Collector/ldaptools.html.twig"));

        $__internal_969b2c9735653153a05ef8d20799e30ec9fefa20ba65834b77705487e87d2ba3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_969b2c9735653153a05ef8d20799e30ec9fefa20ba65834b77705487e87d2ba3->enter($__internal_969b2c9735653153a05ef8d20799e30ec9fefa20ba65834b77705487e87d2ba3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@LdapTools/Collector/ldaptools.html.twig"));

        // line 3
        $context["helper"] = $this;
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e21715234345c8549dba21310b96f8d646dbedc95a79eeb9194aed2b32f21252->leave($__internal_e21715234345c8549dba21310b96f8d646dbedc95a79eeb9194aed2b32f21252_prof);

        
        $__internal_969b2c9735653153a05ef8d20799e30ec9fefa20ba65834b77705487e87d2ba3->leave($__internal_969b2c9735653153a05ef8d20799e30ec9fefa20ba65834b77705487e87d2ba3_prof);

    }

    // line 5
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_d360f5a6028b0e2db703465202ae5b2a1cee00e4c5412857492f24207d121e39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d360f5a6028b0e2db703465202ae5b2a1cee00e4c5412857492f24207d121e39->enter($__internal_d360f5a6028b0e2db703465202ae5b2a1cee00e4c5412857492f24207d121e39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_cbc7ca012d61d9c816d873439d05e92b028761495e41c474bd9d3ede1b01c3ed = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cbc7ca012d61d9c816d873439d05e92b028761495e41c474bd9d3ede1b01c3ed->enter($__internal_cbc7ca012d61d9c816d873439d05e92b028761495e41c474bd9d3ede1b01c3ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 6
        echo "    ";
        $context["profiler_markup_version"] = ((array_key_exists("profiler_markup_version", $context)) ? (_twig_default_filter((isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")), 1)) : (1));
        // line 7
        echo "    ";
        ob_start();
        // line 8
        echo "        ";
        if (((isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")) == 1)) {
            // line 9
            echo "            <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTFCMEY0N0Q5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCMEY0N0U5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MUIwRjQ3Qjk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5MUIwRjQ3Qzk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PizcUhYAAASiSURBVHjahFbJT9tXEB4v2OxrKfsuKAhMALOIAwGL5AAhRWovFKqq6oV/gRtcqhyiSrlFQiiISw+gSgEBt0gQQIgDixCrjFkEZhP7ZhZjd75pnmUcQp80GP9+b2a++eab96xva2sjtVwuF93d3Rl1Op0pMDCwOCgoKMNoNH6n1+vVO8fFxYX16upq6fb2dkar1e7gnUajoW8tPf643W5ih+/DwsJ+T09P/zk2NvYZBzY6nU6C4T0MwQwGgyQ7Ozuzr6+vD29ubn7gEJ/w7tEEX5wbLRbLXzExMbGLi4s0PDxMu7u7dH19Tff39xIcixGTv78/hYaGUnJyckJOTk5jeXl548jIyN87Ozu/cZJ73wS6ioqKF3V1dR+5/ODOzk5aWFig8/NzCYqATJcgh+F/oL+8vCRGTtPT03RyckI1NTUmrsbJLHz2pUvLaF4lJCRQT0+PoGXu6Sle8RyJmEHZOzMzQ7Ozs5SUlPQj/H2Xlpt2dnh4SFVVVegD3dzcCMqnFqpDb1BJYmIiZWVlgdJzVPxVDzirq6+vj6qrq6m5uZmmpqZobW2NTk9PJRHMuxrVh7i4OMrNzSXug1RxcHDg+qK0hwkYgXFra4v6+/spPj6eMjMzqaioSDayHKUqNBu0gRaWrnyCDm4s9fb2SiURERF+ZrNZxwAe8KThps5yc01whBNKR7CQkBDhOCAgQChBFUjKlEpiJMUzRVdlZeVlbW3tDxzT/qACPz+/z6WlpSaUiQWdwxGoEExJVNGlVMV+8j0/P5/Cw8OlF7x+YnvP5vTIlF+aWQEWbIQzkCn9q2bjE9oHhQAAGeMZ+ra3t0fd3d00ODhomJubqykoKCjkyv9hN6FKz5zpJiYmwCHxFFNZWZk4Az0SIRgC5+XleRrOUyxg5ufnqauri4KDg+X52NgYALxubW39lb9/UJN8p1BNTk6KSuAAxKCBEUni1dVVT0WRkZGE2eno6JCKlDzhx1XQ0dHR86ioKEmgLSkpeQak2ITN4NfhcIhCEBRJ7Pb/+gbUMMwNfADCe2bwPxTGtu+RNVMSD6V46xflKr3jHVTiO9nY39TUJO8hCIDCkDY0NLi5EhvCCChWUGJhYaEFG3CuIJhKAsfU1FQpHc4qCZID7fj4ONXX18tsoE8cHIk1AwMDr9LS0irZ76MuIyPDwiVXZWdny5BBcgiEASsuLhb+MbXokaoEwRAUNOLkjY6Olr6Mjo7KwHIfNCsrK2lQmZ5pcEMVQ0NDMlwIhgbyCSn87+/vC1o+niUB0KMyHOdQHCpsaWkRWYN/UAZbWlpCopdov79qMBq3vLwsRzaQAiGCHx8fk81mo+3tbdrY2PA8R/MBhqdYmg9AaiGh1WoN13pPnTqKUS4o8j7gsPBMTbNSFY4N0Ksm3jsWH4AB8Gxns3ofxepieewu8F1IiiFVJ6kyiII/DbhIN9kq2N6w/QLKEOj/7gSVEH2BMFgspO5l+EIEfPgtqJt6j+0Ptj/ZXrNjHTesGs1/6heDulZNJtNYe3v7e/6ubhwNP3fIjwEfHxtvesdI3qWkpLzlXxlm/q57KgFr3c4CaQXNj4H5V4ABAFqBpDvjWvRIAAAAAElFTkSuQmCC\" />
            <span class=\"sf-toolbar-value sf-toolbar-status ";
            // line 10
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) > 10)) {
                echo "sf-toolbar-status-yellow";
            }
            echo "\">";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())), "html", null, true);
            echo "</span>
            ";
            // line 11
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) > 0)) {
                // line 12
                echo "                <span class=\"sf-toolbar-info-piece-additional-detail\">in ";
                echo twig_escape_filter($this->env, twig_round($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "time", array())), "html", null, true);
                echo " ms</span>
            ";
            }
            // line 14
            echo "            ";
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())) > 0)) {
                // line 15
                echo "                <span class=\"sf-toolbar-info-piece-additional sf-toolbar-status sf-toolbar-status-red\">";
                echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())), "html", null, true);
                echo "</span>
            ";
            }
            // line 17
            echo "        ";
        } elseif (((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) > 0) || (twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())) > 0))) {
            // line 18
            echo "            ";
            $context["status"] = (((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())) > 0)) ? ("red") : ((((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) > 10)) ? ("yellow") : (""))));
            // line 19
            echo "
            <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTFCMEY0N0Q5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCMEY0N0U5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MUIwRjQ3Qjk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5MUIwRjQ3Qzk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PizcUhYAAASiSURBVHjahFbJT9tXEB4v2OxrKfsuKAhMALOIAwGL5AAhRWovFKqq6oV/gRtcqhyiSrlFQiiISw+gSgEBt0gQQIgDixCrjFkEZhP7ZhZjd75pnmUcQp80GP9+b2a++eab96xva2sjtVwuF93d3Rl1Op0pMDCwOCgoKMNoNH6n1+vVO8fFxYX16upq6fb2dkar1e7gnUajoW8tPf643W5ih+/DwsJ+T09P/zk2NvYZBzY6nU6C4T0MwQwGgyQ7Ozuzr6+vD29ubn7gEJ/w7tEEX5wbLRbLXzExMbGLi4s0PDxMu7u7dH19Tff39xIcixGTv78/hYaGUnJyckJOTk5jeXl548jIyN87Ozu/cZJ73wS6ioqKF3V1dR+5/ODOzk5aWFig8/NzCYqATJcgh+F/oL+8vCRGTtPT03RyckI1NTUmrsbJLHz2pUvLaF4lJCRQT0+PoGXu6Sle8RyJmEHZOzMzQ7Ozs5SUlPQj/H2Xlpt2dnh4SFVVVegD3dzcCMqnFqpDb1BJYmIiZWVlgdJzVPxVDzirq6+vj6qrq6m5uZmmpqZobW2NTk9PJRHMuxrVh7i4OMrNzSXug1RxcHDg+qK0hwkYgXFra4v6+/spPj6eMjMzqaioSDayHKUqNBu0gRaWrnyCDm4s9fb2SiURERF+ZrNZxwAe8KThps5yc01whBNKR7CQkBDhOCAgQChBFUjKlEpiJMUzRVdlZeVlbW3tDxzT/qACPz+/z6WlpSaUiQWdwxGoEExJVNGlVMV+8j0/P5/Cw8OlF7x+YnvP5vTIlF+aWQEWbIQzkCn9q2bjE9oHhQAAGeMZ+ra3t0fd3d00ODhomJubqykoKCjkyv9hN6FKz5zpJiYmwCHxFFNZWZk4Az0SIRgC5+XleRrOUyxg5ufnqauri4KDg+X52NgYALxubW39lb9/UJN8p1BNTk6KSuAAxKCBEUni1dVVT0WRkZGE2eno6JCKlDzhx1XQ0dHR86ioKEmgLSkpeQak2ITN4NfhcIhCEBRJ7Pb/+gbUMMwNfADCe2bwPxTGtu+RNVMSD6V46xflKr3jHVTiO9nY39TUJO8hCIDCkDY0NLi5EhvCCChWUGJhYaEFG3CuIJhKAsfU1FQpHc4qCZID7fj4ONXX18tsoE8cHIk1AwMDr9LS0irZ76MuIyPDwiVXZWdny5BBcgiEASsuLhb+MbXokaoEwRAUNOLkjY6Olr6Mjo7KwHIfNCsrK2lQmZ5pcEMVQ0NDMlwIhgbyCSn87+/vC1o+niUB0KMyHOdQHCpsaWkRWYN/UAZbWlpCopdov79qMBq3vLwsRzaQAiGCHx8fk81mo+3tbdrY2PA8R/MBhqdYmg9AaiGh1WoN13pPnTqKUS4o8j7gsPBMTbNSFY4N0Ksm3jsWH4AB8Gxns3ofxepieewu8F1IiiFVJ6kyiII/DbhIN9kq2N6w/QLKEOj/7gSVEH2BMFgspO5l+EIEfPgtqJt6j+0Ptj/ZXrNjHTesGs1/6heDulZNJtNYe3v7e/6ubhwNP3fIjwEfHxtvesdI3qWkpLzlXxlm/q57KgFr3c4CaQXNj4H5V4ABAFqBpDvjWvRIAAAAAElFTkSuQmCC\" />
            ";
            // line 21
            if (((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) == 0) && (twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())) > 0))) {
                // line 22
                echo "                <span class=\"sf-toolbar-value\">";
                echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())), "html", null, true);
                echo "</span>
                <span class=\"sf-toolbar-label\">errors</span>
            ";
            } else {
                // line 25
                echo "                <span class=\"sf-toolbar-value\">";
                echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())), "html", null, true);
                echo "</span>
                <span class=\"sf-toolbar-info-piece-additional-detail\">
                    <span class=\"sf-toolbar-label\">in</span>
                    <span class=\"sf-toolbar-value\">";
                // line 28
                echo twig_escape_filter($this->env, twig_round($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "time", array())), "html", null, true);
                echo "</span>
                    <span class=\"sf-toolbar-label\">ms</span>
                </span>
            ";
            }
            // line 32
            echo "        ";
        }
        // line 33
        echo "    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 34
        echo "
    ";
        // line 35
        ob_start();
        // line 36
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>LDAP Operations</b>
            <span class=\"sf-toolbar-status\">";
        // line 38
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())), "html", null, true);
        echo "</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Elapsed Time</b>
            <span>";
        // line 42
        echo twig_escape_filter($this->env, twig_round($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "time", array())), "html", null, true);
        echo " ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Errors</b>
            <span class=\"sf-toolbar-status ";
        // line 46
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())) > 0)) ? ("sf-toolbar-status-red") : (""));
        echo "\">";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 49
        echo "
    ";
        // line 50
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => (isset($context["profiler_url"]) ? $context["profiler_url"] : $this->getContext($context, "profiler_url")), "status" => ((array_key_exists("status", $context)) ? (_twig_default_filter((isset($context["status"]) ? $context["status"] : $this->getContext($context, "status")), "")) : (""))));
        echo "
";
        
        $__internal_cbc7ca012d61d9c816d873439d05e92b028761495e41c474bd9d3ede1b01c3ed->leave($__internal_cbc7ca012d61d9c816d873439d05e92b028761495e41c474bd9d3ede1b01c3ed_prof);

        
        $__internal_d360f5a6028b0e2db703465202ae5b2a1cee00e4c5412857492f24207d121e39->leave($__internal_d360f5a6028b0e2db703465202ae5b2a1cee00e4c5412857492f24207d121e39_prof);

    }

    // line 53
    public function block_menu($context, array $blocks = array())
    {
        $__internal_34cbc03e8e99b3fbbbc0ae5385bdd5f96b192c8253660e9893f52a4fa8b46009 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34cbc03e8e99b3fbbbc0ae5385bdd5f96b192c8253660e9893f52a4fa8b46009->enter($__internal_34cbc03e8e99b3fbbbc0ae5385bdd5f96b192c8253660e9893f52a4fa8b46009_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_f12aba17c6d47187adae2466b940eacc59869d3c963a2eb99d8088b1494c104b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f12aba17c6d47187adae2466b940eacc59869d3c963a2eb99d8088b1494c104b->enter($__internal_f12aba17c6d47187adae2466b940eacc59869d3c963a2eb99d8088b1494c104b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 54
        echo "    ";
        $context["profiler_markup_version"] = ((array_key_exists("profiler_markup_version", $context)) ? (_twig_default_filter((isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")), 1)) : (1));
        // line 55
        echo "    ";
        // line 56
        echo "    ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())) > 0)) {
            // line 57
            echo "        ";
            $context["label_status"] = "error";
            // line 58
            echo "    ";
        } elseif ((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) > 10)) {
            // line 59
            echo "        ";
            $context["label_status"] = "warning";
            // line 60
            echo "    ";
        } else {
            // line 61
            echo "        ";
            $context["label_status"] = "normal";
            // line 62
            echo "    ";
        }
        // line 63
        echo "    <span class=\"label label-status-";
        echo twig_escape_filter($this->env, (isset($context["label_status"]) ? $context["label_status"] : $this->getContext($context, "label_status")), "html", null, true);
        echo " ";
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())) == 0)) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTFCMEY0N0Q5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCMEY0N0U5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MUIwRjQ3Qjk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5MUIwRjQ3Qzk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PizcUhYAAASiSURBVHjahFbJT9tXEB4v2OxrKfsuKAhMALOIAwGL5AAhRWovFKqq6oV/gRtcqhyiSrlFQiiISw+gSgEBt0gQQIgDixCrjFkEZhP7ZhZjd75pnmUcQp80GP9+b2a++eab96xva2sjtVwuF93d3Rl1Op0pMDCwOCgoKMNoNH6n1+vVO8fFxYX16upq6fb2dkar1e7gnUajoW8tPf643W5ih+/DwsJ+T09P/zk2NvYZBzY6nU6C4T0MwQwGgyQ7Ozuzr6+vD29ubn7gEJ/w7tEEX5wbLRbLXzExMbGLi4s0PDxMu7u7dH19Tff39xIcixGTv78/hYaGUnJyckJOTk5jeXl548jIyN87Ozu/cZJ73wS6ioqKF3V1dR+5/ODOzk5aWFig8/NzCYqATJcgh+F/oL+8vCRGTtPT03RyckI1NTUmrsbJLHz2pUvLaF4lJCRQT0+PoGXu6Sle8RyJmEHZOzMzQ7Ozs5SUlPQj/H2Xlpt2dnh4SFVVVegD3dzcCMqnFqpDb1BJYmIiZWVlgdJzVPxVDzirq6+vj6qrq6m5uZmmpqZobW2NTk9PJRHMuxrVh7i4OMrNzSXug1RxcHDg+qK0hwkYgXFra4v6+/spPj6eMjMzqaioSDayHKUqNBu0gRaWrnyCDm4s9fb2SiURERF+ZrNZxwAe8KThps5yc01whBNKR7CQkBDhOCAgQChBFUjKlEpiJMUzRVdlZeVlbW3tDxzT/qACPz+/z6WlpSaUiQWdwxGoEExJVNGlVMV+8j0/P5/Cw8OlF7x+YnvP5vTIlF+aWQEWbIQzkCn9q2bjE9oHhQAAGeMZ+ra3t0fd3d00ODhomJubqykoKCjkyv9hN6FKz5zpJiYmwCHxFFNZWZk4Az0SIRgC5+XleRrOUyxg5ufnqauri4KDg+X52NgYALxubW39lb9/UJN8p1BNTk6KSuAAxKCBEUni1dVVT0WRkZGE2eno6JCKlDzhx1XQ0dHR86ioKEmgLSkpeQak2ITN4NfhcIhCEBRJ7Pb/+gbUMMwNfADCe2bwPxTGtu+RNVMSD6V46xflKr3jHVTiO9nY39TUJO8hCIDCkDY0NLi5EhvCCChWUGJhYaEFG3CuIJhKAsfU1FQpHc4qCZID7fj4ONXX18tsoE8cHIk1AwMDr9LS0irZ76MuIyPDwiVXZWdny5BBcgiEASsuLhb+MbXokaoEwRAUNOLkjY6Olr6Mjo7KwHIfNCsrK2lQmZ5pcEMVQ0NDMlwIhgbyCSn87+/vC1o+niUB0KMyHOdQHCpsaWkRWYN/UAZbWlpCopdov79qMBq3vLwsRzaQAiGCHx8fk81mo+3tbdrY2PA8R/MBhqdYmg9AaiGh1WoN13pPnTqKUS4o8j7gsPBMTbNSFY4N0Ksm3jsWH4AB8Gxns3ofxepieewu8F1IiiFVJ6kyiII/DbhIN9kq2N6w/QLKEOj/7gSVEH2BMFgspO5l+EIEfPgtqJt6j+0Ptj/ZXrNjHTesGs1/6heDulZNJtNYe3v7e/6ubhwNP3fIjwEfHxtvesdI3qWkpLzlXxlm/q57KgFr3c4CaQXNj4H5V4ABAFqBpDvjWvRIAAAAAElFTkSuQmCC\" /></span>
        <strong>LdapTools</strong>
        ";
        // line 66
        if ((((isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")) != 1) && twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())))) {
            // line 67
            echo "            ";
            $context["panel_count"] = twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array()));
            // line 68
            echo "        ";
        } else {
            // line 69
            echo "            ";
            $context["panel_count"] = twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array()));
            // line 70
            echo "        ";
        }
        // line 71
        echo "        ";
        if (((isset($context["panel_count"]) ? $context["panel_count"] : $this->getContext($context, "panel_count")) > 0)) {
            // line 72
            echo "            <span class=\"count\">
                <span>";
            // line 73
            echo twig_escape_filter($this->env, (isset($context["panel_count"]) ? $context["panel_count"] : $this->getContext($context, "panel_count")), "html", null, true);
            echo "</span>
            </span>
        ";
        }
        // line 76
        echo "    </span>
";
        
        $__internal_f12aba17c6d47187adae2466b940eacc59869d3c963a2eb99d8088b1494c104b->leave($__internal_f12aba17c6d47187adae2466b940eacc59869d3c963a2eb99d8088b1494c104b_prof);

        
        $__internal_34cbc03e8e99b3fbbbc0ae5385bdd5f96b192c8253660e9893f52a4fa8b46009->leave($__internal_34cbc03e8e99b3fbbbc0ae5385bdd5f96b192c8253660e9893f52a4fa8b46009_prof);

    }

    // line 79
    public function block_panel($context, array $blocks = array())
    {
        $__internal_49f07b10c65a218d87638ef43eff826580c14d90548ce954977619f1938b3266 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49f07b10c65a218d87638ef43eff826580c14d90548ce954977619f1938b3266->enter($__internal_49f07b10c65a218d87638ef43eff826580c14d90548ce954977619f1938b3266_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_25745d48d6a544b3ed1f66bb1949af8b632e068070918a465427dbafb17a7b57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25745d48d6a544b3ed1f66bb1949af8b632e068070918a465427dbafb17a7b57->enter($__internal_25745d48d6a544b3ed1f66bb1949af8b632e068070918a465427dbafb17a7b57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 80
        echo "    ";
        $context["profiler_markup_version"] = ((array_key_exists("profiler_markup_version", $context)) ? (_twig_default_filter((isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")), 1)) : (1));
        // line 81
        echo "    <h2>LDAP Operations</h2>
    ";
        // line 82
        if (((isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")) == 1)) {
            // line 83
            echo "        ";
            if (twig_test_empty($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array()))) {
                // line 84
                echo "            <div class=\"empty\">
                <p>No LDAP operations were performed.</p>
            </div>
        ";
            } else {
                // line 88
                echo "            ";
                echo $context["helper"]->getrender_table($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array()), (isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")));
                echo "
        ";
            }
            // line 90
            echo "    ";
        } else {
            // line 91
            echo "        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">All Operations<span class=\"badge\">";
            // line 93
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array())), "html", null, true);
            echo "</span></h3>
                <div class=\"tab-content\">
                ";
            // line 95
            if (twig_test_empty($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array()))) {
                // line 96
                echo "                    <div class=\"empty\">
                        <p>No LDAP operations were performed.</p>
                    </div>
                ";
            } else {
                // line 100
                echo "                    ";
                echo $context["helper"]->getrender_table($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operations", array()), (isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")));
                echo "
                ";
            }
            // line 102
            echo "                </div>
            </div>
            <div class=\"tab\">
                <h3 class=\"tab-title\">Errors<span class=\"badge\">";
            // line 105
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array())), "html", null, true);
            echo "</span></h3>
                <div class=\"tab-content\">
                ";
            // line 107
            if (twig_test_empty($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array()))) {
                // line 108
                echo "                    <div class=\"empty\">
                        <p>No operations encountered any errors.</p>
                    </div>
                ";
            } else {
                // line 112
                echo "                    ";
                echo $context["helper"]->getrender_table($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "errors", array()), (isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")));
                echo "
                ";
            }
            // line 114
            echo "                </div>
            </div>
            ";
            // line 116
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["collector"]) ? $context["collector"] : $this->getContext($context, "collector")), "operationsByDomain", array()));
            foreach ($context['_seq'] as $context["key"] => $context["values"]) {
                // line 117
                echo "                <div class=\"tab\">
                    <h3 class=\"tab-title\">";
                // line 118
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "<span class=\"badge\">";
                echo twig_escape_filter($this->env, twig_length_filter($this->env, $context["values"]), "html", null, true);
                echo "</span></h3>
                    <div class=\"tab-content\">
                    ";
                // line 120
                if (twig_test_empty($context["values"])) {
                    // line 121
                    echo "                        <div class=\"empty\">
                            <p>No operations performed for ";
                    // line 122
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo ".</p>
                        </div>
                    ";
                } else {
                    // line 125
                    echo "                        ";
                    echo $context["helper"]->getrender_table($context["values"], (isset($context["profiler_markup_version"]) ? $context["profiler_markup_version"] : $this->getContext($context, "profiler_markup_version")));
                    echo "
                    ";
                }
                // line 127
                echo "                    </div>
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['values'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "        </div>
    ";
        }
        
        $__internal_25745d48d6a544b3ed1f66bb1949af8b632e068070918a465427dbafb17a7b57->leave($__internal_25745d48d6a544b3ed1f66bb1949af8b632e068070918a465427dbafb17a7b57_prof);

        
        $__internal_49f07b10c65a218d87638ef43eff826580c14d90548ce954977619f1938b3266->leave($__internal_49f07b10c65a218d87638ef43eff826580c14d90548ce954977619f1938b3266_prof);

    }

    // line 134
    public function getrender_table($__logs__ = null, $__version__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "logs" => $__logs__,
            "version" => $__version__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_43c58b10f86d64104d8c62d3beabf08dc85b50399da2dee5711fe493ee96e36a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $__internal_43c58b10f86d64104d8c62d3beabf08dc85b50399da2dee5711fe493ee96e36a->enter($__internal_43c58b10f86d64104d8c62d3beabf08dc85b50399da2dee5711fe493ee96e36a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "render_table"));

            $__internal_588fe7808f95a871493736805f78bdeaeac7cce62a6fbb6e188ceab37c01d913 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $__internal_588fe7808f95a871493736805f78bdeaeac7cce62a6fbb6e188ceab37c01d913->enter($__internal_588fe7808f95a871493736805f78bdeaeac7cce62a6fbb6e188ceab37c01d913_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "render_table"));

            // line 135
            echo "<table>
    <tr>
        <th class=\"nowrap\">#</th>
        <th class=\"nowrap\">Time</th>
        <th class=\"nowrap\">Type</th>
        <th class=\"nowrap\">Domain</th>
        <th style=\"width: 100%;\">Details</th>
    </tr>

    ";
            // line 144
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["logs"]) ? $context["logs"] : $this->getContext($context, "logs")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
                // line 145
                echo "    <tr class=\"";
                echo ((((isset($context["version"]) ? $context["version"] : $this->getContext($context, "version")) != 1)) ? ("status-") : (""));
                echo (((null === $this->getAttribute($context["log"], "error", array()))) ? ("normal") : ("error"));
                echo "\">
        <td class=\"nowrap\">";
                // line 146
                echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                echo "</td>
        <td class=\"nowrap\">";
                // line 147
                echo twig_escape_filter($this->env, twig_round($this->getAttribute($context["log"], "duration", array())), "html", null, true);
                echo " ms</td>
        <td class=\"nowrap\">";
                // line 148
                echo twig_escape_filter($this->env, $this->getAttribute($context["log"], "name", array()), "html", null, true);
                echo "</td>
        <td class=\"nowrap\">";
                // line 149
                echo twig_escape_filter($this->env, $this->getAttribute($context["log"], "domain", array()), "html", null, true);
                echo "</td>
        <td>
            ";
                // line 151
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["log"], "data", array()));
                foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                    // line 152
                    echo "            <div>
                <strong class=\"font-normal text-small\">";
                    // line 153
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "</strong>: ";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "
            </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 156
                echo "            ";
                if ( !(null === $this->getAttribute($context["log"], "error", array()))) {
                    // line 157
                    echo "                <div>
                    <strong class=\"font-normal text-small\">Error</strong>: ";
                    // line 158
                    echo twig_escape_filter($this->env, $this->getAttribute($context["log"], "error", array()), "html", null, true);
                    echo "
                </div>
            ";
                }
                // line 161
                echo "        </td>
    </tr>
    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 164
            echo "</table>
";
            
            $__internal_588fe7808f95a871493736805f78bdeaeac7cce62a6fbb6e188ceab37c01d913->leave($__internal_588fe7808f95a871493736805f78bdeaeac7cce62a6fbb6e188ceab37c01d913_prof);

            
            $__internal_43c58b10f86d64104d8c62d3beabf08dc85b50399da2dee5711fe493ee96e36a->leave($__internal_43c58b10f86d64104d8c62d3beabf08dc85b50399da2dee5711fe493ee96e36a_prof);

        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "@LdapTools/Collector/ldaptools.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  510 => 164,  494 => 161,  488 => 158,  485 => 157,  482 => 156,  471 => 153,  468 => 152,  464 => 151,  459 => 149,  455 => 148,  451 => 147,  447 => 146,  441 => 145,  424 => 144,  413 => 135,  394 => 134,  382 => 130,  374 => 127,  368 => 125,  362 => 122,  359 => 121,  357 => 120,  350 => 118,  347 => 117,  343 => 116,  339 => 114,  333 => 112,  327 => 108,  325 => 107,  320 => 105,  315 => 102,  309 => 100,  303 => 96,  301 => 95,  296 => 93,  292 => 91,  289 => 90,  283 => 88,  277 => 84,  274 => 83,  272 => 82,  269 => 81,  266 => 80,  257 => 79,  246 => 76,  240 => 73,  237 => 72,  234 => 71,  231 => 70,  228 => 69,  225 => 68,  222 => 67,  220 => 66,  211 => 63,  208 => 62,  205 => 61,  202 => 60,  199 => 59,  196 => 58,  193 => 57,  190 => 56,  188 => 55,  185 => 54,  176 => 53,  164 => 50,  161 => 49,  153 => 46,  146 => 42,  139 => 38,  135 => 36,  133 => 35,  130 => 34,  127 => 33,  124 => 32,  117 => 28,  110 => 25,  103 => 22,  101 => 21,  97 => 19,  94 => 18,  91 => 17,  85 => 15,  82 => 14,  76 => 12,  74 => 11,  66 => 10,  63 => 9,  60 => 8,  57 => 7,  54 => 6,  45 => 5,  35 => 1,  33 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% import _self as helper %}

{% block toolbar %}
    {% set profiler_markup_version = profiler_markup_version|default(1) %}
    {% set icon %}
        {% if profiler_markup_version == 1 %}
            <img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTFCMEY0N0Q5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCMEY0N0U5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MUIwRjQ3Qjk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5MUIwRjQ3Qzk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PizcUhYAAASiSURBVHjahFbJT9tXEB4v2OxrKfsuKAhMALOIAwGL5AAhRWovFKqq6oV/gRtcqhyiSrlFQiiISw+gSgEBt0gQQIgDixCrjFkEZhP7ZhZjd75pnmUcQp80GP9+b2a++eab96xva2sjtVwuF93d3Rl1Op0pMDCwOCgoKMNoNH6n1+vVO8fFxYX16upq6fb2dkar1e7gnUajoW8tPf643W5ih+/DwsJ+T09P/zk2NvYZBzY6nU6C4T0MwQwGgyQ7Ozuzr6+vD29ubn7gEJ/w7tEEX5wbLRbLXzExMbGLi4s0PDxMu7u7dH19Tff39xIcixGTv78/hYaGUnJyckJOTk5jeXl548jIyN87Ozu/cZJ73wS6ioqKF3V1dR+5/ODOzk5aWFig8/NzCYqATJcgh+F/oL+8vCRGTtPT03RyckI1NTUmrsbJLHz2pUvLaF4lJCRQT0+PoGXu6Sle8RyJmEHZOzMzQ7Ozs5SUlPQj/H2Xlpt2dnh4SFVVVegD3dzcCMqnFqpDb1BJYmIiZWVlgdJzVPxVDzirq6+vj6qrq6m5uZmmpqZobW2NTk9PJRHMuxrVh7i4OMrNzSXug1RxcHDg+qK0hwkYgXFra4v6+/spPj6eMjMzqaioSDayHKUqNBu0gRaWrnyCDm4s9fb2SiURERF+ZrNZxwAe8KThps5yc01whBNKR7CQkBDhOCAgQChBFUjKlEpiJMUzRVdlZeVlbW3tDxzT/qACPz+/z6WlpSaUiQWdwxGoEExJVNGlVMV+8j0/P5/Cw8OlF7x+YnvP5vTIlF+aWQEWbIQzkCn9q2bjE9oHhQAAGeMZ+ra3t0fd3d00ODhomJubqykoKCjkyv9hN6FKz5zpJiYmwCHxFFNZWZk4Az0SIRgC5+XleRrOUyxg5ufnqauri4KDg+X52NgYALxubW39lb9/UJN8p1BNTk6KSuAAxKCBEUni1dVVT0WRkZGE2eno6JCKlDzhx1XQ0dHR86ioKEmgLSkpeQak2ITN4NfhcIhCEBRJ7Pb/+gbUMMwNfADCe2bwPxTGtu+RNVMSD6V46xflKr3jHVTiO9nY39TUJO8hCIDCkDY0NLi5EhvCCChWUGJhYaEFG3CuIJhKAsfU1FQpHc4qCZID7fj4ONXX18tsoE8cHIk1AwMDr9LS0irZ76MuIyPDwiVXZWdny5BBcgiEASsuLhb+MbXokaoEwRAUNOLkjY6Olr6Mjo7KwHIfNCsrK2lQmZ5pcEMVQ0NDMlwIhgbyCSn87+/vC1o+niUB0KMyHOdQHCpsaWkRWYN/UAZbWlpCopdov79qMBq3vLwsRzaQAiGCHx8fk81mo+3tbdrY2PA8R/MBhqdYmg9AaiGh1WoN13pPnTqKUS4o8j7gsPBMTbNSFY4N0Ksm3jsWH4AB8Gxns3ofxepieewu8F1IiiFVJ6kyiII/DbhIN9kq2N6w/QLKEOj/7gSVEH2BMFgspO5l+EIEfPgtqJt6j+0Ptj/ZXrNjHTesGs1/6heDulZNJtNYe3v7e/6ubhwNP3fIjwEfHxtvesdI3qWkpLzlXxlm/q57KgFr3c4CaQXNj4H5V4ABAFqBpDvjWvRIAAAAAElFTkSuQmCC\" />
            <span class=\"sf-toolbar-value sf-toolbar-status {% if collector.operations|length > 10 %}sf-toolbar-status-yellow{% endif %}\">{{ collector.operations|length }}</span>
            {% if collector.operations|length > 0 %}
                <span class=\"sf-toolbar-info-piece-additional-detail\">in {{ collector.time|round }} ms</span>
            {% endif %}
            {% if collector.errors|length > 0 %}
                <span class=\"sf-toolbar-info-piece-additional sf-toolbar-status sf-toolbar-status-red\">{{ collector.errors|length }}</span>
            {% endif %}
        {% elseif collector.operations|length > 0 or collector.errors|length > 0 %}
            {% set status = collector.errors|length > 0 ? 'red' : collector.operations|length > 10 ? 'yellow' %}

            <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTFCMEY0N0Q5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCMEY0N0U5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MUIwRjQ3Qjk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5MUIwRjQ3Qzk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PizcUhYAAASiSURBVHjahFbJT9tXEB4v2OxrKfsuKAhMALOIAwGL5AAhRWovFKqq6oV/gRtcqhyiSrlFQiiISw+gSgEBt0gQQIgDixCrjFkEZhP7ZhZjd75pnmUcQp80GP9+b2a++eab96xva2sjtVwuF93d3Rl1Op0pMDCwOCgoKMNoNH6n1+vVO8fFxYX16upq6fb2dkar1e7gnUajoW8tPf643W5ih+/DwsJ+T09P/zk2NvYZBzY6nU6C4T0MwQwGgyQ7Ozuzr6+vD29ubn7gEJ/w7tEEX5wbLRbLXzExMbGLi4s0PDxMu7u7dH19Tff39xIcixGTv78/hYaGUnJyckJOTk5jeXl548jIyN87Ozu/cZJ73wS6ioqKF3V1dR+5/ODOzk5aWFig8/NzCYqATJcgh+F/oL+8vCRGTtPT03RyckI1NTUmrsbJLHz2pUvLaF4lJCRQT0+PoGXu6Sle8RyJmEHZOzMzQ7Ozs5SUlPQj/H2Xlpt2dnh4SFVVVegD3dzcCMqnFqpDb1BJYmIiZWVlgdJzVPxVDzirq6+vj6qrq6m5uZmmpqZobW2NTk9PJRHMuxrVh7i4OMrNzSXug1RxcHDg+qK0hwkYgXFra4v6+/spPj6eMjMzqaioSDayHKUqNBu0gRaWrnyCDm4s9fb2SiURERF+ZrNZxwAe8KThps5yc01whBNKR7CQkBDhOCAgQChBFUjKlEpiJMUzRVdlZeVlbW3tDxzT/qACPz+/z6WlpSaUiQWdwxGoEExJVNGlVMV+8j0/P5/Cw8OlF7x+YnvP5vTIlF+aWQEWbIQzkCn9q2bjE9oHhQAAGeMZ+ra3t0fd3d00ODhomJubqykoKCjkyv9hN6FKz5zpJiYmwCHxFFNZWZk4Az0SIRgC5+XleRrOUyxg5ufnqauri4KDg+X52NgYALxubW39lb9/UJN8p1BNTk6KSuAAxKCBEUni1dVVT0WRkZGE2eno6JCKlDzhx1XQ0dHR86ioKEmgLSkpeQak2ITN4NfhcIhCEBRJ7Pb/+gbUMMwNfADCe2bwPxTGtu+RNVMSD6V46xflKr3jHVTiO9nY39TUJO8hCIDCkDY0NLi5EhvCCChWUGJhYaEFG3CuIJhKAsfU1FQpHc4qCZID7fj4ONXX18tsoE8cHIk1AwMDr9LS0irZ76MuIyPDwiVXZWdny5BBcgiEASsuLhb+MbXokaoEwRAUNOLkjY6Olr6Mjo7KwHIfNCsrK2lQmZ5pcEMVQ0NDMlwIhgbyCSn87+/vC1o+niUB0KMyHOdQHCpsaWkRWYN/UAZbWlpCopdov79qMBq3vLwsRzaQAiGCHx8fk81mo+3tbdrY2PA8R/MBhqdYmg9AaiGh1WoN13pPnTqKUS4o8j7gsPBMTbNSFY4N0Ksm3jsWH4AB8Gxns3ofxepieewu8F1IiiFVJ6kyiII/DbhIN9kq2N6w/QLKEOj/7gSVEH2BMFgspO5l+EIEfPgtqJt6j+0Ptj/ZXrNjHTesGs1/6heDulZNJtNYe3v7e/6ubhwNP3fIjwEfHxtvesdI3qWkpLzlXxlm/q57KgFr3c4CaQXNj4H5V4ABAFqBpDvjWvRIAAAAAElFTkSuQmCC\" />
            {% if collector.operations|length == 0 and collector.errors|length > 0 %}
                <span class=\"sf-toolbar-value\">{{ collector.errors|length }}</span>
                <span class=\"sf-toolbar-label\">errors</span>
            {% else %}
                <span class=\"sf-toolbar-value\">{{ collector.operations|length }}</span>
                <span class=\"sf-toolbar-info-piece-additional-detail\">
                    <span class=\"sf-toolbar-label\">in</span>
                    <span class=\"sf-toolbar-value\">{{ collector.time|round }}</span>
                    <span class=\"sf-toolbar-label\">ms</span>
                </span>
            {% endif %}
        {% endif %}
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b>LDAP Operations</b>
            <span class=\"sf-toolbar-status\">{{ collector.operations|length }}</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Elapsed Time</b>
            <span>{{ collector.time|round }} ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Errors</b>
            <span class=\"sf-toolbar-status {{ collector.errors|length > 0 ? 'sf-toolbar-status-red' : '' }}\">{{ collector.errors|length }}</span>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: profiler_url, status: status|default('') }) }}
{% endblock %}

{% block menu %}
    {% set profiler_markup_version = profiler_markup_version|default(1) %}
    {# This left-hand menu appears when using the full-screen profiler. #}
    {% if collector.errors|length > 0 %}
        {% set label_status = \"error\" %}
    {% elseif collector.operations|length > 10 %}
        {% set label_status = \"warning\" %}
    {% else %}
        {% set label_status = \"normal\" %}
    {% endif %}
    <span class=\"label label-status-{{ label_status }} {{ collector.operations|length == 0 ? 'disabled' }}\">
        <span class=\"icon\"><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OTFCMEY0N0Q5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTFCMEY0N0U5NDk1MTFFNTk3MDFGQjFGNDc5ODY0MTQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MUIwRjQ3Qjk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5MUIwRjQ3Qzk0OTUxMUU1OTcwMUZCMUY0Nzk4NjQxNCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PizcUhYAAASiSURBVHjahFbJT9tXEB4v2OxrKfsuKAhMALOIAwGL5AAhRWovFKqq6oV/gRtcqhyiSrlFQiiISw+gSgEBt0gQQIgDixCrjFkEZhP7ZhZjd75pnmUcQp80GP9+b2a++eab96xva2sjtVwuF93d3Rl1Op0pMDCwOCgoKMNoNH6n1+vVO8fFxYX16upq6fb2dkar1e7gnUajoW8tPf643W5ih+/DwsJ+T09P/zk2NvYZBzY6nU6C4T0MwQwGgyQ7Ozuzr6+vD29ubn7gEJ/w7tEEX5wbLRbLXzExMbGLi4s0PDxMu7u7dH19Tff39xIcixGTv78/hYaGUnJyckJOTk5jeXl548jIyN87Ozu/cZJ73wS6ioqKF3V1dR+5/ODOzk5aWFig8/NzCYqATJcgh+F/oL+8vCRGTtPT03RyckI1NTUmrsbJLHz2pUvLaF4lJCRQT0+PoGXu6Sle8RyJmEHZOzMzQ7Ozs5SUlPQj/H2Xlpt2dnh4SFVVVegD3dzcCMqnFqpDb1BJYmIiZWVlgdJzVPxVDzirq6+vj6qrq6m5uZmmpqZobW2NTk9PJRHMuxrVh7i4OMrNzSXug1RxcHDg+qK0hwkYgXFra4v6+/spPj6eMjMzqaioSDayHKUqNBu0gRaWrnyCDm4s9fb2SiURERF+ZrNZxwAe8KThps5yc01whBNKR7CQkBDhOCAgQChBFUjKlEpiJMUzRVdlZeVlbW3tDxzT/qACPz+/z6WlpSaUiQWdwxGoEExJVNGlVMV+8j0/P5/Cw8OlF7x+YnvP5vTIlF+aWQEWbIQzkCn9q2bjE9oHhQAAGeMZ+ra3t0fd3d00ODhomJubqykoKCjkyv9hN6FKz5zpJiYmwCHxFFNZWZk4Az0SIRgC5+XleRrOUyxg5ufnqauri4KDg+X52NgYALxubW39lb9/UJN8p1BNTk6KSuAAxKCBEUni1dVVT0WRkZGE2eno6JCKlDzhx1XQ0dHR86ioKEmgLSkpeQak2ITN4NfhcIhCEBRJ7Pb/+gbUMMwNfADCe2bwPxTGtu+RNVMSD6V46xflKr3jHVTiO9nY39TUJO8hCIDCkDY0NLi5EhvCCChWUGJhYaEFG3CuIJhKAsfU1FQpHc4qCZID7fj4ONXX18tsoE8cHIk1AwMDr9LS0irZ76MuIyPDwiVXZWdny5BBcgiEASsuLhb+MbXokaoEwRAUNOLkjY6Olr6Mjo7KwHIfNCsrK2lQmZ5pcEMVQ0NDMlwIhgbyCSn87+/vC1o+niUB0KMyHOdQHCpsaWkRWYN/UAZbWlpCopdov79qMBq3vLwsRzaQAiGCHx8fk81mo+3tbdrY2PA8R/MBhqdYmg9AaiGh1WoN13pPnTqKUS4o8j7gsPBMTbNSFY4N0Ksm3jsWH4AB8Gxns3ofxepieewu8F1IiiFVJ6kyiII/DbhIN9kq2N6w/QLKEOj/7gSVEH2BMFgspO5l+EIEfPgtqJt6j+0Ptj/ZXrNjHTesGs1/6heDulZNJtNYe3v7e/6ubhwNP3fIjwEfHxtvesdI3qWkpLzlXxlm/q57KgFr3c4CaQXNj4H5V4ABAFqBpDvjWvRIAAAAAElFTkSuQmCC\" /></span>
        <strong>LdapTools</strong>
        {% if profiler_markup_version != 1 and collector.errors|length %}
            {% set panel_count = collector.errors|length %}
        {% else %}
            {% set panel_count = collector.operations|length %}
        {% endif %}
        {% if panel_count > 0 %}
            <span class=\"count\">
                <span>{{ panel_count }}</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    {% set profiler_markup_version = profiler_markup_version|default(1) %}
    <h2>LDAP Operations</h2>
    {% if profiler_markup_version == 1 %}
        {% if collector.operations is empty %}
            <div class=\"empty\">
                <p>No LDAP operations were performed.</p>
            </div>
        {% else %}
            {{ helper.render_table(collector.operations, profiler_markup_version) }}
        {% endif %}
    {% else %}
        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">All Operations<span class=\"badge\">{{ collector.operations|length }}</span></h3>
                <div class=\"tab-content\">
                {% if collector.operations is empty %}
                    <div class=\"empty\">
                        <p>No LDAP operations were performed.</p>
                    </div>
                {% else %}
                    {{ helper.render_table(collector.operations, profiler_markup_version) }}
                {% endif %}
                </div>
            </div>
            <div class=\"tab\">
                <h3 class=\"tab-title\">Errors<span class=\"badge\">{{ collector.errors|length }}</span></h3>
                <div class=\"tab-content\">
                {% if collector.errors is empty %}
                    <div class=\"empty\">
                        <p>No operations encountered any errors.</p>
                    </div>
                {% else %}
                    {{ helper.render_table(collector.errors, profiler_markup_version) }}
                {% endif %}
                </div>
            </div>
            {% for key, values in collector.operationsByDomain %}
                <div class=\"tab\">
                    <h3 class=\"tab-title\">{{ key }}<span class=\"badge\">{{ values|length }}</span></h3>
                    <div class=\"tab-content\">
                    {% if values is empty %}
                        <div class=\"empty\">
                            <p>No operations performed for {{ key }}.</p>
                        </div>
                    {% else %}
                        {{ helper.render_table(values, profiler_markup_version) }}
                    {% endif %}
                    </div>
                </div>
            {% endfor %}
        </div>
    {% endif %}
{% endblock %}

{% macro render_table(logs, version) %}
<table>
    <tr>
        <th class=\"nowrap\">#</th>
        <th class=\"nowrap\">Time</th>
        <th class=\"nowrap\">Type</th>
        <th class=\"nowrap\">Domain</th>
        <th style=\"width: 100%;\">Details</th>
    </tr>

    {%  for log in logs %}
    <tr class=\"{{ version != 1 ? 'status-' }}{{ log.error is null ? 'normal' : 'error' }}\">
        <td class=\"nowrap\">{{ loop.index }}</td>
        <td class=\"nowrap\">{{ log.duration|round }} ms</td>
        <td class=\"nowrap\">{{ log.name }}</td>
        <td class=\"nowrap\">{{ log.domain }}</td>
        <td>
            {% for key, value in log.data %}
            <div>
                <strong class=\"font-normal text-small\">{{ key }}</strong>: {{ value }}
            </div>
            {% endfor %}
            {% if log.error is not null %}
                <div>
                    <strong class=\"font-normal text-small\">Error</strong>: {{ log.error }}
                </div>
            {% endif %}
        </td>
    </tr>
    {%  endfor %}
</table>
{% endmacro %}
", "@LdapTools/Collector/ldaptools.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\ldaptools\\ldaptools-bundle\\Resources\\views\\Collector\\ldaptools.html.twig");
    }
}
