<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_b7cfb616b6e0c14eab165183dedef3b1da15f474c4761559d8193818f00f527b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b011bcb4683f40116a961c0cf84862cb072d3fadd982e4845d1edfaa779e0b76 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b011bcb4683f40116a961c0cf84862cb072d3fadd982e4845d1edfaa779e0b76->enter($__internal_b011bcb4683f40116a961c0cf84862cb072d3fadd982e4845d1edfaa779e0b76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_b011bcb4683f40116a961c0cf84862cb072d3fadd982e4845d1edfaa779e0b76->leave($__internal_b011bcb4683f40116a961c0cf84862cb072d3fadd982e4845d1edfaa779e0b76_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/hidden_row.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_row.html.php");
    }
}
