<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_75bf7fff7150ee4eae824dc79853aa625f3902e63e69fec0e92dd1a7aae02305 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbf5f5644696df03ff8d583f8fb5890ce15594cb9d73e750bfc7f68b17010496 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fbf5f5644696df03ff8d583f8fb5890ce15594cb9d73e750bfc7f68b17010496->enter($__internal_fbf5f5644696df03ff8d583f8fb5890ce15594cb9d73e750bfc7f68b17010496_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_fbf5f5644696df03ff8d583f8fb5890ce15594cb9d73e750bfc7f68b17010496->leave($__internal_fbf5f5644696df03ff8d583f8fb5890ce15594cb9d73e750bfc7f68b17010496_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_b34d3567c049f08efd6bb96cd983ca0cb30ac8acfb7566f6fdf8a190e159db02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b34d3567c049f08efd6bb96cd983ca0cb30ac8acfb7566f6fdf8a190e159db02->enter($__internal_b34d3567c049f08efd6bb96cd983ca0cb30ac8acfb7566f6fdf8a190e159db02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        
        $__internal_b34d3567c049f08efd6bb96cd983ca0cb30ac8acfb7566f6fdf8a190e159db02->leave($__internal_b34d3567c049f08efd6bb96cd983ca0cb30ac8acfb7566f6fdf8a190e159db02_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_4d79c5e76f741d23719e75a129dd4ec41b1a8cda50cfd5466d79dbc5d2d3b21e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4d79c5e76f741d23719e75a129dd4ec41b1a8cda50cfd5466d79dbc5d2d3b21e->enter($__internal_4d79c5e76f741d23719e75a129dd4ec41b1a8cda50cfd5466d79dbc5d2d3b21e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_4d79c5e76f741d23719e75a129dd4ec41b1a8cda50cfd5466d79dbc5d2d3b21e->leave($__internal_4d79c5e76f741d23719e75a129dd4ec41b1a8cda50cfd5466d79dbc5d2d3b21e_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_f33a9e3ff19314b8e5b6206df32e84d67e2cc28032694d2d895ca374feb5811b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f33a9e3ff19314b8e5b6206df32e84d67e2cc28032694d2d895ca374feb5811b->enter($__internal_f33a9e3ff19314b8e5b6206df32e84d67e2cc28032694d2d895ca374feb5811b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_f33a9e3ff19314b8e5b6206df32e84d67e2cc28032694d2d895ca374feb5811b->leave($__internal_f33a9e3ff19314b8e5b6206df32e84d67e2cc28032694d2d895ca374feb5811b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/email.txt.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\email.txt.twig");
    }
}
