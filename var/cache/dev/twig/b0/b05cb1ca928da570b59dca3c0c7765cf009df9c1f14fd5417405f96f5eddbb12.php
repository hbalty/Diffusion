<?php

/* incident/incident.html.twig */
class __TwigTemplate_b6f8c5fea8d168821c7b5217531599bce53dfba048e55c0fad561c869fc13d6b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "incident/incident.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c238e8a78cbc976d82e47cf9d90055e9852c837b0445c88e79de417f5449cfd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c238e8a78cbc976d82e47cf9d90055e9852c837b0445c88e79de417f5449cfd7->enter($__internal_c238e8a78cbc976d82e47cf9d90055e9852c837b0445c88e79de417f5449cfd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "incident/incident.html.twig"));

        $__internal_5e366697294209814e85a151f7e32c2fd5e13a8a6f223fb48a7119fb4895fd50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e366697294209814e85a151f7e32c2fd5e13a8a6f223fb48a7119fb4895fd50->enter($__internal_5e366697294209814e85a151f7e32c2fd5e13a8a6f223fb48a7119fb4895fd50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "incident/incident.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c238e8a78cbc976d82e47cf9d90055e9852c837b0445c88e79de417f5449cfd7->leave($__internal_c238e8a78cbc976d82e47cf9d90055e9852c837b0445c88e79de417f5449cfd7_prof);

        
        $__internal_5e366697294209814e85a151f7e32c2fd5e13a8a6f223fb48a7119fb4895fd50->leave($__internal_5e366697294209814e85a151f7e32c2fd5e13a8a6f223fb48a7119fb4895fd50_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_d25508bd93761793c24bca23164778675bae8ed3706a4bcddc7f7607342a006a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d25508bd93761793c24bca23164778675bae8ed3706a4bcddc7f7607342a006a->enter($__internal_d25508bd93761793c24bca23164778675bae8ed3706a4bcddc7f7607342a006a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_d51751dd8bee4fd1a05a33b689eeb9528cc15adfa58494530cc39dd303aaa60e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d51751dd8bee4fd1a05a33b689eeb9528cc15adfa58494530cc39dd303aaa60e->enter($__internal_d51751dd8bee4fd1a05a33b689eeb9528cc15adfa58494530cc39dd303aaa60e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>

  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "
";
        
        $__internal_d51751dd8bee4fd1a05a33b689eeb9528cc15adfa58494530cc39dd303aaa60e->leave($__internal_d51751dd8bee4fd1a05a33b689eeb9528cc15adfa58494530cc39dd303aaa60e_prof);

        
        $__internal_d25508bd93761793c24bca23164778675bae8ed3706a4bcddc7f7607342a006a->leave($__internal_d25508bd93761793c24bca23164778675bae8ed3706a4bcddc7f7607342a006a_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_746eb950d7e32384ffb9fa3fcb16efe149cc836dccebbe95f287f1cc5f4f15e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_746eb950d7e32384ffb9fa3fcb16efe149cc836dccebbe95f287f1cc5f4f15e4->enter($__internal_746eb950d7e32384ffb9fa3fcb16efe149cc836dccebbe95f287f1cc5f4f15e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_63a3ee84c3ff52d66a4e193c0e9e293fb94624e1058239e213e1f8f92bc66ddc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63a3ee84c3ff52d66a4e193c0e9e293fb94624e1058239e213e1f8f92bc66ddc->enter($__internal_63a3ee84c3ff52d66a4e193c0e9e293fb94624e1058239e213e1f8f92bc66ddc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "
    ";
        // line 15
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 16
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 18
        echo "
     ";
        
        $__internal_63a3ee84c3ff52d66a4e193c0e9e293fb94624e1058239e213e1f8f92bc66ddc->leave($__internal_63a3ee84c3ff52d66a4e193c0e9e293fb94624e1058239e213e1f8f92bc66ddc_prof);

        
        $__internal_746eb950d7e32384ffb9fa3fcb16efe149cc836dccebbe95f287f1cc5f4f15e4->leave($__internal_746eb950d7e32384ffb9fa3fcb16efe149cc836dccebbe95f287f1cc5f4f15e4_prof);

    }

    // line 23
    public function block_body($context, array $blocks = array())
    {
        $__internal_6fd87460217ee53ea3d6bdc08e816f5fb1529ad9a26a90895ff0972bade2d175 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fd87460217ee53ea3d6bdc08e816f5fb1529ad9a26a90895ff0972bade2d175->enter($__internal_6fd87460217ee53ea3d6bdc08e816f5fb1529ad9a26a90895ff0972bade2d175_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9def7a145336d20e208adfeded3fedd7266d0e311db17b54ea3d20f87b20169c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9def7a145336d20e208adfeded3fedd7266d0e311db17b54ea3d20f87b20169c->enter($__internal_9def7a145336d20e208adfeded3fedd7266d0e311db17b54ea3d20f87b20169c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 24
        echo "

<div class=\"right_col\" role=\"main\" style=\"min-height: 3742px;\">
\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Suivie Incident <small>Liste des incidents</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                          <li><a href=\"#\">Settings 1</a>
                          </li>
                          <li><a href=\"#\">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">





                    <div id=\"datatable_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap no-footer\">
\t\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t\t<div class=\"col-sm-6\">

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t<table id=\"datatable\" class=\"table table-striped table-bordered dataTable no-footer\" role=\"grid\" aria-describedby=\"datatable_info\">

\t\t\t\t\t  <thead>
                        <tr role=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-sort=\"ascending\" aria-label=\"Name: activate to sort column descending\" style=\"width: 143px;\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-sort=\"ascending\" aria-label=\"Name: activate to sort column descending\" style=\"width: 143px;\">Titre</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Position: activate to sort column ascending\" style=\"width: 236px;\">Client</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Office: activate to sort column ascending\" style=\"width: 105px;\">Statut</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Age: activate to sort column ascending\" style=\"width: 50px;\">Impact Metier</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Start date: activate to sort column ascending\" style=\"width: 101px;\">Impact Application</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Date Debut</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Date Fin</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Niveau impact</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Action</th>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
                      </thead>


                      <tbody>

                     \t\t";
        // line 83
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["incidents"]) ? $context["incidents"] : $this->getContext($context, "incidents")));
        foreach ($context['_seq'] as $context["_key"] => $context["incident"]) {
            // line 84
            echo "\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td> ";
            // line 85
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "id", array()), "html", null, true);
            echo "     </td>
\t\t\t\t\t\t\t<td> ";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "titre", array()), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td> ";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "client", array()), "getClientName", array(), "method"), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td> ";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "statut", array()), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t";
            // line 90
            if ($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactMetier", array())) {
                // line 91
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                $context["metierImpactes"] = $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactMetier", array());
                // line 92
                echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["metierImpactes"]) ? $context["metierImpactes"] : $this->getContext($context, "metierImpactes")));
                foreach ($context['_seq'] as $context["_key"] => $context["metierImpacte"]) {
                    // line 93
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li> \t";
                    // line 94
                    echo twig_escape_filter($this->env, $this->getAttribute($context["metierImpacte"], "nomMetier", array()), "html", null, true);
                    echo " </li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['metierImpacte'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 97
                echo "\t\t\t\t\t\t\t\t\t\t";
            } else {
                // line 98
                echo "\t\t\t\t\t\t\t\t\t\t Pas d'impact metier
\t\t\t\t\t\t\t\t\t\t ";
            }
            // line 100
            echo "\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t";
            // line 103
            if ($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactApplication", array())) {
                // line 104
                echo "\t\t\t\t\t\t\t\t\t";
                $context["applicationImpactees"] = $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impactApplication", array());
                // line 105
                echo "\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["applicationImpactees"]) ? $context["applicationImpactees"] : $this->getContext($context, "applicationImpactees")));
                foreach ($context['_seq'] as $context["_key"] => $context["applicationImpactee"]) {
                    // line 106
                    echo "\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li> \t";
                    // line 107
                    echo twig_escape_filter($this->env, $this->getAttribute($context["applicationImpactee"], "nomApplication", array()), "html", null, true);
                    echo " </li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['applicationImpactee'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 110
                echo "\t\t\t\t\t\t\t";
            } else {
                // line 111
                echo "\t\t\t\t\t\t\t Pas d'impact metier
\t\t\t\t\t\t\t ";
            }
            // line 113
            echo "
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t";
            // line 116
            if (($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateDebut", array()) != null)) {
                // line 117
                echo "\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateDebut", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "
\t\t\t\t\t\t\t";
            } else {
                // line 119
                echo "\t\t\t\t\t\t\t\t<span style=\"color:red\">\tNon connue  </span>
\t\t\t\t\t\t\t";
            }
            // line 121
            echo "\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t";
            // line 123
            if (($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateFin", array()) != null)) {
                // line 124
                echo "\t\t\t\t\t\t\t\t";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "dateFin", array()), "F jS \\a\\t g:ia"), "html", null, true);
                echo "
\t\t\t\t\t\t\t";
            } else {
                // line 126
                echo "\t\t\t\t\t\t\t\t<span style=\"color:red\">En cours  </span>
\t\t\t\t\t\t\t";
            }
            // line 128
            echo "
\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t<td> ";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "impact", array()), "nomImpact", array()), "html", null, true);
            echo " </td>
\t\t\t\t\t\t\t<td> <a href=\"";
            // line 132
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pos_add", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-plus\"></i> Pos </a>
\t\t\t\t\t\t\t<a href=\"";
            // line 133
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_history", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-menu-hamburger\"></i> Historique </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 134
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_delete", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\" onclick=\"return confirm('Vous êtes sure de vouloir supprimer lincident ? Cette action est irréversible ')\"> <i class=\"glyphicon glyphicon-trash\"></i> Supprimer  </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("diffusion_send", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idClient" => $this->getAttribute($this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "client", array()), "getId", array(), "method"))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-envelope\"></i> Envoyer mail </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 136
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_clone", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-duplicate\"> </i>  Cloner </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 137
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_clore", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-thumbs-up\"> </i> clore  </a>
\t\t\t\t\t\t\t <a href=\"";
            // line 138
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("incident_update", array("idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()), "idIncident" => $this->getAttribute($this->getAttribute($context["incident"], 0, array(), "array"), "Id", array()))), "html", null, true);
            echo "\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-pencil\"> </i>  Mettre à jour </a>

\t\t\t\t\t\t\t</td>



\t\t\t\t\t\t</div>
\t\t\t\t\t</tr>
 \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incident'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 147
        echo "

\t\t\t\t\t\t</tbody>
                    </table>
\t\t\t\t\t</div></div>


\t\t\t\t  </div>



\t\t\t\t\t</div>
                  </div>
                </div>





</div>




";
        
        $__internal_9def7a145336d20e208adfeded3fedd7266d0e311db17b54ea3d20f87b20169c->leave($__internal_9def7a145336d20e208adfeded3fedd7266d0e311db17b54ea3d20f87b20169c_prof);

        
        $__internal_6fd87460217ee53ea3d6bdc08e816f5fb1529ad9a26a90895ff0972bade2d175->leave($__internal_6fd87460217ee53ea3d6bdc08e816f5fb1529ad9a26a90895ff0972bade2d175_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_a209261d8032bf4aa51e207d00a6a6917f1b8e92ae1ee7f1720d8e5c2bf2a76f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a209261d8032bf4aa51e207d00a6a6917f1b8e92ae1ee7f1720d8e5c2bf2a76f->enter($__internal_a209261d8032bf4aa51e207d00a6a6917f1b8e92ae1ee7f1720d8e5c2bf2a76f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_be7b46b36d5d610356ec95e61d4a05bd0660bcc5a5fec4d3139c1eab902a4e2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be7b46b36d5d610356ec95e61d4a05bd0660bcc5a5fec4d3139c1eab902a4e2c->enter($__internal_be7b46b36d5d610356ec95e61d4a05bd0660bcc5a5fec4d3139c1eab902a4e2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 175
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 177
        echo "      ";
        
        $__internal_be7b46b36d5d610356ec95e61d4a05bd0660bcc5a5fec4d3139c1eab902a4e2c->leave($__internal_be7b46b36d5d610356ec95e61d4a05bd0660bcc5a5fec4d3139c1eab902a4e2c_prof);

        
        $__internal_a209261d8032bf4aa51e207d00a6a6917f1b8e92ae1ee7f1720d8e5c2bf2a76f->leave($__internal_a209261d8032bf4aa51e207d00a6a6917f1b8e92ae1ee7f1720d8e5c2bf2a76f_prof);

    }

    public function getTemplateName()
    {
        return "incident/incident.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 177,  439 => 175,  434 => 174,  425 => 173,  391 => 147,  376 => 138,  372 => 137,  368 => 136,  364 => 135,  360 => 134,  356 => 133,  352 => 132,  348 => 131,  343 => 128,  339 => 126,  333 => 124,  331 => 123,  327 => 121,  323 => 119,  317 => 117,  315 => 116,  310 => 113,  306 => 111,  303 => 110,  294 => 107,  291 => 106,  286 => 105,  283 => 104,  281 => 103,  276 => 100,  272 => 98,  269 => 97,  260 => 94,  257 => 93,  252 => 92,  249 => 91,  247 => 90,  242 => 88,  238 => 87,  234 => 86,  230 => 85,  227 => 84,  223 => 83,  162 => 24,  153 => 23,  142 => 18,  92 => 16,  88 => 15,  85 => 14,  76 => 13,  65 => 20,  63 => 13,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}

{% block header %}

<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>

  {% block stylesheets %}

    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}

     {% endblock %}

{% endblock %}

{% block body %}


<div class=\"right_col\" role=\"main\" style=\"min-height: 3742px;\">
\t<div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Suivie Incident <small>Liste des incidents</small></h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li class=\"dropdown\">
                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
                        <ul class=\"dropdown-menu\" role=\"menu\">
                          <li><a href=\"#\">Settings 1</a>
                          </li>
                          <li><a href=\"#\">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">





                    <div id=\"datatable_wrapper\" class=\"dataTables_wrapper form-inline dt-bootstrap no-footer\">
\t\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t\t<div class=\"col-sm-6\">

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-sm-12\">
\t\t\t\t\t\t\t\t<table id=\"datatable\" class=\"table table-striped table-bordered dataTable no-footer\" role=\"grid\" aria-describedby=\"datatable_info\">

\t\t\t\t\t  <thead>
                        <tr role=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-sort=\"ascending\" aria-label=\"Name: activate to sort column descending\" style=\"width: 143px;\">#</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-sort=\"ascending\" aria-label=\"Name: activate to sort column descending\" style=\"width: 143px;\">Titre</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Position: activate to sort column ascending\" style=\"width: 236px;\">Client</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Office: activate to sort column ascending\" style=\"width: 105px;\">Statut</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Age: activate to sort column ascending\" style=\"width: 50px;\">Impact Metier</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Start date: activate to sort column ascending\" style=\"width: 101px;\">Impact Application</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Date Debut</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Date Fin</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Niveau impact</th>
\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"sorting\" tabindex=\"0\" aria-controls=\"datatable\" rowspan=\"1\" colspan=\"1\" aria-label=\"Salary: activate to sort column ascending\" style=\"width: 77px;\">Action</th>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
                      </thead>


                      <tbody>

                     \t\t{% for incident in incidents %}
\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td> {{incident[0].id }}     </td>
\t\t\t\t\t\t\t<td> {{ incident[0].titre }} </td>
\t\t\t\t\t\t\t<td> {{ incident[0].client.getClientName() }} </td>
\t\t\t\t\t\t\t<td> {{ incident[0].statut }} </td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t\t{% if incident[0].impactMetier %}
\t\t\t\t\t\t\t\t\t\t\t\t{% set metierImpactes = incident[0].impactMetier %}
\t\t\t\t\t\t\t\t\t\t\t\t{% for metierImpacte in metierImpactes %}
\t\t\t\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t\t\t\t<li> \t{{ metierImpacte.nomMetier }} </li>
\t\t\t\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t Pas d'impact metier
\t\t\t\t\t\t\t\t\t\t {% endif %}
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>

\t\t\t\t\t\t\t{% if incident[0].impactApplication %}
\t\t\t\t\t\t\t\t\t{% set applicationImpactees = incident[0].impactApplication %}
\t\t\t\t\t\t\t\t\t{% for applicationImpactee in applicationImpactees %}
\t\t\t\t\t\t\t\t\t<ul>
\t\t\t\t\t\t\t\t\t\t<li> \t{{ applicationImpactee.nomApplication }} </li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t Pas d'impact metier
\t\t\t\t\t\t\t {% endif %}

\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t{% if incident[0].dateDebut != null %}
\t\t\t\t\t\t\t\t{{ incident[0].dateDebut | date(\"F jS \\\\a\\\\t g:ia\",\"Europe/Paris\") }}
\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<span style=\"color:red\">\tNon connue  </span>
\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t{% if incident[0].dateFin != null %}
\t\t\t\t\t\t\t\t{{ incident[0].dateFin | date(\"F jS \\\\a\\\\t g:ia\")}}
\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<span style=\"color:red\">En cours  </span>
\t\t\t\t\t\t\t{% endif %}

\t\t\t\t\t\t\t</td>

\t\t\t\t\t\t\t<td> {{ incident[0].impact.nomImpact }} </td>
\t\t\t\t\t\t\t<td> <a href=\"{{path('pos_add',{'idIncident' : incident[0].Id })}}\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-plus\"></i> Pos </a>
\t\t\t\t\t\t\t<a href=\"{{path('incident_history',{'idIncident' : incident[0].Id })}}\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-menu-hamburger\"></i> Historique </a>
\t\t\t\t\t\t\t <a href=\"{{path('incident_delete',{'idIncident' : incident[0].Id })}}\" class=\"btn btn-default btn-xs\" onclick=\"return confirm('Vous êtes sure de vouloir supprimer lincident ? Cette action est irréversible ')\"> <i class=\"glyphicon glyphicon-trash\"></i> Supprimer  </a>
\t\t\t\t\t\t\t <a href=\"{{path('diffusion_send',{'idIncident' : incident[0].Id, 'idClient' :  incident[0].client.getId() })}}\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-envelope\"></i> Envoyer mail </a>
\t\t\t\t\t\t\t <a href=\"{{path('incident_clone',{'idIncident' : incident[0].Id, 'idIncident' :  incident[0].Id })}}\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-duplicate\"> </i>  Cloner </a>
\t\t\t\t\t\t\t <a href=\"{{path('incident_clore',{'idIncident' : incident[0].Id, 'idIncident' :  incident[0].Id })}}\" class=\"btn btn-default btn-xs\"> <i class=\"glyphicon glyphicon-thumbs-up\"> </i> clore  </a>
\t\t\t\t\t\t\t <a href=\"{{path('incident_update',{'idIncident' : incident[0].Id, 'idIncident' :  incident[0].Id })}}\" class=\"btn btn-default btn-xs\"><i class=\"glyphicon glyphicon-pencil\"> </i>  Mettre à jour </a>

\t\t\t\t\t\t\t</td>



\t\t\t\t\t\t</div>
\t\t\t\t\t</tr>
 \t\t{% endfor %}


\t\t\t\t\t\t</tbody>
                    </table>
\t\t\t\t\t</div></div>


\t\t\t\t  </div>



\t\t\t\t\t</div>
                  </div>
                </div>





</div>




{% endblock %}

\t{% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}
", "incident/incident.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\incident\\incident.html.twig");
    }
}
