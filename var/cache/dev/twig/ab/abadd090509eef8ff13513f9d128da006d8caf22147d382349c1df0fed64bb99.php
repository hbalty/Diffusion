<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_2693e5e51a1b4b15a8bd1ac348e34582249b8c0a7831b6d952993e076cfd16fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d4dc339a69e0638523f29e08e6bfe12ef9781cb9b76c9c9cd69742fbc562fe0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d4dc339a69e0638523f29e08e6bfe12ef9781cb9b76c9c9cd69742fbc562fe0->enter($__internal_8d4dc339a69e0638523f29e08e6bfe12ef9781cb9b76c9c9cd69742fbc562fe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_8d4dc339a69e0638523f29e08e6bfe12ef9781cb9b76c9c9cd69742fbc562fe0->leave($__internal_8d4dc339a69e0638523f29e08e6bfe12ef9781cb9b76c9c9cd69742fbc562fe0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_row.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_row.html.php");
    }
}
