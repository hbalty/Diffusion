<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_09378dc311bce2bbe5d7b11b575a4359299bd31529a440e873382ea2a807df74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bbd8d8e53bd6edd0c99adf7c5b3e63ba4b7b96d930ae5455fb9a3f1c6bbe5c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bbd8d8e53bd6edd0c99adf7c5b3e63ba4b7b96d930ae5455fb9a3f1c6bbe5c5->enter($__internal_5bbd8d8e53bd6edd0c99adf7c5b3e63ba4b7b96d930ae5455fb9a3f1c6bbe5c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_1bb589809cbbb81767041567eb85012d74db923c70df07775f921afe048a4f8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1bb589809cbbb81767041567eb85012d74db923c70df07775f921afe048a4f8f->enter($__internal_1bb589809cbbb81767041567eb85012d74db923c70df07775f921afe048a4f8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5bbd8d8e53bd6edd0c99adf7c5b3e63ba4b7b96d930ae5455fb9a3f1c6bbe5c5->leave($__internal_5bbd8d8e53bd6edd0c99adf7c5b3e63ba4b7b96d930ae5455fb9a3f1c6bbe5c5_prof);

        
        $__internal_1bb589809cbbb81767041567eb85012d74db923c70df07775f921afe048a4f8f->leave($__internal_1bb589809cbbb81767041567eb85012d74db923c70df07775f921afe048a4f8f_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_d1a47206f52e80a8f68a393268a2a675a2c9b6521b85e45fc23f2436aebaaa02 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1a47206f52e80a8f68a393268a2a675a2c9b6521b85e45fc23f2436aebaaa02->enter($__internal_d1a47206f52e80a8f68a393268a2a675a2c9b6521b85e45fc23f2436aebaaa02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_e7c1bf2615c263a4dfd060002bd59316e2076d375074be33b7d47557b595732e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7c1bf2615c263a4dfd060002bd59316e2076d375074be33b7d47557b595732e->enter($__internal_e7c1bf2615c263a4dfd060002bd59316e2076d375074be33b7d47557b595732e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_e7c1bf2615c263a4dfd060002bd59316e2076d375074be33b7d47557b595732e->leave($__internal_e7c1bf2615c263a4dfd060002bd59316e2076d375074be33b7d47557b595732e_prof);

        
        $__internal_d1a47206f52e80a8f68a393268a2a675a2c9b6521b85e45fc23f2436aebaaa02->leave($__internal_d1a47206f52e80a8f68a393268a2a675a2c9b6521b85e45fc23f2436aebaaa02_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-requests\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\ajax.html.twig");
    }
}
