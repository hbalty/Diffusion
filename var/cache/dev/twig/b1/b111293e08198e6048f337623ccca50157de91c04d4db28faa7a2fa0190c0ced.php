<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_c07803955aa031a7557b7add470f6567734f6abe43a5aaf651f2cff58ab8111e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_261bd8e6611c2051c25d0f566175b319e58f994f14830e4a8e82b8979b121022 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_261bd8e6611c2051c25d0f566175b319e58f994f14830e4a8e82b8979b121022->enter($__internal_261bd8e6611c2051c25d0f566175b319e58f994f14830e4a8e82b8979b121022_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_261bd8e6611c2051c25d0f566175b319e58f994f14830e4a8e82b8979b121022->leave($__internal_261bd8e6611c2051c25d0f566175b319e58f994f14830e4a8e82b8979b121022_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_15f7acb9456e3a8e9f2f165a9004feec2cfd140d0505a59472d1d9593bfdd33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15f7acb9456e3a8e9f2f165a9004feec2cfd140d0505a59472d1d9593bfdd33e->enter($__internal_15f7acb9456e3a8e9f2f165a9004feec2cfd140d0505a59472d1d9593bfdd33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_15f7acb9456e3a8e9f2f165a9004feec2cfd140d0505a59472d1d9593bfdd33e->leave($__internal_15f7acb9456e3a8e9f2f165a9004feec2cfd140d0505a59472d1d9593bfdd33e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_d7214e577dc3d778fa80a7cc075d9267ddd7802e15574429be759c2435758413 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7214e577dc3d778fa80a7cc075d9267ddd7802e15574429be759c2435758413->enter($__internal_d7214e577dc3d778fa80a7cc075d9267ddd7802e15574429be759c2435758413_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : null), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : null), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : null), (isset($context["line"]) ? $context["line"] : null),  -1);
        echo "
</div>
";
        
        $__internal_d7214e577dc3d778fa80a7cc075d9267ddd7802e15574429be759c2435758413->leave($__internal_d7214e577dc3d778fa80a7cc075d9267ddd7802e15574429be759c2435758413_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 15,  69 => 12,  63 => 11,  60 => 10,  54 => 9,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/open.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}
