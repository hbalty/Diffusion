<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_0889c8de84ee72e0c0dc9c764552fb8cd1ddf697209f8ecbe8c28558a5693365 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_619e303e3b6d6007846c498e3af7d63368ae69093bf85ddd97d4e0295b0e8ac6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_619e303e3b6d6007846c498e3af7d63368ae69093bf85ddd97d4e0295b0e8ac6->enter($__internal_619e303e3b6d6007846c498e3af7d63368ae69093bf85ddd97d4e0295b0e8ac6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_619e303e3b6d6007846c498e3af7d63368ae69093bf85ddd97d4e0295b0e8ac6->leave($__internal_619e303e3b6d6007846c498e3af7d63368ae69093bf85ddd97d4e0295b0e8ac6_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_036463d4d703e7e3e30a0edd559af40318143989df7d012b886a55d49e9d93b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_036463d4d703e7e3e30a0edd559af40318143989df7d012b886a55d49e9d93b2->enter($__internal_036463d4d703e7e3e30a0edd559af40318143989df7d012b886a55d49e9d93b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_036463d4d703e7e3e30a0edd559af40318143989df7d012b886a55d49e9d93b2->leave($__internal_036463d4d703e7e3e30a0edd559af40318143989df7d012b886a55d49e9d93b2_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/ajax_layout.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}
