<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_2d984ad7040c93b423a3f61a4ebc3f172d2fcb265eead39e79b77fd5b14df5ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f2b171fd1a18660ea471e3fc2fdaf971cfe943e224dec1593635ac272813d5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f2b171fd1a18660ea471e3fc2fdaf971cfe943e224dec1593635ac272813d5d->enter($__internal_8f2b171fd1a18660ea471e3fc2fdaf971cfe943e224dec1593635ac272813d5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8f2b171fd1a18660ea471e3fc2fdaf971cfe943e224dec1593635ac272813d5d->leave($__internal_8f2b171fd1a18660ea471e3fc2fdaf971cfe943e224dec1593635ac272813d5d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_03d08843174c177fea95b64123e85195f5106c9276ac6137600146d6b4249739 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03d08843174c177fea95b64123e85195f5106c9276ac6137600146d6b4249739->enter($__internal_03d08843174c177fea95b64123e85195f5106c9276ac6137600146d6b4249739_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_03d08843174c177fea95b64123e85195f5106c9276ac6137600146d6b4249739->leave($__internal_03d08843174c177fea95b64123e85195f5106c9276ac6137600146d6b4249739_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_dabd209f91de78debae1dada7197eb2e312f503f226425b0a2e3957c7fde7bb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dabd209f91de78debae1dada7197eb2e312f503f226425b0a2e3957c7fde7bb1->enter($__internal_dabd209f91de78debae1dada7197eb2e312f503f226425b0a2e3957c7fde7bb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : null), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : null), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : null), (isset($context["line"]) ? $context["line"] : null),  -1);
        echo "
</div>
";
        
        $__internal_dabd209f91de78debae1dada7197eb2e312f503f226425b0a2e3957c7fde7bb1->leave($__internal_dabd209f91de78debae1dada7197eb2e312f503f226425b0a2e3957c7fde7bb1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 15,  69 => 12,  63 => 11,  60 => 10,  54 => 9,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Profiler:open.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
