<?php

/* production/login.html */
class __TwigTemplate_4295d57b97a439886cec7bb4f90b31c632af9da340c94ae1695e2f36b20439cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a17d7744859fcb8ce9bae8ac7e7a49aaf2d7e650ba960dc9934af5663879ed4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a17d7744859fcb8ce9bae8ac7e7a49aaf2d7e650ba960dc9934af5663879ed4->enter($__internal_1a17d7744859fcb8ce9bae8ac7e7a49aaf2d7e650ba960dc9934af5663879ed4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "production/login.html"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href=\"../vendors/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\">
    <!-- Font Awesome -->
    <link href=\"../vendors/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\">
    <!-- NProgress -->
    <link href=\"../vendors/nprogress/nprogress.css\" rel=\"stylesheet\">
    <!-- Animate.css -->
    <link href=\"../vendors/animate.css/animate.min.css\" rel=\"stylesheet\">

    <!-- Custom Theme Style -->
    <link href=\"../build/css/custom.css\" rel=\"stylesheet\">
  </head>

  <body class=\"login\">
      <header> 
        <div class=\"login_header\"> 
            <img class=\"logo_cds\" src=\"http://club-commerce-connecte.com/wp-content/uploads/2016/11/Cdiscount-Logo-2016-1.png\">
          
        </div>
      </header>
    <div>
      <a class=\"hiddenanchor\" id=\"signup\"></a>
      <a class=\"hiddenanchor\" id=\"signin\"></a>

      <div class=\"login_wrapper\">
         
        <div class=\"animate form login_form\">
             <img  class=\"login_logo\" src=\"../build/images/login-logo.png\"> 
          <section class=\"login_content\">
            <form>
              <h1>Se connecter </h1>
              <div>
                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" required=\"\" />
              </div>
              <div>
                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" required=\"\" />
              </div>
              <div>
                <a class=\"btn btn-default submit\" href=\"index.html\">Log in</a>
              </div>

              <div class=\"clearfix\"></div>

              <div class=\"separator\">
                </p>

                <div class=\"clearfix\"></div>
                <br />

                <div>
                  <h1> Outil de diffusion Cdiscount © </h1>
                  <p>©2016 All Rights Reserved. Cdiscount!</p>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id=\"register\" class=\"animate form registration_form\">
          <section class=\"login_content\">
            <form>
              <h1>Create Account</h1>
              <div>
                <input type=\"text\" class=\"form-control\" placeholder=\"Username\" required=\"\" />
              </div>
              <div>
                <input type=\"email\" class=\"form-control\" placeholder=\"Email\" required=\"\" />
              </div>
              <div>
                <input type=\"password\" class=\"form-control\" placeholder=\"Password\" required=\"\" />
              </div>
              <div>
                <a class=\"btn btn-default submit\" href=\"index.html\">Submit</a>
              </div>

              <div class=\"clearfix\"></div>

              <div class=\"separator\">
                <p class=\"change_link\">Already a member ?
                  <a href=\"#signin\" class=\"to_register\"> Log in </a>
                </p>

                <div class=\"clearfix\"></div>
                <br />

                <div>
                  <h1><i class=\"fa fa-paw\"></i> Gentelella Alela!</h1>
                  <p>©2016 All Rights Reserved. Gentelella Alela! is a Bootstrap 3 template. Privacy and Terms</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
";
        
        $__internal_1a17d7744859fcb8ce9bae8ac7e7a49aaf2d7e650ba960dc9934af5663879ed4->leave($__internal_1a17d7744859fcb8ce9bae8ac7e7a49aaf2d7e650ba960dc9934af5663879ed4_prof);

    }

    public function getTemplateName()
    {
        return "production/login.html";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "production/login.html", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\production\\login.html");
    }
}
