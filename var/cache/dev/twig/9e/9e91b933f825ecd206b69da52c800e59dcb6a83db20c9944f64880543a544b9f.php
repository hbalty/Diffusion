<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_e6831d6f263b7c7eb28e2751e703874c58f45d9b0e2327567d17c0ac3715c6c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b294faf0284c9f6a969170a307cd392d30904301bcadb3021b49b0f938650c78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b294faf0284c9f6a969170a307cd392d30904301bcadb3021b49b0f938650c78->enter($__internal_b294faf0284c9f6a969170a307cd392d30904301bcadb3021b49b0f938650c78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b294faf0284c9f6a969170a307cd392d30904301bcadb3021b49b0f938650c78->leave($__internal_b294faf0284c9f6a969170a307cd392d30904301bcadb3021b49b0f938650c78_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_21a82fc02884bf34719563face4d4c6f051ffa06bcc538af7526308cf03b9681 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21a82fc02884bf34719563face4d4c6f051ffa06bcc538af7526308cf03b9681->enter($__internal_21a82fc02884bf34719563face4d4c6f051ffa06bcc538af7526308cf03b9681_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_21a82fc02884bf34719563face4d4c6f051ffa06bcc538af7526308cf03b9681->leave($__internal_21a82fc02884bf34719563face4d4c6f051ffa06bcc538af7526308cf03b9681_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Profile:show.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Profile/show.html.twig");
    }
}
