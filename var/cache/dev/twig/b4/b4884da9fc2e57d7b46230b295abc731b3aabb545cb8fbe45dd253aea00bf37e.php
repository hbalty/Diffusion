<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_345b856d7a5bb07d474e902eb36a3c3e7922330536e8fc7ec973c068da8da185 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_04f38d38b4bdd05526ea630308c2b4167badac0a6d06cb2a0087dd8a2644f628 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04f38d38b4bdd05526ea630308c2b4167badac0a6d06cb2a0087dd8a2644f628->enter($__internal_04f38d38b4bdd05526ea630308c2b4167badac0a6d06cb2a0087dd8a2644f628_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_04f38d38b4bdd05526ea630308c2b4167badac0a6d06cb2a0087dd8a2644f628->leave($__internal_04f38d38b4bdd05526ea630308c2b4167badac0a6d06cb2a0087dd8a2644f628_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_rows.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rows.html.php");
    }
}
