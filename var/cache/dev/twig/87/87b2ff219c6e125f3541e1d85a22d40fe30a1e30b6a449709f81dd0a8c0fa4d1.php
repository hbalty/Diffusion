<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_4c667a9d7882b909a0b83eb446c4520abf6cf9e29b543e62b00f97c4be7514a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2e6681311fdda5ae4a5d9e24dee44d93c6c298146be590b25727bd210ce7508 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2e6681311fdda5ae4a5d9e24dee44d93c6c298146be590b25727bd210ce7508->enter($__internal_b2e6681311fdda5ae4a5d9e24dee44d93c6c298146be590b25727bd210ce7508_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b2e6681311fdda5ae4a5d9e24dee44d93c6c298146be590b25727bd210ce7508->leave($__internal_b2e6681311fdda5ae4a5d9e24dee44d93c6c298146be590b25727bd210ce7508_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f587382b3c3449ccda1926751b024525a8a3da697d26a1a1dd2d0ad5c9bcb662 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f587382b3c3449ccda1926751b024525a8a3da697d26a1a1dd2d0ad5c9bcb662->enter($__internal_f587382b3c3449ccda1926751b024525a8a3da697d26a1a1dd2d0ad5c9bcb662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_f587382b3c3449ccda1926751b024525a8a3da697d26a1a1dd2d0ad5c9bcb662->leave($__internal_f587382b3c3449ccda1926751b024525a8a3da697d26a1a1dd2d0ad5c9bcb662_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:list.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/list.html.twig");
    }
}
