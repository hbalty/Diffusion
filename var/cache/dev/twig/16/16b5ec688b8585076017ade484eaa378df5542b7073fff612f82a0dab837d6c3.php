<?php

/* ::loginTemplate.html.twig */
class __TwigTemplate_ead6f42bf967ccb442aa549a7d129674d45ed895c3ffff8431b5e0261fdffa34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9ad11c7bfa620a4dee99dd27b0a3deb08bef2900c80c2cabfae3d0703dadf06 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9ad11c7bfa620a4dee99dd27b0a3deb08bef2900c80c2cabfae3d0703dadf06->enter($__internal_a9ad11c7bfa620a4dee99dd27b0a3deb08bef2900c80c2cabfae3d0703dadf06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::loginTemplate.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
     ";
        // line 4
        $this->displayBlock('header', $context, $blocks);
        // line 23
        echo "  </head>

  <body class=\"login\">
      


      <header> 
        <div class=\"login_header\"> 
            <img class=\"logo_cds\" src=\"http://club-commerce-connecte.com/wp-content/uploads/2016/11/Cdiscount-Logo-2016-1.png\">
        </div>
      </header>
    <div>
      <a class=\"hiddenanchor\" id=\"signup\"></a>
      <a class=\"hiddenanchor\" id=\"signin\"></a>

      <div class=\"login_wrapper\">
         
        <div class=\"animate form login_form\">
                         ";
        // line 41
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "87487f8_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_87487f8_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/87487f8_login-logo_1.png");
            // line 42
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" alt=\"logo\" class=\"login_logo\" />
                          ";
        } else {
            // asset "87487f8"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_87487f8") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/87487f8.png");
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" alt=\"logo\" class=\"login_logo\" />
                          ";
        }
        unset($context["asset_url"]);
        // line 44
        echo "          <section class=\"login_content\">
           ";
        // line 45
        $this->displayBlock('body', $context, $blocks);
        // line 48
        echo "          </section>
        </div>

      </div>
    </div>
        
  </body>
</html>
";
        
        $__internal_a9ad11c7bfa620a4dee99dd27b0a3deb08bef2900c80c2cabfae3d0703dadf06->leave($__internal_a9ad11c7bfa620a4dee99dd27b0a3deb08bef2900c80c2cabfae3d0703dadf06_prof);

    }

    // line 4
    public function block_header($context, array $blocks = array())
    {
        $__internal_a33e90925aa6611b504fe82f198a272b170c523600a6ee76f746fb28d751945e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a33e90925aa6611b504fe82f198a272b170c523600a6ee76f746fb28d751945e->enter($__internal_a33e90925aa6611b504fe82f198a272b170c523600a6ee76f746fb28d751945e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        echo "  
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    ";
        // line 14
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 21
        echo "    
    ";
        
        $__internal_a33e90925aa6611b504fe82f198a272b170c523600a6ee76f746fb28d751945e->leave($__internal_a33e90925aa6611b504fe82f198a272b170c523600a6ee76f746fb28d751945e_prof);

    }

    // line 14
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_07b958b18282cba67134ef045b4a0150a785e7013d24ff54a82b4c800f92b0ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07b958b18282cba67134ef045b4a0150a785e7013d24ff54a82b4c800f92b0ad->enter($__internal_07b958b18282cba67134ef045b4a0150a785e7013d24ff54a82b4c800f92b0ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 15
        echo "      
    ";
        // line 16
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 17
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 19
        echo "      
    ";
        
        $__internal_07b958b18282cba67134ef045b4a0150a785e7013d24ff54a82b4c800f92b0ad->leave($__internal_07b958b18282cba67134ef045b4a0150a785e7013d24ff54a82b4c800f92b0ad_prof);

    }

    // line 45
    public function block_body($context, array $blocks = array())
    {
        $__internal_13daae138b8c94af2433ab1590b986f6159aa98089380a87a9de90fcb45b2484 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13daae138b8c94af2433ab1590b986f6159aa98089380a87a9de90fcb45b2484->enter($__internal_13daae138b8c94af2433ab1590b986f6159aa98089380a87a9de90fcb45b2484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 46
        echo "
            ";
        
        $__internal_13daae138b8c94af2433ab1590b986f6159aa98089380a87a9de90fcb45b2484->leave($__internal_13daae138b8c94af2433ab1590b986f6159aa98089380a87a9de90fcb45b2484_prof);

    }

    public function getTemplateName()
    {
        return "::loginTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 46,  188 => 45,  180 => 19,  130 => 17,  126 => 16,  123 => 15,  117 => 14,  109 => 21,  107 => 14,  90 => 4,  75 => 48,  73 => 45,  70 => 44,  56 => 42,  52 => 41,  32 => 23,  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "::loginTemplate.html.twig", "C:\\xampp2\\htdocs\\symfony\\app/Resources\\views/loginTemplate.html.twig");
    }
}
