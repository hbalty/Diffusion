<?php

/* diffusion/history.html.twig */
class __TwigTemplate_36345728320d6fd29df1bff9b03c839837de4de279a62735bba68f6a6eed5186 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "diffusion/history.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1e341146707ffd3fe64f260d67c902f3c623e5e65d476cda0fddfb6a01a42503 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e341146707ffd3fe64f260d67c902f3c623e5e65d476cda0fddfb6a01a42503->enter($__internal_1e341146707ffd3fe64f260d67c902f3c623e5e65d476cda0fddfb6a01a42503_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "diffusion/history.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1e341146707ffd3fe64f260d67c902f3c623e5e65d476cda0fddfb6a01a42503->leave($__internal_1e341146707ffd3fe64f260d67c902f3c623e5e65d476cda0fddfb6a01a42503_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_5894de5a631b68c71487dc8e62ae4747e94ecdf00e9c796226cdee37cfc7b66f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5894de5a631b68c71487dc8e62ae4747e94ecdf00e9c796226cdee37cfc7b66f->enter($__internal_5894de5a631b68c71487dc8e62ae4747e94ecdf00e9c796226cdee37cfc7b66f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>
  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "

";
        
        $__internal_5894de5a631b68c71487dc8e62ae4747e94ecdf00e9c796226cdee37cfc7b66f->leave($__internal_5894de5a631b68c71487dc8e62ae4747e94ecdf00e9c796226cdee37cfc7b66f_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_1f61f9bab4b0cdf9f0d83ec6bea9ad6e9dd5fc537e005691e18e604f7e880096 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f61f9bab4b0cdf9f0d83ec6bea9ad6e9dd5fc537e005691e18e604f7e880096->enter($__internal_1f61f9bab4b0cdf9f0d83ec6bea9ad6e9dd5fc537e005691e18e604f7e880096_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "\t  
\t";
        // line 15
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 16
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 18
        echo "\t  
\t ";
        
        $__internal_1f61f9bab4b0cdf9f0d83ec6bea9ad6e9dd5fc537e005691e18e604f7e880096->leave($__internal_1f61f9bab4b0cdf9f0d83ec6bea9ad6e9dd5fc537e005691e18e604f7e880096_prof);

    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        $__internal_9c2f82d7fe5e6811eda5f8b6dc618fb06d5423f76794fa480d6b129eb1c99662 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c2f82d7fe5e6811eda5f8b6dc618fb06d5423f76794fa480d6b129eb1c99662->enter($__internal_9c2f82d7fe5e6811eda5f8b6dc618fb06d5423f76794fa480d6b129eb1c99662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "



<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2> Timeline </h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
<ul class=\"timeline\">
\t\t
\t\t
\t\t";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history"]) ? $context["history"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
            // line 57
            echo "\t\t\t";
            if ((($this->getAttribute($context["action"], "id", array()) % 2) == 0)) {
                // line 58
                echo "\t\t\t\t\t        <li>
\t\t\t\t\t\t<div class=\"timeline-badge info\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
\t\t\t\t\t\t<div class=\"timeline-panel\">
\t\t\t\t\t\t  <div class=\"timeline-heading\">
\t\t\t\t\t\t\t<h4 class=\"timeline-title\"> Envoi de mail </h4>
\t\t\t\t\t\t\t<p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
                // line 63
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "dateEnvoi", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "  </small></p>
\t\t\t\t\t\t  </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t <h5> Destinataires : </h5> 
\t\t\t\t ";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destinataire", array()), "html", null, true);
                echo "

\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires CC : </h5>
\t\t\t\t";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destEnCopie", array()), "html", null, true);
                echo "
\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<h5> Categorie :
\t\t\t\t\t";
                // line 77
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["action"], "template", array()), "categorie", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
                    // line 78
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "nomCategorie", array()), "html", null, true);
                    echo "
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "\t\t\t\t</h5>
\t\t\t\t<p> Envoyé par : ";
                // line 81
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "emetteur", array()), "html", null, true);
                echo "</p> <br>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t";
                // line 85
                echo $this->getAttribute($context["action"], "contenu", array());
                echo "
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t\t</p>
\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#";
                // line 93
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\tAfficher mail 
\t\t\t\t</button>
            </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </li>
\t\t";
            } else {
                // line 100
                echo "\t\t\t    <li class=\"timeline-inverted\">
          <div class=\"timeline-badge warning\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
          <div class=\"timeline-panel\">
            <div class=\"timeline-heading\">
              <h4 class=\"timeline-title\">Envoi de mail </h4>
\t\t\t  <p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
                // line 105
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "dateEnvoi", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "  </small></p>
            </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t <h5> Destinataires : </h5> 
\t\t\t\t ";
                // line 110
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destinataire", array()), "html", null, true);
                echo "

\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires CC : </h5>
\t\t\t\t";
                // line 114
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destEnCopie", array()), "html", null, true);
                echo "
\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<h5>
\t\t\t\t\t";
                // line 119
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["action"], "template", array()), "categorie", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
                    // line 120
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "nomCategorie", array()), "html", null, true);
                    echo "
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 122
                echo "\t\t\t\t</h5>
\t\t\t\t<p> Envoyé par : ";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "emetteur", array()), "html", null, true);
                echo "</p><br>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"";
                // line 124
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t";
                // line 127
                echo $this->getAttribute($context["action"], "contenu", array());
                echo "
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t\t</p>
\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#";
                // line 135
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\tAfficher mail 
\t\t\t\t</button>
            </div>
        </li>
\t";
            }
            // line 141
            echo "\t

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 144
        echo "\t\t
\t\t
        
 

    </ul>

    


    </div>
</div>
</div>
</div>



<!-- Modal -->


";
        
        $__internal_9c2f82d7fe5e6811eda5f8b6dc618fb06d5423f76794fa480d6b129eb1c99662->leave($__internal_9c2f82d7fe5e6811eda5f8b6dc618fb06d5423f76794fa480d6b129eb1c99662_prof);

    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_9ccb6e19bdcb605b68d94323d511c6e4181e6abe923e4ee3b92c88cb4b65c7bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ccb6e19bdcb605b68d94323d511c6e4181e6abe923e4ee3b92c88cb4b65c7bc->enter($__internal_9ccb6e19bdcb605b68d94323d511c6e4181e6abe923e4ee3b92c88cb4b65c7bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 167
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 168
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t\t";
        }
        unset($context["asset_url"]);
        // line 170
        echo "\t  ";
        
        $__internal_9ccb6e19bdcb605b68d94323d511c6e4181e6abe923e4ee3b92c88cb4b65c7bc->leave($__internal_9ccb6e19bdcb605b68d94323d511c6e4181e6abe923e4ee3b92c88cb4b65c7bc_prof);

    }

    public function getTemplateName()
    {
        return "diffusion/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  437 => 170,  381 => 168,  376 => 167,  370 => 166,  343 => 144,  335 => 141,  326 => 135,  315 => 127,  309 => 124,  305 => 123,  302 => 122,  293 => 120,  289 => 119,  281 => 114,  274 => 110,  266 => 105,  259 => 100,  249 => 93,  238 => 85,  232 => 82,  228 => 81,  225 => 80,  216 => 78,  212 => 77,  204 => 72,  197 => 68,  189 => 63,  182 => 58,  179 => 57,  175 => 56,  142 => 25,  136 => 24,  128 => 18,  78 => 16,  74 => 15,  71 => 14,  65 => 13,  56 => 20,  54 => 13,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "diffusion/history.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\diffusion\\history.html.twig");
    }
}
