<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_b63bd83df405156069cb1b015eb316fa9c9406e181f5bc26265e179bbeaff26a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25433adba91555b90b5eb67e0c4353e9208d5097c08d57387922bc62cdbec243 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25433adba91555b90b5eb67e0c4353e9208d5097c08d57387922bc62cdbec243->enter($__internal_25433adba91555b90b5eb67e0c4353e9208d5097c08d57387922bc62cdbec243_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_25433adba91555b90b5eb67e0c4353e9208d5097c08d57387922bc62cdbec243->leave($__internal_25433adba91555b90b5eb67e0c4353e9208d5097c08d57387922bc62cdbec243_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4dc29e4c94d1ca6789b19f0bcdb5c4753216c9df693f6fd0f782118b66598774 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4dc29e4c94d1ca6789b19f0bcdb5c4753216c9df693f6fd0f782118b66598774->enter($__internal_4dc29e4c94d1ca6789b19f0bcdb5c4753216c9df693f6fd0f782118b66598774_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 4)->display($context);
        
        $__internal_4dc29e4c94d1ca6789b19f0bcdb5c4753216c9df693f6fd0f782118b66598774->leave($__internal_4dc29e4c94d1ca6789b19f0bcdb5c4753216c9df693f6fd0f782118b66598774_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/reset.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\reset.html.twig");
    }
}
