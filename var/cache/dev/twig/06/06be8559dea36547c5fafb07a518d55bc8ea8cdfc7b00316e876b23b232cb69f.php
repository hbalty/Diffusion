<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_56135012ddd3d22f5b59766a48b5e542dea19158c59581ed41dc506742543e64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ebc8a440cc72908facbe26ef58d3506114e950e2dca1a3d3af592d6324c6e8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ebc8a440cc72908facbe26ef58d3506114e950e2dca1a3d3af592d6324c6e8e->enter($__internal_2ebc8a440cc72908facbe26ef58d3506114e950e2dca1a3d3af592d6324c6e8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2ebc8a440cc72908facbe26ef58d3506114e950e2dca1a3d3af592d6324c6e8e->leave($__internal_2ebc8a440cc72908facbe26ef58d3506114e950e2dca1a3d3af592d6324c6e8e_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f179619a52b8918439e90a4a26c12af474992fd8600186a6897e81d4f9436ccc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f179619a52b8918439e90a4a26c12af474992fd8600186a6897e81d4f9436ccc->enter($__internal_f179619a52b8918439e90a4a26c12af474992fd8600186a6897e81d4f9436ccc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_f179619a52b8918439e90a4a26c12af474992fd8600186a6897e81d4f9436ccc->leave($__internal_f179619a52b8918439e90a4a26c12af474992fd8600186a6897e81d4f9436ccc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:request.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
