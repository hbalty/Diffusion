<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_0f7c0dc4f9c5cc15e6596e0b0bff45355db81910b987d4f300bdfa26fc66ea58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71898f611e8f7c3238d0a7591a83c68e91ed35b358822699fae7d3d07aa1eea6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71898f611e8f7c3238d0a7591a83c68e91ed35b358822699fae7d3d07aa1eea6->enter($__internal_71898f611e8f7c3238d0a7591a83c68e91ed35b358822699fae7d3d07aa1eea6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_71898f611e8f7c3238d0a7591a83c68e91ed35b358822699fae7d3d07aa1eea6->leave($__internal_71898f611e8f7c3238d0a7591a83c68e91ed35b358822699fae7d3d07aa1eea6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/container_attributes.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
