<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_e27d4a52cbe375416c1b807f2d39514f92c751cebdbd4b521829ced561cac22e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Registration/register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3092b1a6736a7a70cd993f1d08a10298262a0a157ea30a8ad9173d3262684976 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3092b1a6736a7a70cd993f1d08a10298262a0a157ea30a8ad9173d3262684976->enter($__internal_3092b1a6736a7a70cd993f1d08a10298262a0a157ea30a8ad9173d3262684976_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3092b1a6736a7a70cd993f1d08a10298262a0a157ea30a8ad9173d3262684976->leave($__internal_3092b1a6736a7a70cd993f1d08a10298262a0a157ea30a8ad9173d3262684976_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_88e44afb05b97d1dfe671931b3231727f3282820235513d0804f4a10351efba2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88e44afb05b97d1dfe671931b3231727f3282820235513d0804f4a10351efba2->enter($__internal_88e44afb05b97d1dfe671931b3231727f3282820235513d0804f4a10351efba2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 4)->display($context);
        
        $__internal_88e44afb05b97d1dfe671931b3231727f3282820235513d0804f4a10351efba2->leave($__internal_88e44afb05b97d1dfe671931b3231727f3282820235513d0804f4a10351efba2_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/register.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\register.html.twig");
    }
}
