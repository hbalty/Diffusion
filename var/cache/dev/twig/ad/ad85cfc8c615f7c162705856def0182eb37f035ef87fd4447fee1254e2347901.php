<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_28e6905b3132b1c5c0bb3c8560145232ecb6de264746e6ca2aa8a41081475e1d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7ed34a5bee645b80cfa724f846bb4b3103a9ef995faa5bea9adf2bdb688550b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7ed34a5bee645b80cfa724f846bb4b3103a9ef995faa5bea9adf2bdb688550b->enter($__internal_c7ed34a5bee645b80cfa724f846bb4b3103a9ef995faa5bea9adf2bdb688550b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c7ed34a5bee645b80cfa724f846bb4b3103a9ef995faa5bea9adf2bdb688550b->leave($__internal_c7ed34a5bee645b80cfa724f846bb4b3103a9ef995faa5bea9adf2bdb688550b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_a8694d12a2e473558e28aa672b2b48740fbae42ce22a377e4e8e8b53d663f50d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8694d12a2e473558e28aa672b2b48740fbae42ce22a377e4e8e8b53d663f50d->enter($__internal_a8694d12a2e473558e28aa672b2b48740fbae42ce22a377e4e8e8b53d663f50d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_a8694d12a2e473558e28aa672b2b48740fbae42ce22a377e4e8e8b53d663f50d->leave($__internal_a8694d12a2e473558e28aa672b2b48740fbae42ce22a377e4e8e8b53d663f50d_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_1acde8e3e6f9ccb7fc62e291c5e7b3d3a4dd32e386382c3e6e8714621b24a939 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1acde8e3e6f9ccb7fc62e291c5e7b3d3a4dd32e386382c3e6e8714621b24a939->enter($__internal_1acde8e3e6f9ccb7fc62e291c5e7b3d3a4dd32e386382c3e6e8714621b24a939_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_1acde8e3e6f9ccb7fc62e291c5e7b3d3a4dd32e386382c3e6e8714621b24a939->leave($__internal_1acde8e3e6f9ccb7fc62e291c5e7b3d3a4dd32e386382c3e6e8714621b24a939_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_225cbd063cff0601ce22584c039e5f474f85a2a8ed1acf754ff5baa4a30966f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_225cbd063cff0601ce22584c039e5f474f85a2a8ed1acf754ff5baa4a30966f1->enter($__internal_225cbd063cff0601ce22584c039e5f474f85a2a8ed1acf754ff5baa4a30966f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </div>
    ";
        }
        
        $__internal_225cbd063cff0601ce22584c039e5f474f85a2a8ed1acf754ff5baa4a30966f1->leave($__internal_225cbd063cff0601ce22584c039e5f474f85a2a8ed1acf754ff5baa4a30966f1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Collector/exception.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
