<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_56e23bb576a31635482c42b5a46a1cb1480966f2a845600b2194782c7ef7f1d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_479aa2903265496b1891fd8ca5705dfacd06463299b3d68564e745a8fe72b6f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_479aa2903265496b1891fd8ca5705dfacd06463299b3d68564e745a8fe72b6f2->enter($__internal_479aa2903265496b1891fd8ca5705dfacd06463299b3d68564e745a8fe72b6f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_479aa2903265496b1891fd8ca5705dfacd06463299b3d68564e745a8fe72b6f2->leave($__internal_479aa2903265496b1891fd8ca5705dfacd06463299b3d68564e745a8fe72b6f2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c9b6efcb082305d7b5b6dc4ab17ac3fbb96461fc0cae348fb37e77ad6e1681c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9b6efcb082305d7b5b6dc4ab17ac3fbb96461fc0cae348fb37e77ad6e1681c9->enter($__internal_c9b6efcb082305d7b5b6dc4ab17ac3fbb96461fc0cae348fb37e77ad6e1681c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_c9b6efcb082305d7b5b6dc4ab17ac3fbb96461fc0cae348fb37e77ad6e1681c9->leave($__internal_c9b6efcb082305d7b5b6dc4ab17ac3fbb96461fc0cae348fb37e77ad6e1681c9_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:register.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/register.html.twig");
    }
}
