<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_fc0c537c107c66935b8bff2064db68fb40cbf9e3e6e0a7e505086da49d787b3d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("loginTemplate.html.twig", "FOSUserBundle:Security:login_content.html.twig", 2);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "loginTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_577595e7be94c70af15d4e671135c6da52c0f76df5f7042db6fcec9c059a520d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_577595e7be94c70af15d4e671135c6da52c0f76df5f7042db6fcec9c059a520d->enter($__internal_577595e7be94c70af15d4e671135c6da52c0f76df5f7042db6fcec9c059a520d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_577595e7be94c70af15d4e671135c6da52c0f76df5f7042db6fcec9c059a520d->leave($__internal_577595e7be94c70af15d4e671135c6da52c0f76df5f7042db6fcec9c059a520d_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_f332c1705d5040990cf7009fd8828a61abb5126130efe19cdfbfd8e5638d9947 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f332c1705d5040990cf7009fd8828a61abb5126130efe19cdfbfd8e5638d9947->enter($__internal_f332c1705d5040990cf7009fd8828a61abb5126130efe19cdfbfd8e5638d9947_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        $this->displayParentBlock("header", $context, $blocks);
        echo "

<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

";
        
        $__internal_f332c1705d5040990cf7009fd8828a61abb5126130efe19cdfbfd8e5638d9947->leave($__internal_f332c1705d5040990cf7009fd8828a61abb5126130efe19cdfbfd8e5638d9947_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_b189e9542be5beab13541aa5d00dd61407031e8455e798e53f2fadca078b9495 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b189e9542be5beab13541aa5d00dd61407031e8455e798e53f2fadca078b9495->enter($__internal_b189e9542be5beab13541aa5d00dd61407031e8455e798e53f2fadca078b9495_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 15
        $this->displayParentBlock("body", $context, $blocks);
        echo "



";
        // line 19
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 20
            echo "    <div class=\"alert alert-danger alert-dismissible fade in\"> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 22
        echo "<h1>Se connecter </h1>
<form action=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    ";
        // line 24
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : null)) {
            // line 25
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
            echo "\" />
    ";
        }
        // line 27
        echo "

    <div>
      <input type=\"text\" id=\"username\" name=\"_username\"  class=\"form-control\" placeholder=\"pseudo\" required=\"required\" />
    </div>
    <div>
    <input type=\"password\" id=\"password\" name=\"_password\" placeholder=\"mot de passe\" class=\"form-control\" required=\"required\" />
    </div>
    <div>
    <input type=\"submit\" id=\"_submit\" name=\"_submit\" class=\"btn btn-default submit\" value=\"Se connecter\" />
    </div>

    <div class=\"clearfix\"></div>


      </p>

      <div class=\"clearfix\"></div>
      <br />

      <div>
        <h1> Outil de diffusion Cdiscount © </h1>
        <p>©2016 All Rights Reserved. Cdiscount!</p>
      </div>
    </div>
</form>

";
        // line 54
        $this->displayBlock('javascripts', $context, $blocks);
        // line 59
        echo "
";
        
        $__internal_b189e9542be5beab13541aa5d00dd61407031e8455e798e53f2fadca078b9495->leave($__internal_b189e9542be5beab13541aa5d00dd61407031e8455e798e53f2fadca078b9495_prof);

    }

    // line 54
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_abee9a18528bcde64331eab56c3f15932b1978726b81bbf19ee600931e328d7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_abee9a18528bcde64331eab56c3f15932b1978726b81bbf19ee600931e328d7e->enter($__internal_abee9a18528bcde64331eab56c3f15932b1978726b81bbf19ee600931e328d7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 55
        echo "    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 56
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
        
        $__internal_abee9a18528bcde64331eab56c3f15932b1978726b81bbf19ee600931e328d7e->leave($__internal_abee9a18528bcde64331eab56c3f15932b1978726b81bbf19ee600931e328d7e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 56,  138 => 55,  132 => 54,  124 => 59,  122 => 54,  93 => 27,  87 => 25,  85 => 24,  81 => 23,  78 => 22,  72 => 20,  70 => 19,  63 => 15,  57 => 14,  42 => 4,  36 => 3,  11 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Security:login_content.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Security/login_content.html.twig");
    }
}
