<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_492e64c7e5638f3fa5451f6558a63391eeb937bf591c7355e56cfffde0a00d70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fae30ce0208014de881aa57c2d3eaf4c2eb7d1112e8a79b41da5f0f20f2fb33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fae30ce0208014de881aa57c2d3eaf4c2eb7d1112e8a79b41da5f0f20f2fb33->enter($__internal_6fae30ce0208014de881aa57c2d3eaf4c2eb7d1112e8a79b41da5f0f20f2fb33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_6fae30ce0208014de881aa57c2d3eaf4c2eb7d1112e8a79b41da5f0f20f2fb33->leave($__internal_6fae30ce0208014de881aa57c2d3eaf4c2eb7d1112e8a79b41da5f0f20f2fb33_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_41dcd35f90b1e734bfd330dbc8206791635a1c9866aca96dd2ed9c50a0c540f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41dcd35f90b1e734bfd330dbc8206791635a1c9866aca96dd2ed9c50a0c540f0->enter($__internal_41dcd35f90b1e734bfd330dbc8206791635a1c9866aca96dd2ed9c50a0c540f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())), "FOSUserBundle");
        
        $__internal_41dcd35f90b1e734bfd330dbc8206791635a1c9866aca96dd2ed9c50a0c540f0->leave($__internal_41dcd35f90b1e734bfd330dbc8206791635a1c9866aca96dd2ed9c50a0c540f0_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_7bee4e64f65d77c97a43fbc6e4e7d285c709b3d34c814fdb9705da5e1845d2c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bee4e64f65d77c97a43fbc6e4e7d285c709b3d34c814fdb9705da5e1845d2c6->enter($__internal_7bee4e64f65d77c97a43fbc6e4e7d285c709b3d34c814fdb9705da5e1845d2c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_7bee4e64f65d77c97a43fbc6e4e7d285c709b3d34c814fdb9705da5e1845d2c6->leave($__internal_7bee4e64f65d77c97a43fbc6e4e7d285c709b3d34c814fdb9705da5e1845d2c6_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_2305fa4f608259076ba0801d1d7902de6da94f637e60447026061a3744fb766d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2305fa4f608259076ba0801d1d7902de6da94f637e60447026061a3744fb766d->enter($__internal_2305fa4f608259076ba0801d1d7902de6da94f637e60447026061a3744fb766d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_2305fa4f608259076ba0801d1d7902de6da94f637e60447026061a3744fb766d->leave($__internal_2305fa4f608259076ba0801d1d7902de6da94f637e60447026061a3744fb766d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:email.txt.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
