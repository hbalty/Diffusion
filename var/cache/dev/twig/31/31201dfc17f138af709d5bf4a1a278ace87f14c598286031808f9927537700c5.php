<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_28376f4836e4ee85d79af4baddbc7fd33cca6557c6415894f3f4e734508bb614 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09e3e178dc110e602046fb1d85cfe98fd8b7f61678a90820beb2a61994acdb82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09e3e178dc110e602046fb1d85cfe98fd8b7f61678a90820beb2a61994acdb82->enter($__internal_09e3e178dc110e602046fb1d85cfe98fd8b7f61678a90820beb2a61994acdb82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_09e3e178dc110e602046fb1d85cfe98fd8b7f61678a90820beb2a61994acdb82->leave($__internal_09e3e178dc110e602046fb1d85cfe98fd8b7f61678a90820beb2a61994acdb82_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form.html.php");
    }
}
