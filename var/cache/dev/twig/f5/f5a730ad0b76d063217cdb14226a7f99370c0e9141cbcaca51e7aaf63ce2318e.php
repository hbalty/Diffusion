<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_76fcf373db7544f62f92c0c16333611611867c1753f64ffbac35f0a8f227b695 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba4ed6804d11a3f8694f6cf0f00e2275ef01310114ce06f80d90651bc0f37862 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba4ed6804d11a3f8694f6cf0f00e2275ef01310114ce06f80d90651bc0f37862->enter($__internal_ba4ed6804d11a3f8694f6cf0f00e2275ef01310114ce06f80d90651bc0f37862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_ba4ed6804d11a3f8694f6cf0f00e2275ef01310114ce06f80d90651bc0f37862->leave($__internal_ba4ed6804d11a3f8694f6cf0f00e2275ef01310114ce06f80d90651bc0f37862_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_d77f269a249a0a5e969fbdd5b1672357b629ee5812b3f8229771c8befde0449d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d77f269a249a0a5e969fbdd5b1672357b629ee5812b3f8229771c8befde0449d->enter($__internal_d77f269a249a0a5e969fbdd5b1672357b629ee5812b3f8229771c8befde0449d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        
        $__internal_d77f269a249a0a5e969fbdd5b1672357b629ee5812b3f8229771c8befde0449d->leave($__internal_d77f269a249a0a5e969fbdd5b1672357b629ee5812b3f8229771c8befde0449d_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_ba3a4b31e263d1a58546dc1cf2147a3f45466d7cccdf6458cf976506a63e0821 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba3a4b31e263d1a58546dc1cf2147a3f45466d7cccdf6458cf976506a63e0821->enter($__internal_ba3a4b31e263d1a58546dc1cf2147a3f45466d7cccdf6458cf976506a63e0821_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_ba3a4b31e263d1a58546dc1cf2147a3f45466d7cccdf6458cf976506a63e0821->leave($__internal_ba3a4b31e263d1a58546dc1cf2147a3f45466d7cccdf6458cf976506a63e0821_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_39a30b830edb16a157c477bdeb98239a2ad213b8d0741170bc649968e71c47b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39a30b830edb16a157c477bdeb98239a2ad213b8d0741170bc649968e71c47b5->enter($__internal_39a30b830edb16a157c477bdeb98239a2ad213b8d0741170bc649968e71c47b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_39a30b830edb16a157c477bdeb98239a2ad213b8d0741170bc649968e71c47b5->leave($__internal_39a30b830edb16a157c477bdeb98239a2ad213b8d0741170bc649968e71c47b5_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:email.txt.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
