<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b5fee68cfaa61562fb81263ca23167a42a752d11e24cbd73765130ade65cf7d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_80c7f91709c575567026be2486148c2f6a3aa93df40cc5ae2276800a523ef391 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80c7f91709c575567026be2486148c2f6a3aa93df40cc5ae2276800a523ef391->enter($__internal_80c7f91709c575567026be2486148c2f6a3aa93df40cc5ae2276800a523ef391_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_832be78901a9fd7c464f0810e7e1f07917c00fb8ebee5efedb9fa65722dffa78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_832be78901a9fd7c464f0810e7e1f07917c00fb8ebee5efedb9fa65722dffa78->enter($__internal_832be78901a9fd7c464f0810e7e1f07917c00fb8ebee5efedb9fa65722dffa78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_80c7f91709c575567026be2486148c2f6a3aa93df40cc5ae2276800a523ef391->leave($__internal_80c7f91709c575567026be2486148c2f6a3aa93df40cc5ae2276800a523ef391_prof);

        
        $__internal_832be78901a9fd7c464f0810e7e1f07917c00fb8ebee5efedb9fa65722dffa78->leave($__internal_832be78901a9fd7c464f0810e7e1f07917c00fb8ebee5efedb9fa65722dffa78_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_00a884dc80da4a10ddcfdf13b065e7836c2354c52b62758592bd2f3d82e685cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00a884dc80da4a10ddcfdf13b065e7836c2354c52b62758592bd2f3d82e685cd->enter($__internal_00a884dc80da4a10ddcfdf13b065e7836c2354c52b62758592bd2f3d82e685cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_15d0ffe6758a5f25ee8a876d8b9d4cb2bc6dcb4ae60c0a0cf616dacb68d66071 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15d0ffe6758a5f25ee8a876d8b9d4cb2bc6dcb4ae60c0a0cf616dacb68d66071->enter($__internal_15d0ffe6758a5f25ee8a876d8b9d4cb2bc6dcb4ae60c0a0cf616dacb68d66071_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_15d0ffe6758a5f25ee8a876d8b9d4cb2bc6dcb4ae60c0a0cf616dacb68d66071->leave($__internal_15d0ffe6758a5f25ee8a876d8b9d4cb2bc6dcb4ae60c0a0cf616dacb68d66071_prof);

        
        $__internal_00a884dc80da4a10ddcfdf13b065e7836c2354c52b62758592bd2f3d82e685cd->leave($__internal_00a884dc80da4a10ddcfdf13b065e7836c2354c52b62758592bd2f3d82e685cd_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_05cdcde4bdc8413f34b9c099e16da384ae70aabca16e5748e122698da3d48f79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05cdcde4bdc8413f34b9c099e16da384ae70aabca16e5748e122698da3d48f79->enter($__internal_05cdcde4bdc8413f34b9c099e16da384ae70aabca16e5748e122698da3d48f79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_dfc9512261378f80ea6488cff5efe103835f3b2b9370839d30f1b2c897e60c1b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfc9512261378f80ea6488cff5efe103835f3b2b9370839d30f1b2c897e60c1b->enter($__internal_dfc9512261378f80ea6488cff5efe103835f3b2b9370839d30f1b2c897e60c1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_dfc9512261378f80ea6488cff5efe103835f3b2b9370839d30f1b2c897e60c1b->leave($__internal_dfc9512261378f80ea6488cff5efe103835f3b2b9370839d30f1b2c897e60c1b_prof);

        
        $__internal_05cdcde4bdc8413f34b9c099e16da384ae70aabca16e5748e122698da3d48f79->leave($__internal_05cdcde4bdc8413f34b9c099e16da384ae70aabca16e5748e122698da3d48f79_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8593df57637ada213f9588b9c53e2e5ee5a3c6208cb30d8694c3f0cab7c1bc50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8593df57637ada213f9588b9c53e2e5ee5a3c6208cb30d8694c3f0cab7c1bc50->enter($__internal_8593df57637ada213f9588b9c53e2e5ee5a3c6208cb30d8694c3f0cab7c1bc50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_a57dada64cd70efb49454d19f321b09ba38148f6d95bd6f7d861ba6440c7d34d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a57dada64cd70efb49454d19f321b09ba38148f6d95bd6f7d861ba6440c7d34d->enter($__internal_a57dada64cd70efb49454d19f321b09ba38148f6d95bd6f7d861ba6440c7d34d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_a57dada64cd70efb49454d19f321b09ba38148f6d95bd6f7d861ba6440c7d34d->leave($__internal_a57dada64cd70efb49454d19f321b09ba38148f6d95bd6f7d861ba6440c7d34d_prof);

        
        $__internal_8593df57637ada213f9588b9c53e2e5ee5a3c6208cb30d8694c3f0cab7c1bc50->leave($__internal_8593df57637ada213f9588b9c53e2e5ee5a3c6208cb30d8694c3f0cab7c1bc50_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
