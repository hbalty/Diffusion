<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_9358a7d47d736e7ca9175777db3991abc09473e2d798730dd955a8c2ee5c2122 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ba0c4c607546b6970b2e517187bb3252aee12347b9e9e89ede46b0202445ff9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ba0c4c607546b6970b2e517187bb3252aee12347b9e9e89ede46b0202445ff9->enter($__internal_6ba0c4c607546b6970b2e517187bb3252aee12347b9e9e89ede46b0202445ff9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_d68dc5b21f7416e1305e4285ac0b5a8c8d7744f84aedbc208ccb43a9c1c9b9e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d68dc5b21f7416e1305e4285ac0b5a8c8d7744f84aedbc208ccb43a9c1c9b9e6->enter($__internal_d68dc5b21f7416e1305e4285ac0b5a8c8d7744f84aedbc208ccb43a9c1c9b9e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_6ba0c4c607546b6970b2e517187bb3252aee12347b9e9e89ede46b0202445ff9->leave($__internal_6ba0c4c607546b6970b2e517187bb3252aee12347b9e9e89ede46b0202445ff9_prof);

        
        $__internal_d68dc5b21f7416e1305e4285ac0b5a8c8d7744f84aedbc208ccb43a9c1c9b9e6->leave($__internal_d68dc5b21f7416e1305e4285ac0b5a8c8d7744f84aedbc208ccb43a9c1c9b9e6_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\traces.txt.twig");
    }
}
