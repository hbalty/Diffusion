<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_415d9f7faeed75c08048353fbc5336e42d2d8dd12c1a30fa2be41d72ccac0504 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fd36939ab0ed4febf6172655765e4f2fc1f46c014021a1f8142ce23a3c67aa9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fd36939ab0ed4febf6172655765e4f2fc1f46c014021a1f8142ce23a3c67aa9->enter($__internal_7fd36939ab0ed4febf6172655765e4f2fc1f46c014021a1f8142ce23a3c67aa9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_7fd36939ab0ed4febf6172655765e4f2fc1f46c014021a1f8142ce23a3c67aa9->leave($__internal_7fd36939ab0ed4febf6172655765e4f2fc1f46c014021a1f8142ce23a3c67aa9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/repeated_row.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\repeated_row.html.php");
    }
}
