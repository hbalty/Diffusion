<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_6e943f9359dc674e8c7cb3aca300a87052d3b0471e2b4fafc20c47f852678c86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41e357b73a8febff329ec8e7fe4ae8157cc14c99545ee63366f638c8c1d5a68a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41e357b73a8febff329ec8e7fe4ae8157cc14c99545ee63366f638c8c1d5a68a->enter($__internal_41e357b73a8febff329ec8e7fe4ae8157cc14c99545ee63366f638c8c1d5a68a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : null), "message" => (isset($context["status_text"]) ? $context["status_text"] : null))));
        echo "
";
        
        $__internal_41e357b73a8febff329ec8e7fe4ae8157cc14c99545ee63366f638c8c1d5a68a->leave($__internal_41e357b73a8febff329ec8e7fe4ae8157cc14c99545ee63366f638c8c1d5a68a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:error.json.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
