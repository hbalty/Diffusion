<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_0161bd475eae422a83fa821fd15d6b4ee95137deb5b6f04174b178a7739d19ef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_938e8d4085080d8a7be5c453eee3b17456d6493ce50ff2b56e8b7638f4dfccca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_938e8d4085080d8a7be5c453eee3b17456d6493ce50ff2b56e8b7638f4dfccca->enter($__internal_938e8d4085080d8a7be5c453eee3b17456d6493ce50ff2b56e8b7638f4dfccca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_938e8d4085080d8a7be5c453eee3b17456d6493ce50ff2b56e8b7638f4dfccca->leave($__internal_938e8d4085080d8a7be5c453eee3b17456d6493ce50ff2b56e8b7638f4dfccca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/integer_widget.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}
