<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_a28ca5e2cda5f0175604be4c9df6ec4fefcedf2720072334223cab604ff6a7c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd40241e41ca1be64bc28e6df5c5fdc7e00b985f68136441ed6ed3b2236ef3a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd40241e41ca1be64bc28e6df5c5fdc7e00b985f68136441ed6ed3b2236ef3a5->enter($__internal_fd40241e41ca1be64bc28e6df5c5fdc7e00b985f68136441ed6ed3b2236ef3a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
        
        $__internal_fd40241e41ca1be64bc28e6df5c5fdc7e00b985f68136441ed6ed3b2236ef3a5->leave($__internal_fd40241e41ca1be64bc28e6df5c5fdc7e00b985f68136441ed6ed3b2236ef3a5_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/exception.rdf.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.rdf.twig");
    }
}
