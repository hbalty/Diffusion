<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_a30109f6e0087fd4f4c80fcc392b062389545147edf7dee8109441ecfe2d5ed8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02b4f7a68680d0f482cfa8a94f856a49813d7bbde496017fb69f12f8f56cdae6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02b4f7a68680d0f482cfa8a94f856a49813d7bbde496017fb69f12f8f56cdae6->enter($__internal_02b4f7a68680d0f482cfa8a94f856a49813d7bbde496017fb69f12f8f56cdae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_02b4f7a68680d0f482cfa8a94f856a49813d7bbde496017fb69f12f8f56cdae6->leave($__internal_02b4f7a68680d0f482cfa8a94f856a49813d7bbde496017fb69f12f8f56cdae6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_aa9391968d9e5af3eccfc54a404072e09fb50a39af4d0c6e5b8b1794f16be339 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa9391968d9e5af3eccfc54a404072e09fb50a39af4d0c6e5b8b1794f16be339->enter($__internal_aa9391968d9e5af3eccfc54a404072e09fb50a39af4d0c6e5b8b1794f16be339_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_aa9391968d9e5af3eccfc54a404072e09fb50a39af4d0c6e5b8b1794f16be339->leave($__internal_aa9391968d9e5af3eccfc54a404072e09fb50a39af4d0c6e5b8b1794f16be339_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/ChangePassword/change_password.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\ChangePassword\\change_password.html.twig");
    }
}
