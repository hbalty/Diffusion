<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_db76e6c676281b8f9b480acd7d8329c0ec3fb17be990b24087f8a64fede011a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b80f8b38c7b8c12a93d08fd8371b12cf1757ba20cfe003875c1bb4080207a433 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b80f8b38c7b8c12a93d08fd8371b12cf1757ba20cfe003875c1bb4080207a433->enter($__internal_b80f8b38c7b8c12a93d08fd8371b12cf1757ba20cfe003875c1bb4080207a433_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_b80f8b38c7b8c12a93d08fd8371b12cf1757ba20cfe003875c1bb4080207a433->leave($__internal_b80f8b38c7b8c12a93d08fd8371b12cf1757ba20cfe003875c1bb4080207a433_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_enctype.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}
