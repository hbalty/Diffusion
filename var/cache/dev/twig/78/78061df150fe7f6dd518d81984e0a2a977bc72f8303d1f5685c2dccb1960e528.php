<?php

/* metier/metierDetails.html.twig */
class __TwigTemplate_5a71672e262fa804b017d87c5398f4e8e20a3df4da5613595796b92a98266c50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "metier/metierDetails.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b85f4b96324c647a9abd670d4635b82e679a7f2e4556d7decd5391babc38e9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b85f4b96324c647a9abd670d4635b82e679a7f2e4556d7decd5391babc38e9f->enter($__internal_2b85f4b96324c647a9abd670d4635b82e679a7f2e4556d7decd5391babc38e9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "metier/metierDetails.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b85f4b96324c647a9abd670d4635b82e679a7f2e4556d7decd5391babc38e9f->leave($__internal_2b85f4b96324c647a9abd670d4635b82e679a7f2e4556d7decd5391babc38e9f_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_aad5ad068a122411f094fd848e76c3f28404396b9218882f0f8ca046d9d93201 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aad5ad068a122411f094fd848e76c3f28404396b9218882f0f8ca046d9d93201->enter($__internal_aad5ad068a122411f094fd848e76c3f28404396b9218882f0f8ca046d9d93201_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>

<script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script>
  
  ";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        
        $__internal_aad5ad068a122411f094fd848e76c3f28404396b9218882f0f8ca046d9d93201->leave($__internal_aad5ad068a122411f094fd848e76c3f28404396b9218882f0f8ca046d9d93201_prof);

    }

    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_4030d8b5969a6ca042acee5ccb4d6ea7f4eeb5a3494e8a6554dc394261a73181 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4030d8b5969a6ca042acee5ccb4d6ea7f4eeb5a3494e8a6554dc394261a73181->enter($__internal_4030d8b5969a6ca042acee5ccb4d6ea7f4eeb5a3494e8a6554dc394261a73181_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 12
        echo "      
    ";
        // line 13
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 14
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 16
        echo "      
     ";
        
        $__internal_4030d8b5969a6ca042acee5ccb4d6ea7f4eeb5a3494e8a6554dc394261a73181->leave($__internal_4030d8b5969a6ca042acee5ccb4d6ea7f4eeb5a3494e8a6554dc394261a73181_prof);

    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
        $__internal_3cbe821a7278ce632421e3084a73d49bb3c1fbf823f196260fb7fb003b340345 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cbe821a7278ce632421e3084a73d49bb3c1fbf823f196260fb7fb003b340345->enter($__internal_3cbe821a7278ce632421e3084a73d49bb3c1fbf823f196260fb7fb003b340345_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 21
        echo "
<div class=\"right_col\" role=\"main\" style=\"min-height: 478px;\">

        <div class=\"row\">
              <div class=\"col-md-12 col-sm-12 col-xs-12\">
                <div class=\"x_panel\">
                  <div class=\"x_title\">
                    <h2> Ajouter un metier  </h2>
                    <ul class=\"nav navbar-right panel_toolbox\">
                      <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
                      </li>
                      <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
                      </li>
                    </ul>
                    <div class=\"clearfix\"></div>
                  </div>
                  <div class=\"x_content\">
                    <br>    

<h1>  ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["metier"]) ? $context["metier"] : null), "nomMetier", array()), "html", null, true);
        echo " </h1>

    <h3> Détails </h3>
    <b> Liaison Client : </b> ";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["metier"]) ? $context["metier"] : null), "client", array()), "clientName", array()), "html", null, true);
        echo " <br>
    <b> Statut : </b> ";
        // line 44
        if ($this->getAttribute((isset($context["metier"]) ? $context["metier"] : null), "activationStatus", array())) {
            echo " Activée ";
        } else {
            echo " Désactivée  ";
        }
        // line 45
        echo "<p>
    <h3> Applications Concernées </h3>
    <table class='table table-bordered table-hover dataTable'>
        <tr>
            <td> Nom Application </td>
            <td> Statut</td>
        </tr>
        
    ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["metier"]) ? $context["metier"] : null), "applications", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["application"]) {
            // line 54
            echo "        <tr>
            <td> ";
            // line 55
            echo twig_escape_filter($this->env, $this->getAttribute($context["application"], "nomApplication", array()), "html", null, true);
            echo "</td>
            <td> ";
            // line 56
            if ($this->getAttribute($context["application"], "activationStatus", array())) {
                echo " Activée ";
            } else {
                echo " Désactivée  ";
            }
            echo " </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['application'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "    </table>
    <h3> Listes de diffusion </h3>

            
    ";
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_split_filter($this->env, $this->getAttribute((isset($context["metier"]) ? $context["metier"] : null), "mailingLists", array()), ";"));
        foreach ($context['_seq'] as $context["_key"] => $context["mailingList"]) {
            // line 64
            echo "        ";
            echo twig_escape_filter($this->env, $context["mailingList"], "html", null, true);
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mailingList'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "</p>



                  </div>
                  </div>
                </div>
        </div>
    </div>





";
        
        $__internal_3cbe821a7278ce632421e3084a73d49bb3c1fbf823f196260fb7fb003b340345->leave($__internal_3cbe821a7278ce632421e3084a73d49bb3c1fbf823f196260fb7fb003b340345_prof);

    }

    // line 82
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f6206fa4633777f8b6a624d19592be4dc397bd8ab72e7cd151a6c0d2132258e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6206fa4633777f8b6a624d19592be4dc397bd8ab72e7cd151a6c0d2132258e3->enter($__internal_f6206fa4633777f8b6a624d19592be4dc397bd8ab72e7cd151a6c0d2132258e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 83
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 84
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 86
        echo "      ";
        
        $__internal_f6206fa4633777f8b6a624d19592be4dc397bd8ab72e7cd151a6c0d2132258e3->leave($__internal_f6206fa4633777f8b6a624d19592be4dc397bd8ab72e7cd151a6c0d2132258e3_prof);

    }

    public function getTemplateName()
    {
        return "metier/metierDetails.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 86,  257 => 84,  252 => 83,  246 => 82,  225 => 66,  216 => 64,  212 => 63,  206 => 59,  193 => 56,  189 => 55,  186 => 54,  182 => 53,  172 => 45,  166 => 44,  162 => 43,  156 => 40,  135 => 21,  129 => 20,  121 => 16,  71 => 14,  67 => 13,  64 => 12,  52 => 11,  43 => 4,  37 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "metier/metierDetails.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\metier\\metierDetails.html.twig");
    }
}
