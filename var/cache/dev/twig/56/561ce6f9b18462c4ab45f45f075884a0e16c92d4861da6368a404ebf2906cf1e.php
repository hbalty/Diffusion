<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_5683d3d29ac6fdeddf6ed372d403078c5f5b5f5c2d412fe7302b3f037f8549d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e623a9f02948a39677b7ac6f7589f54e5a4da265eaaf5c89fc394ffc6f2f3fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e623a9f02948a39677b7ac6f7589f54e5a4da265eaaf5c89fc394ffc6f2f3fc->enter($__internal_3e623a9f02948a39677b7ac6f7589f54e5a4da265eaaf5c89fc394ffc6f2f3fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_3e623a9f02948a39677b7ac6f7589f54e5a4da265eaaf5c89fc394ffc6f2f3fc->leave($__internal_3e623a9f02948a39677b7ac6f7589f54e5a4da265eaaf5c89fc394ffc6f2f3fc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/choice_options.html.php", "C:\\xampp2\\htdocs\\symfony\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_options.html.php");
    }
}
