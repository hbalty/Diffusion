<?php

/* diffusion/history.html.twig */
class __TwigTemplate_9f2e15c90182ea1d2c6cabd05fad3ff727770a7a3628b5f489a6a5a6ee228310 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("dashboard.html.twig", "diffusion/history.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "dashboard.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a20f2fddf43323bf4a26c86e47b8459cc773b17e2aad290cb02949a195884be6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a20f2fddf43323bf4a26c86e47b8459cc773b17e2aad290cb02949a195884be6->enter($__internal_a20f2fddf43323bf4a26c86e47b8459cc773b17e2aad290cb02949a195884be6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "diffusion/history.html.twig"));

        $__internal_358b35f32ed8d1c536cd052b72b126492b48e13b303c3faab1d18de752fa2d07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_358b35f32ed8d1c536cd052b72b126492b48e13b303c3faab1d18de752fa2d07->enter($__internal_358b35f32ed8d1c536cd052b72b126492b48e13b303c3faab1d18de752fa2d07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "diffusion/history.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a20f2fddf43323bf4a26c86e47b8459cc773b17e2aad290cb02949a195884be6->leave($__internal_a20f2fddf43323bf4a26c86e47b8459cc773b17e2aad290cb02949a195884be6_prof);

        
        $__internal_358b35f32ed8d1c536cd052b72b126492b48e13b303c3faab1d18de752fa2d07->leave($__internal_358b35f32ed8d1c536cd052b72b126492b48e13b303c3faab1d18de752fa2d07_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_b4a28e789f84036c30e54fd73f35723d46d8a1a268791b5ddad93498a936f13c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4a28e789f84036c30e54fd73f35723d46d8a1a268791b5ddad93498a936f13c->enter($__internal_b4a28e789f84036c30e54fd73f35723d46d8a1a268791b5ddad93498a936f13c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_d6ffd260221efe0f3e4044ce0a8a1422e70e2a185f6660e490fcf62dafc4600c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6ffd260221efe0f3e4044ce0a8a1422e70e2a185f6660e490fcf62dafc4600c->enter($__internal_d6ffd260221efe0f3e4044ce0a8a1422e70e2a185f6660e490fcf62dafc4600c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        echo "
<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>
  ";
        // line 13
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 20
        echo "

";
        
        $__internal_d6ffd260221efe0f3e4044ce0a8a1422e70e2a185f6660e490fcf62dafc4600c->leave($__internal_d6ffd260221efe0f3e4044ce0a8a1422e70e2a185f6660e490fcf62dafc4600c_prof);

        
        $__internal_b4a28e789f84036c30e54fd73f35723d46d8a1a268791b5ddad93498a936f13c->leave($__internal_b4a28e789f84036c30e54fd73f35723d46d8a1a268791b5ddad93498a936f13c_prof);

    }

    // line 13
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_c7edf9b8fecd23525769c486ff615703d9222450ac8e008c5c09c0202a0ca294 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7edf9b8fecd23525769c486ff615703d9222450ac8e008c5c09c0202a0ca294->enter($__internal_c7edf9b8fecd23525769c486ff615703d9222450ac8e008c5c09c0202a0ca294_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_37fea4b852f239c22f92ca82444a14ec6b834684bda7815881ed63f19d579471 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37fea4b852f239c22f92ca82444a14ec6b834684bda7815881ed63f19d579471->enter($__internal_37fea4b852f239c22f92ca82444a14ec6b834684bda7815881ed63f19d579471_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 14
        echo "\t  
\t";
        // line 15
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 16
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "\t<link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
\t";
        }
        unset($context["asset_url"]);
        // line 18
        echo "\t  
\t ";
        
        $__internal_37fea4b852f239c22f92ca82444a14ec6b834684bda7815881ed63f19d579471->leave($__internal_37fea4b852f239c22f92ca82444a14ec6b834684bda7815881ed63f19d579471_prof);

        
        $__internal_c7edf9b8fecd23525769c486ff615703d9222450ac8e008c5c09c0202a0ca294->leave($__internal_c7edf9b8fecd23525769c486ff615703d9222450ac8e008c5c09c0202a0ca294_prof);

    }

    // line 24
    public function block_body($context, array $blocks = array())
    {
        $__internal_36ab1a22378049d9d2e3b4f726ea70086084cfd9580df07f2bef3dc5f4aa1ae9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36ab1a22378049d9d2e3b4f726ea70086084cfd9580df07f2bef3dc5f4aa1ae9->enter($__internal_36ab1a22378049d9d2e3b4f726ea70086084cfd9580df07f2bef3dc5f4aa1ae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_dce7119869b152bf3041d5a09075bfa38b25785d9224478d10cebd471c5db032 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dce7119869b152bf3041d5a09075bfa38b25785d9224478d10cebd471c5db032->enter($__internal_dce7119869b152bf3041d5a09075bfa38b25785d9224478d10cebd471c5db032_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 25
        echo "



<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2> Timeline </h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
<ul class=\"timeline\">
\t\t
\t\t
\t\t";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history"]) ? $context["history"] : $this->getContext($context, "history")));
        foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
            // line 57
            echo "\t\t\t";
            if ((($this->getAttribute($context["action"], "id", array()) % 2) == 0)) {
                // line 58
                echo "\t\t\t\t\t        <li>
\t\t\t\t\t\t<div class=\"timeline-badge info\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
\t\t\t\t\t\t<div class=\"timeline-panel\">
\t\t\t\t\t\t  <div class=\"timeline-heading\">
\t\t\t\t\t\t\t<h4 class=\"timeline-title\"> Envoi de mail </h4>
\t\t\t\t\t\t\t<p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
                // line 63
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "dateEnvoi", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "  </small></p>
\t\t\t\t\t\t  </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t <h5> Destinataires : </h5> 
\t\t\t\t ";
                // line 68
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destinataire", array()), "html", null, true);
                echo "

\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires CC : </h5>
\t\t\t\t";
                // line 72
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destEnCopie", array()), "html", null, true);
                echo "
\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<h5> Categorie :
\t\t\t\t\t";
                // line 77
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["action"], "template", array()), "categorie", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
                    // line 78
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "nomCategorie", array()), "html", null, true);
                    echo "
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "\t\t\t\t</h5>
\t\t\t\t<p> Envoyé par : ";
                // line 81
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "emetteur", array()), "html", null, true);
                echo "</p> <br>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"";
                // line 82
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t";
                // line 85
                echo $this->getAttribute($context["action"], "contenu", array());
                echo "
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t\t</p>
\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#";
                // line 93
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\tAfficher mail 
\t\t\t\t</button>
            </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </li>
\t\t";
            } else {
                // line 100
                echo "\t\t\t    <li class=\"timeline-inverted\">
          <div class=\"timeline-badge warning\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
          <div class=\"timeline-panel\">
            <div class=\"timeline-heading\">
              <h4 class=\"timeline-title\">Envoi de mail </h4>
\t\t\t  <p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> ";
                // line 105
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["action"], "dateEnvoi", array()), "F jS \\a\\t g:ia", "Europe/Paris"), "html", null, true);
                echo "  </small></p>
            </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t <h5> Destinataires : </h5> 
\t\t\t\t ";
                // line 110
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destinataire", array()), "html", null, true);
                echo "

\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires CC : </h5>
\t\t\t\t";
                // line 114
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "destEnCopie", array()), "html", null, true);
                echo "
\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<h5>
\t\t\t\t\t";
                // line 119
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["action"], "template", array()), "categorie", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["categorie"]) {
                    // line 120
                    echo "\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["categorie"], "nomCategorie", array()), "html", null, true);
                    echo "
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categorie'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 122
                echo "\t\t\t\t</h5>
\t\t\t\t<p> Envoyé par : ";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "emetteur", array()), "html", null, true);
                echo "</p><br>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"";
                // line 124
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t";
                // line 127
                echo $this->getAttribute($context["action"], "contenu", array());
                echo "
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t\t</p>
\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#";
                // line 135
                echo twig_escape_filter($this->env, $this->getAttribute($context["action"], "id", array()), "html", null, true);
                echo "\">
\t\t\t\t\tAfficher mail 
\t\t\t\t</button>
            </div>
        </li>
\t";
            }
            // line 141
            echo "\t

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 144
        echo "\t\t
\t\t
        
 

    </ul>

    


    </div>
</div>
</div>
</div>



<!-- Modal -->


";
        
        $__internal_dce7119869b152bf3041d5a09075bfa38b25785d9224478d10cebd471c5db032->leave($__internal_dce7119869b152bf3041d5a09075bfa38b25785d9224478d10cebd471c5db032_prof);

        
        $__internal_36ab1a22378049d9d2e3b4f726ea70086084cfd9580df07f2bef3dc5f4aa1ae9->leave($__internal_36ab1a22378049d9d2e3b4f726ea70086084cfd9580df07f2bef3dc5f4aa1ae9_prof);

    }

    // line 166
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_63bffc39a9938d7b5965ea68004eb30bf566dd43777e52893cd7c5847223dea4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63bffc39a9938d7b5965ea68004eb30bf566dd43777e52893cd7c5847223dea4->enter($__internal_63bffc39a9938d7b5965ea68004eb30bf566dd43777e52893cd7c5847223dea4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_0aededc2521b3942c84d38d424ccd8969bc9bda65138a7ee7c79c610bcb805dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0aededc2521b3942c84d38d424ccd8969bc9bda65138a7ee7c79c610bcb805dc->enter($__internal_0aededc2521b3942c84d38d424ccd8969bc9bda65138a7ee7c79c610bcb805dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 167
        echo "\t\t";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 168
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t\t\t<script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t\t";
        }
        unset($context["asset_url"]);
        // line 170
        echo "\t  ";
        
        $__internal_0aededc2521b3942c84d38d424ccd8969bc9bda65138a7ee7c79c610bcb805dc->leave($__internal_0aededc2521b3942c84d38d424ccd8969bc9bda65138a7ee7c79c610bcb805dc_prof);

        
        $__internal_63bffc39a9938d7b5965ea68004eb30bf566dd43777e52893cd7c5847223dea4->leave($__internal_63bffc39a9938d7b5965ea68004eb30bf566dd43777e52893cd7c5847223dea4_prof);

    }

    public function getTemplateName()
    {
        return "diffusion/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  464 => 170,  408 => 168,  403 => 167,  394 => 166,  364 => 144,  356 => 141,  347 => 135,  336 => 127,  330 => 124,  326 => 123,  323 => 122,  314 => 120,  310 => 119,  302 => 114,  295 => 110,  287 => 105,  280 => 100,  270 => 93,  259 => 85,  253 => 82,  249 => 81,  246 => 80,  237 => 78,  233 => 77,  225 => 72,  218 => 68,  210 => 63,  203 => 58,  200 => 57,  196 => 56,  163 => 25,  154 => 24,  143 => 18,  93 => 16,  89 => 15,  86 => 14,  77 => 13,  65 => 20,  63 => 13,  52 => 4,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'dashboard.html.twig' %}

{% block header %}

<script
  src=\"https://code.jquery.com/jquery-2.2.4.min.js\"
  integrity=\"sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=\"
  crossorigin=\"anonymous\"></script>


<script src=\"https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js\"></script>
<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css\"/>
  {% block stylesheets %}
\t  
\t{% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
\t<link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
\t{% endstylesheets %}
\t  
\t {% endblock %}


{% endblock %}

{% block body %}




<div class=\"right_col\" role=\"main\">
<div class=\"col-md-12\">
\t<div class=\"x_panel\">
\t  <div class=\"x_title\">
\t\t<h2> Timeline </h2>
\t\t<ul class=\"nav navbar-right panel_toolbox\">
\t\t  <li><a class=\"collapse-link\"><i class=\"fa fa-chevron-up\"></i></a>
\t\t  </li>
\t\t  <li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\"><i class=\"fa fa-wrench\"></i></a>
\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t  <li><a href=\"#\">Settings 1</a>
\t\t\t  </li>
\t\t\t  <li><a href=\"#\">Settings 2</a>
\t\t\t  </li>
\t\t\t</ul>
\t\t  </li>
\t\t  <li><a class=\"close-link\"><i class=\"fa fa-close\"></i></a>
\t\t  </li>
\t\t</ul>
\t\t<div class=\"clearfix\"></div>
\t  </div>
\t  <div class=\"x_content\">
\t\t\t
<ul class=\"timeline\">
\t\t
\t\t
\t\t{% for  action in history %}
\t\t\t{% if action.id % 2 == 0 %}
\t\t\t\t\t        <li>
\t\t\t\t\t\t<div class=\"timeline-badge info\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
\t\t\t\t\t\t<div class=\"timeline-panel\">
\t\t\t\t\t\t  <div class=\"timeline-heading\">
\t\t\t\t\t\t\t<h4 class=\"timeline-title\"> Envoi de mail </h4>
\t\t\t\t\t\t\t<p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> {{ action.dateEnvoi | date(\"F jS \\\\a\\\\t g:ia\",\"Europe/Paris\")}}  </small></p>
\t\t\t\t\t\t  </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t <h5> Destinataires : </h5> 
\t\t\t\t {{ action.destinataire }}

\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires CC : </h5>
\t\t\t\t{{ action.destEnCopie }}
\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<h5> Categorie :
\t\t\t\t\t{% for categorie in action.template.categorie %}
\t\t\t\t\t\t{{ categorie.nomCategorie }}
\t\t\t\t\t{% endfor %}
\t\t\t\t</h5>
\t\t\t\t<p> Envoyé par : {{ action.emetteur }}</p> <br>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"{{action.id}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t{{ action.contenu | raw }}
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t\t</p>
\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#{{action.id}}\">
\t\t\t\t\tAfficher mail 
\t\t\t\t</button>
            </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </li>
\t\t{% else  %}
\t\t\t    <li class=\"timeline-inverted\">
          <div class=\"timeline-badge warning\"><i class=\"glyphicon glyphicon-envelope\"></i></div>
          <div class=\"timeline-panel\">
            <div class=\"timeline-heading\">
              <h4 class=\"timeline-title\">Envoi de mail </h4>
\t\t\t  <p><small class=\"text-muted\"><i class=\"glyphicon glyphicon-time\"></i> {{ action.dateEnvoi | date(\"F jS \\\\a\\\\t g:ia\",\"Europe/Paris\")}}  </small></p>
            </div>
            <div class=\"timeline-body\">
             <p>
\t\t\t <h5> Destinataires : </h5> 
\t\t\t\t {{ action.destinataire }}

\t\t\t\t<br/> 
\t\t\t\t <h5> Destinataires CC : </h5>
\t\t\t\t{{ action.destEnCopie }}
\t\t\t\t
\t\t\t\t<br/>
\t\t\t\t<hr class=\"half-rule\"/>
\t\t\t\t<h5>
\t\t\t\t\t{% for categorie in action.template.categorie %}
\t\t\t\t\t\t{{ categorie.nomCategorie }}
\t\t\t\t\t{% endfor %}
\t\t\t\t</h5>
\t\t\t\t<p> Envoyé par : {{ action.emetteur }}</p><br>
\t\t\t\t<div class=\"modal fade bs-example-modal-lg\" id=\"{{action.id}}\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myLargeModalLabel\">
\t\t\t\t\t\t<div class=\"modal-dialog modal-lg\" role=\"document\">
\t\t\t\t\t\t  <div class=\"modal-content\">
\t\t\t\t\t\t\t{{ action.contenu | raw }}
\t\t\t\t\t\t  </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t  </div>

\t\t\t\t\t
\t\t\t\t
\t\t\t\t</p>
\t\t\t\t\t <button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#{{action.id}}\">
\t\t\t\t\tAfficher mail 
\t\t\t\t</button>
            </div>
        </li>
\t{% endif %}
\t

{% endfor %}
\t\t
\t\t
        
 

    </ul>

    


    </div>
</div>
</div>
</div>



<!-- Modal -->


{% endblock %}

\t{% block javascripts %}
\t\t{% javascripts '@AppBundle/Resources/public/js/*' %}
\t\t\t<script src=\"{{ asset_url }}\"></script>
\t\t{% endjavascripts %}
\t  {% endblock %}


", "diffusion/history.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\diffusion\\history.html.twig");
    }
}
