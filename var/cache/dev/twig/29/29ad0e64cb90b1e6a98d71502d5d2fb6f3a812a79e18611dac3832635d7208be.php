<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_35b0fb4aa56a462c0e3768b83a6470f33c98cc757c8df533b9b637d967aaec8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cfe67df748ba64bd01d09b9c26aee8fd80aced26c1d1d92bc56b5a3b43867de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8cfe67df748ba64bd01d09b9c26aee8fd80aced26c1d1d92bc56b5a3b43867de->enter($__internal_8cfe67df748ba64bd01d09b9c26aee8fd80aced26c1d1d92bc56b5a3b43867de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8cfe67df748ba64bd01d09b9c26aee8fd80aced26c1d1d92bc56b5a3b43867de->leave($__internal_8cfe67df748ba64bd01d09b9c26aee8fd80aced26c1d1d92bc56b5a3b43867de_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9a401e6d02d8314bb56419cc0c040b6f520552a0fa62fba81a1eb788f19f0111 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a401e6d02d8314bb56419cc0c040b6f520552a0fa62fba81a1eb788f19f0111->enter($__internal_9a401e6d02d8314bb56419cc0c040b6f520552a0fa62fba81a1eb788f19f0111_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_9a401e6d02d8314bb56419cc0c040b6f520552a0fa62fba81a1eb788f19f0111->leave($__internal_9a401e6d02d8314bb56419cc0c040b6f520552a0fa62fba81a1eb788f19f0111_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/show.html.twig", "C:\\xampp2\\htdocs\\symfony\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\show.html.twig");
    }
}
