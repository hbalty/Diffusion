<?php

/* :user:login.html.twig */
class __TwigTemplate_5de0edaf997dd6c228cc5dc4d4400e17b27b85daf9adc1e8e10788fa0da032e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("loginTemplate.html.twig", ":user:login.html.twig", 1);
        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "loginTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_790703006c92b014d40162bbfd22f13bdf41c17d29b4dc3d81a88ab95bc8d6c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_790703006c92b014d40162bbfd22f13bdf41c17d29b4dc3d81a88ab95bc8d6c4->enter($__internal_790703006c92b014d40162bbfd22f13bdf41c17d29b4dc3d81a88ab95bc8d6c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":user:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_790703006c92b014d40162bbfd22f13bdf41c17d29b4dc3d81a88ab95bc8d6c4->leave($__internal_790703006c92b014d40162bbfd22f13bdf41c17d29b4dc3d81a88ab95bc8d6c4_prof);

    }

    // line 3
    public function block_header($context, array $blocks = array())
    {
        $__internal_dc2f110264d57476bd8204b92902e7aafe72e8e1dd4dd71956515ee2c44f7aed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dc2f110264d57476bd8204b92902e7aafe72e8e1dd4dd71956515ee2c44f7aed->enter($__internal_dc2f110264d57476bd8204b92902e7aafe72e8e1dd4dd71956515ee2c44f7aed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 4
        $this->displayParentBlock("header", $context, $blocks);
        echo "

";
        
        $__internal_dc2f110264d57476bd8204b92902e7aafe72e8e1dd4dd71956515ee2c44f7aed->leave($__internal_dc2f110264d57476bd8204b92902e7aafe72e8e1dd4dd71956515ee2c44f7aed_prof);

    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        $__internal_23a81e256e36a2176e6864b54ad6fecc152dab4108f455c264804048ff1f9ad8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23a81e256e36a2176e6864b54ad6fecc152dab4108f455c264804048ff1f9ad8->enter($__internal_23a81e256e36a2176e6864b54ad6fecc152dab4108f455c264804048ff1f9ad8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 9
        $this->displayParentBlock("body", $context, $blocks);
        echo "




     ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
      <h1>Se connecter </h1>

      <div>
        ";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "userName", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Psoeudo")));
        echo "
      </div>
      <div>
         ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "Password", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Mot de passe")));
        echo "
      </div>
      <div>
        ";
        // line 24
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-default submit")));
        echo "
      </div>

              <div class=\"clearfix\"></div>

              <div class=\"separator\">
                </p>

                <div class=\"clearfix\"></div>
                <br />

                <div>
                  <h1> Outil de diffusion Cdiscount © </h1>
                  <p>©2016 All Rights Reserved. Cdiscount!</p>
                </div>
              </div>
       ";
        // line 40
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "


\t";
        // line 43
        $this->displayBlock('javascripts', $context, $blocks);
        // line 48
        echo "
";
        
        $__internal_23a81e256e36a2176e6864b54ad6fecc152dab4108f455c264804048ff1f9ad8->leave($__internal_23a81e256e36a2176e6864b54ad6fecc152dab4108f455c264804048ff1f9ad8_prof);

    }

    // line 43
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_2b18f2a1e40696aba7fb84b1f5ea3eaae638cdf4ac406d7712800ab9500afbc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b18f2a1e40696aba7fb84b1f5ea3eaae638cdf4ac406d7712800ab9500afbc9->enter($__internal_2b18f2a1e40696aba7fb84b1f5ea3eaae638cdf4ac406d7712800ab9500afbc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 44
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 45
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 47
        echo "\t";
        
        $__internal_2b18f2a1e40696aba7fb84b1f5ea3eaae638cdf4ac406d7712800ab9500afbc9->leave($__internal_2b18f2a1e40696aba7fb84b1f5ea3eaae638cdf4ac406d7712800ab9500afbc9_prof);

    }

    public function getTemplateName()
    {
        return ":user:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 47,  131 => 45,  126 => 44,  120 => 43,  112 => 48,  110 => 43,  104 => 40,  85 => 24,  79 => 21,  73 => 18,  66 => 14,  58 => 9,  52 => 8,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":user:login.html.twig", "C:\\xampp2\\htdocs\\symfony\\app/Resources\\views/user/login.html.twig");
    }
}
