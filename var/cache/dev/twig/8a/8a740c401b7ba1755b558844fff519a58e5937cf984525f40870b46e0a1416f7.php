<?php

/* dashboard.html.twig */
class __TwigTemplate_bf63c83c49833e93f23eca32cdc53da47aa7b6d4bcbdc5c2707ccbc6aced2008 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'header' => array($this, 'block_header'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4cdc2b1dd3ac04c9bb2f566015d3467ba6f06b8666ebd126c963c0ad5763dd2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4cdc2b1dd3ac04c9bb2f566015d3467ba6f06b8666ebd126c963c0ad5763dd2e->enter($__internal_4cdc2b1dd3ac04c9bb2f566015d3467ba6f06b8666ebd126c963c0ad5763dd2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard.html.twig"));

        $__internal_500d9af10895fd5a934db9336a690a8a391c2815bad37f896df75cf20cc4a325 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_500d9af10895fd5a934db9336a690a8a391c2815bad37f896df75cf20cc4a325->enter($__internal_500d9af10895fd5a934db9336a690a8a391c2815bad37f896df75cf20cc4a325_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
  <head>
    ";
        // line 4
        $this->displayBlock('header', $context, $blocks);
        // line 24
        echo "  </head>

  <body class=\"nav-md\">
    <div class=\"container body\">
      <div class=\"main_container\">
        <div class=\"col-md-3 left_col\">
          <div class=\"left_col scroll-view\">
            <div class=\"navbar nav_title\" style=\"border: 0;background-color:#DF3A00\">
              <a href=\"/\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>C' La diffusion</span></a>
            </div>

            <div class=\"clearfix\"></div>

            <!-- menu profile quick info -->
            <div class=\"profile clearfix\">
              <div class=\"profile_pic\">
                          ";
        // line 40
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "photo", array()) != null)) {
            // line 41
            echo "                          <img src=\"data:image/jpg;base64,";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "photo", array()), "html", null, true);
            echo "\" alt=\"img\" class=\"img-circle profile_img\" />
                          ";
        } else {
            // line 43
            echo "                          <img src=\"https://cdn0.vox-cdn.com/images/verge/default-avatar.v989902574302a6378709709f7baab789b242ebbb.gif\" alt=\"img\" class=\"img-circle profile_img\" />
                          ";
        }
        // line 45
        echo "              </div>
              <div class=\"profile_info\">
                <span>Welcome,</span>
                <h2> ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        echo " </h2>
              </div>
              <div class=\"clearfix\"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
              <div class=\"menu_section\">
                <h3> Menu </h3>
                <ul class=\"nav side-menu\">
                  <li><a><i class=\"fa fa-bug\"></i>Incidents <span class=\"fa fa-chevron-down\"></span></a>
                    <ul class=\"nav child_menu\">
                      <li><a href=\"/incident/new\">Nouvel Incident</a></li>
                      <li><a href=\"/incident/list\">Liste des incidents</a></li>
                    </ul>
                  </li>
                  <li><a><i class=\"fa fa-info\"></i> Information <span class=\"fa fa-chevron-down\"></span></a>
                    <ul class=\"nav child_menu\">
                      <li><a href=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("template_add");
        echo "\">Nouveau template</a></li>
                      <li><a href=\"";
        // line 70
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("template_list");
        echo "\"> Liste des templates </a></li>
                       <li><a href=\"";
        // line 71
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("information_diffusion_history");
        echo "\"> Historique (Cdiscount) </a></li>

                     
                    </ul>
                  </li>


                </ul>
              </div>
              <div class=\"menu_section\">

              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class=\"sidebar-footer hidden-small\">
              <a href=\"";
        // line 89
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("settings");
        echo "\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\" style=\"color:white\">
                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
              </a>
              <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\" style=\"color:white\"></span>
              </a>
              <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\" style=\"color:white\">
                <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
              </a>
              <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"/logout\" style=\"color:white\">
                <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class=\"top_nav\">
          <div class=\"nav_menu\">
            <nav>
              <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
              </div>

              <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"\">
                  <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                    ";
        // line 117
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "photo", array()) != null)) {
            // line 118
            echo "                    <img src=\"data:image/jpg;base64,";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "photo", array()), "html", null, true);
            echo "\" alt=\"img\"/>
                    ";
        } else {
            // line 120
            echo "                    <img src=\"https://cdn0.vox-cdn.com/images/verge/default-avatar.v989902574302a6378709709f7baab789b242ebbb.gif\" alt=\"img\" />
                    ";
        }
        // line 122
        echo "                    <span  class=\" fa fa-angle-down\"></span>
                  </a>
                  <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                    <li><a href=\"javascript:;\"> Profile</a></li>
                    <li>
                      <a href=\"";
        // line 127
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("settings");
        echo "\">

                        <span>Settings</span>
                        <i style=\"float: right\" class=\"fa fa-cogs\"></i>
                      </a>
                    </li>
                    <li><a href=\"https://wiki.cdbdx.biz/Exploitation:MajorIncidentManagement\">Help <i class=\"fa fa-info pull-right\"></i></a></li>
                    <li><a href=\"/logout\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role=\"presentation\" class=\"dropdown\">
                  <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                    <i class=\"fa fa-bell-o\"></i>
                    <span class=\"badge bg-green\">6</span>
                  </a>
                  <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list\" role=\"menu\">
                    <li>
                      <a>
                        <span class=\"image\">
                          ";
        // line 147
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "73f06ae_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae_img_1.jpg");
            // line 148
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\" />
                         ";
        } else {
            // asset "73f06ae"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae.jpg");
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\" />
                         ";
        }
        unset($context["asset_url"]);
        // line 149
        echo "</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class=\"image\">";
        // line 161
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "73f06ae_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae_img_1.jpg");
            // line 162
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\" />
                             ";
        } else {
            // asset "73f06ae"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae.jpg");
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\" />
                             ";
        }
        unset($context["asset_url"]);
        // line 163
        echo "</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class=\"image\"> ";
        // line 175
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "73f06ae_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae_img_1.jpg");
            // line 176
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\"  />
                  ";
        } else {
            // asset "73f06ae"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae.jpg");
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\"  />
                  ";
        }
        unset($context["asset_url"]);
        // line 177
        echo "</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class=\"image\"> ";
        // line 189
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "73f06ae_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae_img_1.jpg");
            // line 190
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\"  />
                  ";
        } else {
            // asset "73f06ae"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_73f06ae") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("images/73f06ae.jpg");
            echo "                          <img src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" alt=\"img\"  />
                  ";
        }
        unset($context["asset_url"]);
        // line 191
        echo "</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class=\"text-center\">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class=\"fa fa-angle-right\"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        ";
        // line 218
        $this->displayBlock('body', $context, $blocks);
        // line 221
        echo "        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class=\"pull-right\">
          Outil de diffusion <a href=\"https://cdiscount.com\">Cdiscount</a>
          </div>
          <div class=\"clearfix\"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
      ";
        // line 233
        $this->displayBlock('javascripts', $context, $blocks);
        // line 238
        echo "


  </body>
</html>
";
        
        $__internal_4cdc2b1dd3ac04c9bb2f566015d3467ba6f06b8666ebd126c963c0ad5763dd2e->leave($__internal_4cdc2b1dd3ac04c9bb2f566015d3467ba6f06b8666ebd126c963c0ad5763dd2e_prof);

        
        $__internal_500d9af10895fd5a934db9336a690a8a391c2815bad37f896df75cf20cc4a325->leave($__internal_500d9af10895fd5a934db9336a690a8a391c2815bad37f896df75cf20cc4a325_prof);

    }

    // line 4
    public function block_header($context, array $blocks = array())
    {
        $__internal_34ac87e0052f5f6e7a5097a4313f9d8474a21bea0132c465e68a8163aef07c4b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34ac87e0052f5f6e7a5097a4313f9d8474a21bea0132c465e68a8163aef07c4b->enter($__internal_34ac87e0052f5f6e7a5097a4313f9d8474a21bea0132c465e68a8163aef07c4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_3bb018df5dba8d6eef246c762cf74f02dfd3055f084f2a4c017d48cadaf98d1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bb018df5dba8d6eef246c762cf74f02dfd3055f084f2a4c017d48cadaf98d1f->enter($__internal_3bb018df5dba8d6eef246c762cf74f02dfd3055f084f2a4c017d48cadaf98d1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 5
        echo "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Diffusion </title>
    ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "



     ";
        
        $__internal_3bb018df5dba8d6eef246c762cf74f02dfd3055f084f2a4c017d48cadaf98d1f->leave($__internal_3bb018df5dba8d6eef246c762cf74f02dfd3055f084f2a4c017d48cadaf98d1f_prof);

        
        $__internal_34ac87e0052f5f6e7a5097a4313f9d8474a21bea0132c465e68a8163aef07c4b->leave($__internal_34ac87e0052f5f6e7a5097a4313f9d8474a21bea0132c465e68a8163aef07c4b_prof);

    }

    // line 12
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_df4456ce65514f08886713884327b4b1ef6c144fcd325ad34e2edaea6964bd31 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df4456ce65514f08886713884327b4b1ef6c144fcd325ad34e2edaea6964bd31->enter($__internal_df4456ce65514f08886713884327b4b1ef6c144fcd325ad34e2edaea6964bd31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_f579df224bf05fef33a01965d8668af42a362021df5342dcc88bf2df985a3dd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f579df224bf05fef33a01965d8668af42a362021df5342dcc88bf2df985a3dd1->enter($__internal_f579df224bf05fef33a01965d8668af42a362021df5342dcc88bf2df985a3dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 13
        echo "
    ";
        // line 14
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "3e7b5a2_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_animate.min_1.css");
            // line 15
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_bootstrap.min_2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_custom_3.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_font-awesome.min_4.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_ngprogress_5.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_style_6.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
            // asset "3e7b5a2_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2_part_1_timeline_7.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        } else {
            // asset "3e7b5a2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_3e7b5a2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/3e7b5a2.css");
            echo "    <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
    ";
        }
        unset($context["asset_url"]);
        // line 17
        echo "
     ";
        
        $__internal_f579df224bf05fef33a01965d8668af42a362021df5342dcc88bf2df985a3dd1->leave($__internal_f579df224bf05fef33a01965d8668af42a362021df5342dcc88bf2df985a3dd1_prof);

        
        $__internal_df4456ce65514f08886713884327b4b1ef6c144fcd325ad34e2edaea6964bd31->leave($__internal_df4456ce65514f08886713884327b4b1ef6c144fcd325ad34e2edaea6964bd31_prof);

    }

    // line 218
    public function block_body($context, array $blocks = array())
    {
        $__internal_3e37b0357720bc192a9c1624ae5e621fba22eac6c7765663a719b1acc745c85e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e37b0357720bc192a9c1624ae5e621fba22eac6c7765663a719b1acc745c85e->enter($__internal_3e37b0357720bc192a9c1624ae5e621fba22eac6c7765663a719b1acc745c85e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f01f42102645607a061c4a797df904e5477f84fd63f5c70cc828836e43e85a8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f01f42102645607a061c4a797df904e5477f84fd63f5c70cc828836e43e85a8b->enter($__internal_f01f42102645607a061c4a797df904e5477f84fd63f5c70cc828836e43e85a8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 219
        echo "
        ";
        
        $__internal_f01f42102645607a061c4a797df904e5477f84fd63f5c70cc828836e43e85a8b->leave($__internal_f01f42102645607a061c4a797df904e5477f84fd63f5c70cc828836e43e85a8b_prof);

        
        $__internal_3e37b0357720bc192a9c1624ae5e621fba22eac6c7765663a719b1acc745c85e->leave($__internal_3e37b0357720bc192a9c1624ae5e621fba22eac6c7765663a719b1acc745c85e_prof);

    }

    // line 233
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7867a07e27adcc995bc1c7df1051089ca8da8a47c8a01f64ba885be9a42499ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7867a07e27adcc995bc1c7df1051089ca8da8a47c8a01f64ba885be9a42499ec->enter($__internal_7867a07e27adcc995bc1c7df1051089ca8da8a47c8a01f64ba885be9a42499ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_977415b70455645fef20e93116e4e76810d07a43d3d952916a8da0c499814822 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_977415b70455645fef20e93116e4e76810d07a43d3d952916a8da0c499814822->enter($__internal_977415b70455645fef20e93116e4e76810d07a43d3d952916a8da0c499814822_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 234
        echo "\t    ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bb54fd1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_bootstrap.min_1.js");
            // line 235
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_custom.min_2.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_fastclick_3.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_ngprogress_4.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_script_5.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_switchery.min_6.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_6"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_6") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_template_7.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
            // asset "bb54fd1_7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1_7") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1_part_1_textreplace_8.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        } else {
            // asset "bb54fd1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_bb54fd1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bb54fd1.js");
            echo "\t        <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
\t    ";
        }
        unset($context["asset_url"]);
        // line 237
        echo "      ";
        
        $__internal_977415b70455645fef20e93116e4e76810d07a43d3d952916a8da0c499814822->leave($__internal_977415b70455645fef20e93116e4e76810d07a43d3d952916a8da0c499814822_prof);

        
        $__internal_7867a07e27adcc995bc1c7df1051089ca8da8a47c8a01f64ba885be9a42499ec->leave($__internal_7867a07e27adcc995bc1c7df1051089ca8da8a47c8a01f64ba885be9a42499ec_prof);

    }

    public function getTemplateName()
    {
        return "dashboard.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  574 => 237,  518 => 235,  513 => 234,  504 => 233,  493 => 219,  484 => 218,  473 => 17,  423 => 15,  419 => 14,  416 => 13,  407 => 12,  393 => 19,  391 => 12,  382 => 5,  373 => 4,  358 => 238,  356 => 233,  342 => 221,  340 => 218,  311 => 191,  297 => 190,  293 => 189,  279 => 177,  265 => 176,  261 => 175,  247 => 163,  233 => 162,  229 => 161,  215 => 149,  201 => 148,  197 => 147,  174 => 127,  167 => 122,  163 => 120,  157 => 118,  155 => 117,  124 => 89,  103 => 71,  99 => 70,  95 => 69,  71 => 48,  66 => 45,  62 => 43,  56 => 41,  54 => 40,  36 => 24,  34 => 4,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
  <head>
    {% block header %}
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Diffusion </title>
    {% block stylesheets %}

    {% stylesheets '@AppBundle/Resources/public/css/*' filter='cssrewrite' %}
    <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
    {% endstylesheets %}

     {% endblock %}




     {% endblock%}
  </head>

  <body class=\"nav-md\">
    <div class=\"container body\">
      <div class=\"main_container\">
        <div class=\"col-md-3 left_col\">
          <div class=\"left_col scroll-view\">
            <div class=\"navbar nav_title\" style=\"border: 0;background-color:#DF3A00\">
              <a href=\"/\" class=\"site_title\"><i class=\"fa fa-paw\"></i> <span>C' La diffusion</span></a>
            </div>

            <div class=\"clearfix\"></div>

            <!-- menu profile quick info -->
            <div class=\"profile clearfix\">
              <div class=\"profile_pic\">
                          {% if app.user.photo != null %}
                          <img src=\"data:image/jpg;base64,{{app.user.photo}}\" alt=\"img\" class=\"img-circle profile_img\" />
                          {% else %}
                          <img src=\"https://cdn0.vox-cdn.com/images/verge/default-avatar.v989902574302a6378709709f7baab789b242ebbb.gif\" alt=\"img\" class=\"img-circle profile_img\" />
                          {% endif %}
              </div>
              <div class=\"profile_info\">
                <span>Welcome,</span>
                <h2> {{ app.user.username }} </h2>
              </div>
              <div class=\"clearfix\"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id=\"sidebar-menu\" class=\"main_menu_side hidden-print main_menu\">
              <div class=\"menu_section\">
                <h3> Menu </h3>
                <ul class=\"nav side-menu\">
                  <li><a><i class=\"fa fa-bug\"></i>Incidents <span class=\"fa fa-chevron-down\"></span></a>
                    <ul class=\"nav child_menu\">
                      <li><a href=\"/incident/new\">Nouvel Incident</a></li>
                      <li><a href=\"/incident/list\">Liste des incidents</a></li>
                    </ul>
                  </li>
                  <li><a><i class=\"fa fa-info\"></i> Information <span class=\"fa fa-chevron-down\"></span></a>
                    <ul class=\"nav child_menu\">
                      <li><a href=\"{{path('template_add')}}\">Nouveau template</a></li>
                      <li><a href=\"{{path('template_list')}}\"> Liste des templates </a></li>
                       <li><a href=\"{{path('information_diffusion_history')}}\"> Historique (Cdiscount) </a></li>

                     
                    </ul>
                  </li>


                </ul>
              </div>
              <div class=\"menu_section\">

              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class=\"sidebar-footer hidden-small\">
              <a href=\"{{path('settings')}}\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Settings\" style=\"color:white\">
                <span class=\"glyphicon glyphicon-cog\" aria-hidden=\"true\"></span>
              </a>
              <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"FullScreen\">
                <span class=\"glyphicon glyphicon-fullscreen\" aria-hidden=\"true\" style=\"color:white\"></span>
              </a>
              <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Lock\" style=\"color:white\">
                <span class=\"glyphicon glyphicon-eye-close\" aria-hidden=\"true\"></span>
              </a>
              <a data-toggle=\"tooltip\" data-placement=\"top\" title=\"Logout\" href=\"/logout\" style=\"color:white\">
                <span class=\"glyphicon glyphicon-off\" aria-hidden=\"true\"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class=\"top_nav\">
          <div class=\"nav_menu\">
            <nav>
              <div class=\"nav toggle\">
                <a id=\"menu_toggle\"><i class=\"fa fa-bars\"></i></a>
              </div>

              <ul class=\"nav navbar-nav navbar-right\">
                <li class=\"\">
                  <a href=\"javascript:;\" class=\"user-profile dropdown-toggle\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                    {% if app.user.photo != null %}
                    <img src=\"data:image/jpg;base64,{{app.user.photo}}\" alt=\"img\"/>
                    {% else %}
                    <img src=\"https://cdn0.vox-cdn.com/images/verge/default-avatar.v989902574302a6378709709f7baab789b242ebbb.gif\" alt=\"img\" />
                    {% endif %}
                    <span  class=\" fa fa-angle-down\"></span>
                  </a>
                  <ul class=\"dropdown-menu dropdown-usermenu pull-right\">
                    <li><a href=\"javascript:;\"> Profile</a></li>
                    <li>
                      <a href=\"{{path('settings')}}\">

                        <span>Settings</span>
                        <i style=\"float: right\" class=\"fa fa-cogs\"></i>
                      </a>
                    </li>
                    <li><a href=\"https://wiki.cdbdx.biz/Exploitation:MajorIncidentManagement\">Help <i class=\"fa fa-info pull-right\"></i></a></li>
                    <li><a href=\"/logout\"><i class=\"fa fa-sign-out pull-right\"></i> Log Out</a></li>
                  </ul>
                </li>

                <li role=\"presentation\" class=\"dropdown\">
                  <a href=\"javascript:;\" class=\"dropdown-toggle info-number\" data-toggle=\"dropdown\" aria-expanded=\"false\">
                    <i class=\"fa fa-bell-o\"></i>
                    <span class=\"badge bg-green\">6</span>
                  </a>
                  <ul id=\"menu1\" class=\"dropdown-menu list-unstyled msg_list\" role=\"menu\">
                    <li>
                      <a>
                        <span class=\"image\">
                          {% image '@AppBundle/Resources/public/images/img.jpg' %}
                          <img src=\"{{ asset_url }}\" alt=\"img\" />
                         {% endimage %}</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class=\"image\">{% image '@AppBundle/Resources/public/images/img.jpg' %}
                          <img src=\"{{ asset_url }}\" alt=\"img\" />
                             {% endimage %}</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class=\"image\"> {% image '@AppBundle/Resources/public/images/img.jpg' %}
                          <img src=\"{{ asset_url }}\" alt=\"img\"  />
                  {% endimage %}</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <a>
                        <span class=\"image\"> {% image '@AppBundle/Resources/public/images/img.jpg' %}
                          <img src=\"{{ asset_url }}\" alt=\"img\"  />
                  {% endimage %}</span>
                        <span>
                          <span>John Smith</span>
                          <span class=\"time\">3 mins ago</span>
                        </span>
                        <span class=\"message\">
                          Film festivals used to be do-or-die moments for movie makers. They were where...
                        </span>
                      </a>
                    </li>
                    <li>
                      <div class=\"text-center\">
                        <a>
                          <strong>See All Alerts</strong>
                          <i class=\"fa fa-angle-right\"></i>
                        </a>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        {% block body%}

        {% endblock %}
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class=\"pull-right\">
          Outil de diffusion <a href=\"https://cdiscount.com\">Cdiscount</a>
          </div>
          <div class=\"clearfix\"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
      {% block javascripts %}
\t    {% javascripts '@AppBundle/Resources/public/js/*' %}
\t        <script src=\"{{ asset_url }}\"></script>
\t    {% endjavascripts %}
      {% endblock %}



  </body>
</html>
", "dashboard.html.twig", "C:\\xampp2\\htdocs\\symfony\\app\\Resources\\views\\dashboard.html.twig");
    }
}
